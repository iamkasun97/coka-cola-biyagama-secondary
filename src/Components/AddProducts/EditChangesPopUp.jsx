import {useEffect , useState} from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import Loader from '../Loader';
import * as Yup from "yup";
import { Form, Formik } from "formik";
import Textfield from '../FormsUI/Textfield';
import Select from '../FormsUI/Select';
import { loadCaseConfiguration} from '../../Services/ConfigurationService';
import { editProduct } from '../../Services/ProductService';

export default function FormDialog(props) {

  const [cases , setCases] = useState([]);
  useEffect(() => {
    loadCaseConfiguration(setCases);
  },[]);
  const [open, setOpen] = useState(false);
  const [loading , setLoading] = useState(false);

  const handleClickOpen = () => {
    // console.log(props.data.row.case_type_id);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  // form validation
  const INITIAL_FORM_STATE = {
    productName:  props.data.row.item_name,
    caseType : props.data.row.case_type_id
};

const FORM_VALIDATION = Yup.object().shape({
    productName : Yup.string().required('Please Enter product name'),
    caseType : Yup.number().required('please select a case type')
});

  return (
    <div>
      <Button variant="contained" onClick={handleClickOpen} style={{width : '80px' , height : '40px'}}>
        Edit
      </Button>
      <div style={{width : '1000px'}}>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Edit User Details</DialogTitle>
        <Formik
          initialValues={{
              ...INITIAL_FORM_STATE,
          }}
          validationSchema={FORM_VALIDATION}
          onSubmit={(values , {resetForm}) => {
              // console.log(values , props.data.id);
              setLoading(true);
              editProduct(props.data.id , values.productName , values.caseType , props.currentPage , props.postsPerPage , props.setProducts , props.setCount , setLoading);
              resetForm();
              setOpen(false);
          }}
          >
          <Form>
        <DialogContent>
          <Grid container spacing={3} >
            <Grid item xs={12} sm={6} >
              <Textfield
                    type={"text"}
                    name="productName"
                    label="Product Name"
                    placeholder="Product Name"
                />
            </Grid>
            <Grid item xs={12} sm={6} >
              <Select name='caseType' options={cases} />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button type='submit'>Save</Button>
        </DialogActions>
        </Form></Formik>
        {loading && <Loader />}
      </Dialog>
      </div>
    </div>
  );
}