export const SidebarData = [
    {
      title: "Home",
      path: "/home",
      icon: "fa fa-home",
      role : ['ADMIN' , 'SUBADMIN']
    },
    {
      title: "Issue Stock",
      path: "/issue-stock",
      icon: "fa fa-arrow-circle-right",
      role : ['ADMIN' , 'SUBADMIN']
    },
    {
      title: "Route Returns",
      path: "/route-returns",
      icon: "fa fa-reply",
      role : ['ADMIN' , 'SUBADMIN']
    },
    {
      title: "Collections",
      path: "/collections",
      icon: "fa fa-usd",
      role : ['ADMIN' , 'SUBADMIN']
    },
    {
      title: "Input Stock",
      path: "/input-stock",
      icon: "fa fa-arrow-circle-left",
      role : ['ADMIN' , 'SUBADMIN']
    },
    {
      title: "Return Inventory",
      path: "/return-inventory",
      icon: "fa fa-retweet",
      role : ['ADMIN' , 'SUBADMIN']
    },
    {
      title: "Reports",
      path: "/sales",
      icon: "fa fa-balance-scale",
      role : ['ADMIN' , 'SUBADMIN']
    },
    {
      title: "Error Correction",
      path: "/error-correction",
      icon: "fa fa-pencil",
      role : ['ADMIN' , 'SUBADMIN']
    },
    {
      title: "Enter Details",
      icon: "fa fa-plus-square",
      iconClosed: "fas fa-chevron-down",
      iconOpened: "fas fa-chevron-up",
      role : ['ADMIN'],
      subNav: [
        {
          title: "Add New User",
          path: "/add-users",
          icon: "fa fa-users",
        },
        {
          title: "Add New Route",
          path: "/add-routes",
          icon: "fa fa-road",
        },
        {
          title: "Add New Case",
          path: "/add-cases",
          icon: "fa fa-th",
        },
        {
          title: "Add New Product",
          path: "/add-products",
          icon: "fa fa-beer",
        },
        {
          title: "Add New collection type",
          path: "/add-collections",
          icon: "fa fa-briefcase",
        },
      ],
    },
  ];



  /*
  
  {
      title: "Report",
      icon: "fas fa-chart-simple",
      activePath: "/report/",
      iconClosed: "fas fa-chevron-down",
      iconOpened: "fas fa-chevron-up",
  
      subNav: [
        {
          title: "SMS Log",
          path: "/report/sms-log",
          icon: "fas fa-comment-sms",
        },
        {
          title: "Email Log",
          path: "/report/email-log",
          icon: "fas fa-envelope",
        },
      ],
    },
  
  */
  