import * as api from '../api/index';

export const getDashBoardData = async (startDate , endDate , setSales , setRouteSales , setTotSales , setTotProfit , setColors) => {
    try{
        const data = await api.getDashBoardData(startDate , endDate);
        let sales = [];
        data.data.data.dateSales?.map(item => {
            let temp = {
                date : item[0],
                sales : Number(item[1])/1000
            };

            sales.push(temp);
        });

        let routes = [];
        data.data.data.routeSales?.map(item => {  
        const res = data.data.data?.routeList?.filter(route => route.id == item[0]);
        if(res.length !== 0){
            let temp = {
                name : res[0].route_name,
                value : item[1]
            }
            routes.push(temp);
        }  
        });
        let colors = [];
        routes.forEach(e => {
            let maxVal = 0xFFFFFF; // 16777215
            let randomNumber = Math.random() * maxVal; 
            randomNumber = Math.floor(randomNumber);
            randomNumber = randomNumber.toString(16);
            let randColor = randomNumber.padStart(6, 0);   
            colors.push(`#${randColor.toUpperCase()}`);
        })
        setColors(colors);
        setRouteSales(routes);
        setSales(sales);
        setTotSales(data.data.data.profit[0][1]);
        setTotProfit(data.data.data.profit[0][0]);
    }catch(error){

    }
} 