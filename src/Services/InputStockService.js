import * as api from '../api/index';
import { openSuccessDialog } from '../Components/FormsUI/PopUps/SuccessDialog/index.tsx';

export const startInputStockTransaction = async (date , invoiceId , setLoading , setTransactionId , setRows) => {
    try{
        const data = await api.startInputStockTransaction(date , invoiceId);
        // console.log(data);
        if(data.data.status === 'success'){
            setTransactionId(data.data.data.id);
            const dataNext = await api.getStockItems();
            // console.log(dataNext);
            if(dataNext.data.status === 'success'){
                let rows = [];
                dataNext.data.data.map((item) => {
                    let temp = {
                        id : item.id,
                        item : item.item_name,
                        caseType : item.case_number_of_bottles,
                        cases : 0,
                        items : 0,
                        broughtPrice : 0,
                        sellingPrice : 0
                    }
                    rows.push(temp);
                });
                setRows(rows);
            }
        }
        setLoading(false);
    }catch(error){
        // console.log(error);
        setLoading(false);
    }
}

export const publishInputStockTransaction = async (transactionId , items , setTransactionId , setLoading , setInvoiceNumber , setEditedRow) => {
    try{
        const data = await api.addItemsToStock(transactionId , items);
        openSuccessDialog(data.data.status , data.data.comment);
        setLoading(false);
        setTransactionId(null);
        setInvoiceNumber('');
        setEditedRow([]);
    }catch(error){
        // console.log(error);
        setLoading(false);
    }
}