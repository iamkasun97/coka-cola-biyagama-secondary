import * as React from 'react';
import Grid from '@mui/material/Grid';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import dayjs from 'dayjs';
import { useState  , useEffect} from 'react';
import TextField from '@mui/material/TextField';
import SelectRoutesCustom from '../CustomComponent/SelectRoutesCustom';
// import SelectCustomNew from '../CustomComponent/SelectCustomNew';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import { DataGrid , GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import ExcelExport3 from '../CustomComponent/ExcelExport3';
import { monthlyPurchaseSummary } from '../../Services/ReportService';
import Loader from '../Loader';
import SelectCustom from '../CustomComponent/SelectCustom';

  function CustomToolbar(props) {

    return (
      <GridToolbarContainer>
        <ExcelExport3 props={props}/>
        <GridToolbarExport 
        csvOptions={{
            fileName: new Date(),
        }}
        />
      </GridToolbarContainer>
    );
  }


function MonthlyPurchaseSummary(){

    const [visible , setVisible] = useState(false);
    const [rows , setRows] = useState([]);
    const [count , setCount] = useState(0);
    const [routes , setRoutes] = useState([]);
    const [routesIds , setRouteIds] = useState([]);
    const [loading , setLoading] = useState(false);
    const [currentPage , setCurrentPage] = useState(0);
    const [postsPerPage , setPostsPerPage] = useState(5);
    const [product , setProduct] = useState('');


    const [date1 , setDate1] = useState(dayjs().format('YYYY-MM'));
    const handleDateChange1 = (newValue) => {
        setDate1(dayjs(newValue).format('YYYY-MM'));
    };

    const [date2 , setDate2] = useState(dayjs().format('YYYY-MM'));
    const handleDateChange2 = (newValue) => {
        setDate2(dayjs(newValue).format('YYYY-MM'));
    }


    const handleStart = () => {
        setVisible(true);
        setLoading(true);
        monthlyPurchaseSummary(date1 , date2 , currentPage , postsPerPage , setLoading , setRows , setCount);
    }

    //pagination functions
    const handleChangePage = (currentPage) => {
        setCurrentPage(currentPage);
        monthlyPurchaseSummary(date1 , date2 , currentPage , postsPerPage , setLoading , setRows , setCount);
    }

    const handleChangePageSize = (postsPerPage) => {
        setPostsPerPage(postsPerPage);
        monthlyPurchaseSummary(date1 , date2 , currentPage , postsPerPage , setLoading , setRows , setCount);
    }
    return (
        <div style={styles.mainDiv}>
            <Grid container spacing={3} >
                <Grid item xs={12} sm={4} >
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DatePicker 
                            views={['month', 'year']} 
                            value={date1}
                            openTo="month"
                            onChange={handleDateChange1}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={4} >
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <DatePicker 
                            views={['month', 'year']} 
                            value={date2}
                            openTo="month"
                            onChange={handleDateChange2}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={4} >
                    {
                    //   visible ? <Button variant="contained" style={{width : '100%' , height : '100%'}} onClick={() => handleCancel()}>Cancel</Button>
                    //   : 
                      <Button variant="contained" style={{width : '100%' , height : '100%'}} disabled={loading ||  date1 === 'Invalid Date' || date2 === 'Invalid Date'} onClick={() => handleStart()}>Search</Button>
                    }
                </Grid>
            </Grid>
            {
            visible ? 
            <div >
                <Box  style={styles.tableBox}>
                    <DataGrid
                        pagination
                        paginationMode="server"
                        rowCount={count}
                        rows={rows}
                        columns={columns}
                        pageSize={postsPerPage}
                        page={currentPage}
                        onPageSizeChange={(newValue) => handleChangePageSize(newValue)}
                        onPageChange={(newValue) => handleChangePage(newValue)}
                        rowsPerPageOptions={[5 , 10]}
                        experimentalFeatures={{ newEditingApi: true }}
                        components={{
                            Toolbar: CustomToolbar,
                        }}
                        componentsProps={{ 
                            toolbar: { function : monthlyPurchaseSummary , date1 , date2 , count}
                        }}
                    />
                </Box>
            </div> : null
            }
            {loading && <Loader />}
        </div>
    )
}

export default MonthlyPurchaseSummary;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    buttonDiv : {
        marginTop : '10px'
    },
    tableBox : {
        marginTop : '10px',
        width : '100%',
        height : '400px'
    },
    scroll : {
        overflowX : 'scroll',
        display : 'flex',
        justifyContent : 'center',
        marginTop : '10px'
    }
}

const columns = [
    { field: 'month', 
    headerName: 'month', 
    width: 180, 
    editable: false ,
    },
    { 
        field: 'totalItemsPurchased', 
        headerName: 'total items purchased' ,
        width: 180, 
        editable: false ,
    },
    { 
        field: 'purchasedCost', 
        headerName: 'purchased cost' ,
        width: 180, 
        editable: false ,
    },
    { 
        field: 'totalItemsReturned', 
        headerName: 'total items returned' ,
        width: 180, 
        editable: false ,
    },
    { 
        field: 'returnItemsCost', 
        headerName: 'return items cost' ,
        width: 180, 
        editable: false ,
    },
  ];