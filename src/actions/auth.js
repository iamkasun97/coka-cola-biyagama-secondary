import * as api from "../api/index";
import { AUTH, FORGET_USER, REMEMBER_USER } from "./constants";

export const login = async (formData, setLoading, navigate , dispatch) => {
  try {
    //login the user
    let data = await api.login(formData);
    // if (formData.rememberMe) {
    //   let rememberUserData = {
    //     usuario: formData.username,
    //     clave: formData.password,
    //     recuerdame: formData.rememberMe,
    //   };
    //   dispatch({ type: REMEMBER_USER, rememberUserData });
    // } else {
    //   dispatch({ type: FORGET_USER });
    // }

    dispatch({ type: AUTH, data });
    setLoading(false);
    navigate("/home");
  } catch (error) {
    // console.log(error);
    setLoading(false);
  }
};
