import { useState , useEffect} from 'react';
import Grid from '@mui/material/Grid';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import dayjs from 'dayjs';
import Button from '@mui/material/Button';
import SelectCustom from '../CustomComponent/SelectCustom';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import {InputAdornment} from "@mui/material";
import { loadRouteConfigurationAndCollections } from '../../Services/ConfigurationService';
import { addRevenueCollection , searchRevenueCollection } from '../../Services/RevenueCollectionService';
import Loader from '../Loader';

function CollectionView(){

    // cash collections
    const [transactionForm , setTransactionForm] = useState({});

    const handleChangeCash = (event , index , sub_type , sub_type_value) => {
        const { value } = event.target;
        let cash = transactionForm.cash;
        cash[index] = { sub_type: sub_type, value: sub_type_value, amount: Number(value === '' ? 0 : value) }
        setTransactionForm({ ...transactionForm, cash: cash });
    }

    const handleChageOthers = (event , index , sub_type) => {
        const { value } = event.target;
        if(isNaN(value)){}
        else {
            let others = transactionForm.others;
            // others[index] = { sub_type: sub_type, value: Number(value === '' ? 0 : value), amount: Number(value === '' ? 0 : value) }
            others[index] = { sub_type: sub_type, value: value, amount: value }
            setTransactionForm({ ...transactionForm, others: others });
        }
    }

    const calculateTotal = () => {
        let cashTotal = 0;
        transactionForm?.cash?.map(item => {
            cashTotal +=  Number(item.amount) * Number(item.value);
        });

        let othersTotal1 = 0;
        transactionForm?.others?.map((item) => {
            if(item.sub_type === 'Credit Collections'){
                othersTotal1 -= Number(item.amount);
            }else {
                othersTotal1 += Number(item.amount);
            }
        });

        return cashTotal + othersTotal1;
    }

    const calculateCashTotal = () => {
        let cashTotal = 0;

        transactionForm?.cash?.map(item => {
            cashTotal +=  Number(item.amount) * Number(item.value);
        });

        return cashTotal;
    }

    const [visible , setVisible] = useState(false);
    const [routes , setRoutes] = useState([]);
    const [issueNumbers , setIssueNumbers] = useState([{id : 1 , value : 1} , {id : 2 , value : 2} , {id : 3 , value : 3} , {id : 4 , value : 4} , {id : 5 , value : 5}]);
    const [loading , setLoading] = useState(false);
    const [salesData , setSalesData] = useState({totalSales : 0 , totalProfit : 0});

    const [date , setDate] = useState(dayjs().format('YYYY-MM-DD'));
    const handleDateChange = (newValue) => {
        setDate(dayjs(newValue).format('YYYY-MM-DD'));
    };

    useEffect(() => {
        loadRouteConfigurationAndCollections(setRoutes , setTransactionForm);
    },[]);

      const [route , setRoute] = useState('');
      const [issueNumber , setIssueNumber] = useState('');

    const handleStart = () => {
        setLoading(true);
        searchRevenueCollection(date , route , issueNumber , setLoading , setTransactionForm , setVisible , setSalesData);
    }

    const handleCancel = () => {
        setVisible(false);
        setRoute('');
        setIssueNumber('');
        setSalesData({
            totalSales : 0,
            totalProfit : 0
        })
        loadRouteConfigurationAndCollections(setRoutes , setTransactionForm);
    }

    const handleSubmit = () => {
        window.print();
    }

    return (
        <div style={styles.mainDiv}>
            <Grid container spacing={3} >
                <Grid item xs={12} sm={3} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                    <DesktopDatePicker
                        inputFormat="MM/DD/YYYY"
                        value={date}
                        onChange={handleDateChange}
                        renderInput={(params) => <TextField {...params} />}
                    />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={3} >
                    <SelectCustom name="routes" items={routes} setValue={setRoute} initialValue={route}/>
                </Grid>
                <Grid item xs={12} sm={3} >
                    <SelectCustom name="issue number" items={issueNumbers} setValue={setIssueNumber} initialValue={issueNumber}/>
                </Grid>
                <Grid item xs={12} sm={3} >
                    {
                      visible ? <Button variant="contained" style={{width : '100%' , height : '100%'}} onClick={() => handleCancel()}>Cancel</Button>
                      : 
                      <Button variant="contained" style={{width : '100%' , height : '100%'}} disabled={loading || route === '' || issueNumber === '' || date === 'Invalid Date'} onClick={() => handleStart()}>Start</Button>
                    }
                </Grid>
            </Grid>
            <hr color='black'/>
            {
                visible ? 
                <Card sx={{ maxWidth: 1000 }} style={styles.card}>
            <Grid item container direction="row" columnSpacing={2} marginTop={2} paddingLeft={2} paddingRight={2}>
                <Grid item xs={12} sm={12} md={3} lg={3}>
                    <Typography variant="h5" fontWeight={'bold'} gutterBottom>
                        Cash:
                    </Typography>
                </Grid>
                    <Grid item xs={12} sm={12} md={9} lg={9} columnSpacing={2}>
                        {transactionForm?.cash?.map((row, index) => (<Grid key={index} container direction="row" columnSpacing={2} marginY={2}>
                            <Grid item xs={12} sm={12} md={2} lg={2}>
                                <Typography variant="h6" gutterBottom>
                                    {row.sub_type}
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={1} lg={1}>
                                <Typography variant="h6" gutterBottom>
                                    x
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={4} lg={4}>
                                <TextField
                                    disabled={true}
                                    variant="outlined"
                                    size="small"
                                    type="number"
                                    value={row.amount}
                                    onChange={e => handleChangeCash(e, index, row.sub_type, row.value)}
                                    label="Amount"
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12} sm={12} md={1} lg={1} style={{ display: 'flex', justifyContent: 'center' }}>
                                <Typography variant="h6" gutterBottom>
                                    =
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={3} lg={3} style={{ display: 'flex', justifyContent: 'center' }}>
                                <Typography variant="h6" gutterBottom>
                                    {/* {currencyFormat(row.value * row.amount, 'Rs ')} */}
                                    {row.value * row.amount}
                                </Typography>
                            </Grid>
                        </Grid>))}
                        <Grid container spacing={3} >
                        <Grid item xs={12} sm={12} md={4} >
                                <Typography variant="h6" gutterBottom>
                                    Cash Total : 
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={1} >
                                
                            </Grid>
                            <Grid item xs={12} sm={12} md={2} >

                            </Grid>
                            <Grid item xs={12} sm={12} md={1}  style={{ display: 'flex', justifyContent: 'center' }}>
                                <Typography variant="h6" gutterBottom>
                                    =
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={3}  style={{ display: 'flex', justifyContent: 'center' }}>
                                <Typography variant="h6" gutterBottom>
                                {calculateCashTotal()}
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item container direction="row" columnSpacing={2} marginTop={2}>
                        {transactionForm?.others?.map((row, index) => (<Grid key={index} item container direction="row" columnSpacing={2} marginY={2}>
                                    <Grid item xs={12} sm={12} md={3} lg={3}>
                                        <Typography variant="h5" fontWeight={'bold'} gutterBottom>
                                            {row.sub_type}:
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} sm={12} md={3} lg={3}>
                                        <TextField
                                            variant="outlined"
                                            size="small"
                                            // type="number"
                                            disabled={true}
                                            value={row.amount}
                                            onChange={e => handleChageOthers(e, index, row.sub_type)}
                                            label="Amount"
                                            fullWidth
                                            InputProps={{
                                                startAdornment: <InputAdornment position="start">Rs </InputAdornment>,
                                            }}
                                        />
                                    </Grid>
                                    </Grid>))}
                        </Grid>
                        <Grid item container direction="row" columnSpacing={2} marginTop={2}>
                            <Grid item xs={12} sm={3} md={3} lg={3}>
                                <Typography variant="h5" fontWeight={'bold'} gutterBottom>
                                    Total:
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={3} md={3} lg={3}>
                                <Typography variant="h5" fontWeight={'bold'} gutterBottom>
                                    {calculateTotal()}
                                </Typography>
                            </Grid>
                        </Grid>
                        <>
                            <Grid item container direction="row" columnSpacing={2} marginTop={2}>
                                <Grid item xs={12} sm={3} md={3} lg={3}>
                                    <Typography variant="h5" fontWeight={'bold'} gutterBottom>
                                        Total Sales:
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} sm={3} md={3} lg={3}>
                                    <Typography variant="h5" fontWeight={'bold'} gutterBottom>
                                        {salesData.totalSales}
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid item container direction="row" columnSpacing={2} marginTop={2}>
                                <Grid item xs={12} sm={3} md={3} lg={3}>
                                    <Typography variant="h5" fontWeight={'bold'} gutterBottom>
                                        Variation:
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} sm={3} md={3} lg={3}>
                                    <Typography variant="h5" fontWeight={'bold'} gutterBottom>
                                        {parseFloat((Number(salesData.totalSales))-calculateTotal()).toFixed(2)}
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Grid item container direction="row" columnSpacing={2} marginTop={2}>
                                <Grid item xs={12} sm={3} md={6} lg={9}>
                                    
                                </Grid>
                                <Grid item xs={12} sm={3} md={6} lg={3}>
                                    <div style={styles.buttonDiv}>
                                        <Button variant="contained" style={{width : '100%' , height : '55px'}} onClick={() => handleSubmit()}>Download</Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </>
                </Grid>
            </Card> : null
            }
            {loading && <Loader />}
        </div>
    )
}

export default CollectionView;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    card: {
        margin: 'auto'
    },
    buttonDiv : {
        marginBottom : '20px'
    }
}

