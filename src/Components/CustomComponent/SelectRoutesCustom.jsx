import * as React from 'react';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import ListItemText from '@mui/material/ListItemText';
import Select from '@mui/material/Select';
import Checkbox from '@mui/material/Checkbox';



export default function MultipleSelectCheckmarks(props) {
  const [personName, setPersonName] = React.useState([]);
  const handleChange = (event) => {
    let temp = event.target.value;
    if(event.target.value.includes('All')){
      temp = props.routes?.map(item => {
        return item.id;
      });

      temp.push('All');
    }
    
    setPersonName(
      temp
    );
    

    props.setRouteIds(temp.filter(item => item != 'All'));
  };
  return (
    <div>
      <FormControl  style={{width : '100%'}}>
        <InputLabel id="demo-multiple-checkbox-label">Routes</InputLabel>
        <Select
          labelId="demo-multiple-checkbox-label"
          id="demo-multiple-checkbox"
          multiple
          value={personName}
          onChange={handleChange}
          input={<OutlinedInput label="Tag" />}
          renderValue={(selected) => selected.join(',')}
        >
          <MenuItem key={'All'} value={'All'}>
              <Checkbox checked={personName.indexOf('All') > -1} />
              <ListItemText primary={'All'} />
          </MenuItem>
          {props.routes?.map((route) => (
            <MenuItem key={route.id} value={route.id}>
              <Checkbox checked={personName.indexOf(route.id) > -1} />
              <ListItemText primary={route.route_name} />
            </MenuItem>
          ))}
          
        </Select>
      </FormControl>
    </div>
  );
}