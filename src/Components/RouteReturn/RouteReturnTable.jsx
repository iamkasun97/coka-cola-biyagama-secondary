import Grid from '@mui/material/Grid';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';
import TextField from '@mui/material/TextField';
import * as React from 'react';
import Button from '@mui/material/Button';
import SelectCustom from '../CustomComponent/SelectCustom';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import { DataGrid } from '@mui/x-data-grid';
import Box from '@mui/material/Box';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { loadRouteConfiguration } from '../../Services/ConfigurationService';
import { startReturnRouteTransactions , publishRouteReturnTransaction } from '../../Services/RouteReturnService';
import Loader from '../Loader';
import { darken, lighten } from '@mui/material/styles';
import Test from './test';

const useFakeMutation = () => {
    return React.useCallback(
      (item) =>
        new Promise((resolve, reject) =>
          setTimeout(() => {
            if(item.itemIssued === '' || isNaN(item.itemIssued)){
              reject(new Error('Enter valid value for items issued!!'));
            }
            if(item.casesIssued === '' || isNaN(item.casesIssued)){
              reject(new Error('Enter valid value for cases issued!!'));
            }
            if(item.returnedCases === '' || isNaN(item.returnedCases)){
              reject(new Error('Enter valid value for returned cases!!'));
            }
            if(item.returnedItems === '' || isNaN(item.returnedItems)){
              reject(new Error('Enter valid value for returned items!!'));
            }
            resolve({...item , name : item.id});
          }, 200),
        ),
      [],
    );
  };

  function CustomFooterStatusComponent(props) {
    return (
      <Box  style={{    marginTop : '10px' , marginBottom : '10px' }}>
        <Grid container spacing={3} >
                <Grid item xs={12} sm={3} >
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                  Cases Issued - {props.sumCases}
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Items Issued - {props.sumItems}
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Total Item Issued - {props.total}
                </Grid>
        </Grid>
        <Grid container spacing={3} >
                <Grid item xs={12} sm={3} >
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                   Returned Issued Cases - {props.sumReturnedCases}
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Returned Issued Items - {props.sumReturnedItems}
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Total Returned Items - {props.totalReturns}
                </Grid>
        </Grid>
      </Box>
    );
  }

function RouteReturn(){

    const [sumCases , setSumCases] = useState(0);
    const [sumItems , setSumItems] = useState(0);
    const [total , setTotal] = useState(0);
    const [sumReturnedCases , setSumReturnedCases] = useState(0);
    const [sumReturnedItems , setSumReturnedItems] = useState(0);
    const [totalReturns , setTotalReturns] = useState(0);
    const [error , setError] = useState(false);

    // table data
    const [editedRow , setEditedRow] = useState([]);   
    const mutateRow = useFakeMutation();

    const [snackbar, setSnackbar] = React.useState(null);

    const handleCloseSnackbar = () => setSnackbar(null);

    const processRowUpdate = React.useCallback(
        async (newRow) => {
        // Make the HTTP request to save in the backend
        const response = await mutateRow(newRow);
        setEditedRow(prevState => [...prevState , {
          id : response.id,
          totalItemsIssued : Number(response.caseType)*Number(response.returnedCases) + Number(response.returnedItems)
        }]);
        setSnackbar({ children: 'Data successfully Added to Submit', severity: 'success' });
        setError(false);
        return response;
        },
        [mutateRow],
    );

    const handleProcessRowUpdateError = React.useCallback((error) => {
        setSnackbar({ children: error.message, severity: 'error' });
        setError(true);
    }, []);

    const [issueNumbers , setIssueNumbers] = useState([{id : 1 , value : 1} , {id : 2 , value : 2} , {id : 3 , value : 3} , {id : 4 , value : 4} , {id : 5 , value : 5}]);
    const [routes , setRoutes] = useState([]);
    useEffect(() => {
      loadRouteConfiguration(setRoutes);
    },[]);

    const [transactionId , setTransactionId] = useState(null);
    const [loading , setLoading] = useState(false);
    const [date , setDate] = useState(dayjs().format('YYYY-MM-DD'));
    const [route , setRoute] = useState('');
    const [issueNumber , setIssueNumber] = useState('');

    const handleDateChange = (newValue) => {
      setDate(dayjs(newValue).format('YYYY-MM-DD'));
    };

  const [rows , setRows] = useState([]);

  const handleStart = () => {
    setLoading(true);
    startReturnRouteTransactions(date , route , issueNumber , setLoading, setTransactionId , setRows);
  }

  const handleCancel = () => {
    setTransactionId(null);
    setIssueNumber('');
    setRoute('');
    setEditedRow([]);
  }

  const handleSubmit = () => {
    let items = [];
    let ids = editedRow.map(item => item.id);
    let unique = ids.filter((v, i, a) => a.indexOf(v) === i);
    unique.forEach(i => {
      let temp = editedRow.filter(e => e.id === i);
      temp = temp.reverse();
      items.push({
        stock_summary_data_id : temp[0].id,
        total_bottle_quantity : temp[0].totalItemsIssued,
      });
    });
    rows.forEach(e => {
      if(unique.includes(e.id) !== true){
        items.push({
          stock_summary_data_id : e.id,
          total_bottle_quantity : 0
        });
      }
    })
    setLoading(true);
    publishRouteReturnTransaction(transactionId , items , setTransactionId , setLoading , setRoute , setIssueNumber , setEditedRow);
  }

    return (
        <div style={styles.mainDiv}>
            <Grid container spacing={3} >
                <Grid item xs={12} sm={3} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                    <DesktopDatePicker
                        inputFormat="MM/DD/YYYY"
                        value={date}
                        onChange={handleDateChange}
                        renderInput={(params) => <TextField {...params} />}
                    />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={3} >
                <SelectCustom name={"routes"} items={routes} setValue={setRoute} initialValue={route}/>
                </Grid>
                <Grid item xs={12} sm={3} >
                <SelectCustom name={"issue number"} items={issueNumbers} setValue={setIssueNumber} initialValue={issueNumber}/>
                </Grid>
                <Grid item xs={12} sm={3} >
                    {
                      transactionId !== null ? <Button variant="contained" style={{width : '100%' , height : '100%'}} onClick={() => handleCancel()}>Cancel</Button>
                      : 
                      <Button variant="contained" style={{width : '100%' , height : '100%'}} disabled={loading || route === '' || issueNumber === '' || date === 'Invalid Date'} onClick={() => handleStart()}>Start</Button>
                    }
                </Grid>
            </Grid>
            <hr color='black'/>
            {
              transactionId !== null ? 
              <div>
                  <Box sx={{ 
                          height: 400, 
                          width: '100%' ,
                          '& .super-app-theme--Open': {
                            bgcolor: (theme) =>
                              darken('#FAF2AA', 0.005),
                            '&:hover': {
                              bgcolor: (theme) =>
                              darken('#FAF2AA', 0.005),
                            },
                          },
                          '& .super-app-theme--cell': {
                            backgroundColor: '#CCF1F1',
                            color: '#1a3e72',
                            fontWeight: '600',
                          }
                          }}>
                    <DataGrid
                        rows={rows}
                        columns={columns}
                        processRowUpdate={processRowUpdate}
                        onProcessRowUpdateError={handleProcessRowUpdateError}
                        experimentalFeatures={{ newEditingApi: true }}
                        onStateChange={(state) => {
                          let sumCasesIssued = 0;
                          let sumItemIssued = 0;
                          let total = 0;
                          let sumReturnedCasesIssued = 0;
                          let sumReturnedItemsIssued = 0;
                          let totalRetuns = 0;
                          state.rows.ids.map(item => {
                            sumCasesIssued += Number(state.rows.idRowsLookup[item].casesIssued);
                            sumItemIssued += Number(state.rows.idRowsLookup[item].itemIssued);
                            total += Number(state.rows.idRowsLookup[item].totalItemsIssued);
                            sumReturnedCasesIssued += Number(state.rows.idRowsLookup[item].returnedCases);
                            sumReturnedItemsIssued += Number(state.rows.idRowsLookup[item].returnedItems);
                            totalRetuns += Number(state.rows.idRowsLookup[item].returnedCases)*Number(state.rows.idRowsLookup[item].caseType) + Number(state.rows.idRowsLookup[item].returnedItems);
                          });
                          setSumCases(sumCasesIssued);
                          setTotal(total);
                          setSumItems(sumItemIssued);
                          setSumReturnedCases(sumReturnedCasesIssued);
                          setSumReturnedItems(sumReturnedItemsIssued);
                          setTotalReturns(totalRetuns);
                        }}
                        components={{
                          Footer: CustomFooterStatusComponent,
                        }}
                        componentsProps={{
                          footer: { total , sumItems , sumCases , sumReturnedCases , sumReturnedItems , totalReturns},
                        }}
                        sx={{
                          '& .MuiDataGrid-cell:click': {
                            color: 'black',
                          },
                        }}
                        getRowClassName={(params) => `super-app-theme--${params.row.status}`}
                    />
                    {!!snackbar && (
                        <Snackbar
                        open
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                        onClose={handleCloseSnackbar}
                        autoHideDuration={6000}
                        >
                        <Alert {...snackbar} onClose={handleCloseSnackbar} />
                        </Snackbar>
                    )}
                  </Box>
                  <Grid container spacing={3} >
                      <Grid item xs={12} sm={9} >
                      </Grid>
                      <Grid item xs={12} sm={3} >
                          <div style={styles.buttonDiv}>
                          <Button variant="contained" style={{width : '100%' , height : '55px'}} disabled={error} onClick={() => handleSubmit()}>Submit</Button>
                          </div>
                      </Grid>
                  </Grid>
              </div> : null
            }
            {loading && <Loader />}
        </div>
    )
}

export default RouteReturn;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    buttonDiv : {
        marginTop : '10px'
    }
}

const columns = [
    { field: 'item', headerName: 'Item', width: 160, editable: false },
    {
      field: 'caseType',
      headerName: 'Case Type',
      width: 140,
      editable: false,
    },
    {
      field: 'broughtPrice',
      headerName: 'brought price',
      width: 140,
      editable: false,
    },
    {
      field: 'sellingPrice',
      headerName: 'selling price',
      width: 140,
      editable: false,
    },
    {
      field: 'casesIssued',
      headerName: 'Cases Issued',
      width: 140,
      editable: false,
    },
    {
      field: 'itemIssued',
      headerName: 'Item Issued',
      width: 140,
      editable: false,
    },
    {
      field: 'totalItemsIssued',
      headerName: 'Total Items Issued',
      width: 140,
      editable: false,
    },
    {
      field: 'returnedCases',
      headerName: 'Returned Cases',
      width: 140,
      editable: true,
      cellClassName: 'super-app-theme--cell',
    },
    {
        field: 'returnedItems',
        headerName: 'Returned Items',
        width: 140,
        editable: true,
        cellClassName: 'super-app-theme--cell',
      },
    {
        field: 'totalReturns',
        headerName: 'Total Returns',
        width: 140,
        editable: false,
        renderCell : (params) => {
          if(params.row.caseType === undefined || params.row.returnedCases === undefined || params.row.returnedItems === undefined){
            return 0
          }
          return Number(params.row.caseType) * Number(params.row.returnedCases) + Number(params.row.returnedItems);
        }
      },
  ];