import {useState , useEffect} from 'react';
import PieChartNew from './PieChartNew';
import CurrentStock from './CurrentStock';
import Chart from './Chart';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import PaidIcon from '@mui/icons-material/Paid';
import LocalMallSharpIcon from '@mui/icons-material/LocalMallSharp';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import dayjs from 'dayjs';
import TextField from '@mui/material/TextField';
import { getDashBoardData } from '../../Services/DashBoardService';
import salesContext from '../../context/salesContext';
import Button from '@mui/material/Button';
import PieChart1 from './PieChartNew2';

function Home (){

    const [loading , setLoading] = useState(false);
    const [totSales , setTotSales] = useState(0);
    const [totProfit , setTotProfit] = useState(0);
    const [date1 , setDate1] = useState(dayjs().subtract(30 , 'days').format('YYYY-MM-DD'));
    const handleDateChange1 = (newValue) => {
        setDate1(dayjs(newValue).format('YYYY-MM-DD'));
    };

    const [date2 , setDate2] = useState(dayjs().format('YYYY-MM-DD'));
    const handleDateChange2 = (newValue) => {
        setDate2(dayjs(newValue).format('YYYY-MM-DD'));
    };

    const [sales , setSales] = useState([]);
    const [routeSales , setRouteSales] = useState([]);
    const [colors , setColors] = useState([]);

    useEffect(() => {
        getDashBoardData(date1 , date2 , setSales , setRouteSales , setTotSales , setTotProfit , setColors);
    },[]);

    const handleStart = () => {
        getDashBoardData(date1 , date2 , setSales , setRouteSales , setTotSales , setTotProfit , setColors);
    }

    return (
        <salesContext.Provider value={{sales , routeSales , colors}}>
            <div style={styles.wrapDiv}>
            <div style={{marginTop : '20px' , marginBottom : '20px'}} >
            <Grid container spacing={3} >
                <Grid item xs={6} sm={3} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                        <DesktopDatePicker
                            inputFormat="MM/DD/YYYY"
                            value={date1}
                            onChange={handleDateChange1}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={6} sm={3} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                        <DesktopDatePicker
                            inputFormat="MM/DD/YYYY"
                            value={date2}
                            onChange={handleDateChange2}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={6} sm={3} >
                <Button variant="contained" style={{width : '100%' , height : '100%'}} disabled={loading} onClick={() => handleStart()}>Start</Button>
                </Grid>
            </Grid>
            </div>
            <Grid container spacing={3} >
                <Grid item xs={6} sm={3} >
                    <div style={styles.totalDiv}>
                        <Grid container spacing={3} style={styles.totalGrid}>
                            <Grid item xs={12} sm={8} >
                                <Typography variant="h8" gutterBottom style={{color : 'white'}}>
                                    total sales
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={4} >
                                {/* <Icon>star</Icon> */}
                                <LocalMallSharpIcon sx={{ color: 'white' }}/>
                            </Grid>
                            <Grid item xs={12} sm={12} style={{color : 'white'}}>
                                <Typography variant="h8" gutterBottom>
                                    LKR {new Intl.NumberFormat().format(totSales)}
                                </Typography>
                            </Grid>
                        </Grid>
                    </div>
                </Grid>
                <Grid item xs={6} sm={3} >
                <div style={styles.salesDiv}>
                        <Grid container spacing={3} style={styles.salesGrid}>
                            <Grid item xs={12} sm={8} >
                                <Typography variant="h8" gutterBottom >
                                    total profit
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={4} >
                            <PaidIcon />
                            </Grid>
                            <Grid item xs={12} sm={12} >
                                <Typography variant="h8" gutterBottom>
                                    LKR {new Intl.NumberFormat().format(totProfit)}
                                </Typography>
                            </Grid>
                        </Grid>
                    </div>
                </Grid>
            </Grid>
            </div>
            <Grid container spacing={3} >
            <Grid item xs={12} sm={7} >
              <Chart />
            </Grid>
            <Grid item xs={12} sm={5} >
                <PieChart1 />
            </Grid>
        </Grid>
        <CurrentStock />
        </salesContext.Provider>
    )
}

const styles = {
    wrapDiv : {
        margin : '20px'
    },
    totalDiv : {
        border : '1px black solid',
        borderRadius : '10px',
        backgroundColor : 'black'
    },
    totalGrid : {
        padding : '10px'
    },
    salesDiv : {
        border : '1px black solid',
        borderRadius : '10px'
    },
    salesGrid : {
        padding : '10px'
    }
}

export default Home;