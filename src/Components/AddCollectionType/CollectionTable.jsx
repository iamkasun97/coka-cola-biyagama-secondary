import { useEffect, useMemo, useState } from 'react';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { DataGrid } from '@mui/x-data-grid';
import Box from '@mui/material/Box';
import EditChangesPopUp from './EditChangesPopUp';
import { getCollectionTypes , addCollectionType } from '../../Services/CollectionService';
import Loader from '../Loader';
import * as Yup from "yup";
import { Form, Formik } from "formik";
import Textfield from '../FormsUI/Textfield/index';
import ConfirmDeleteButton from '../CustomComponent/ConfirmDeleteButton';

function CollectionTable(){

    const [loading , setLoading] = useState(false);
    const [currentPage , setCurrentPage] = useState(0);
    const [postsPerPage , setPostsPerPage] = useState(5);
    const [collections , setCollections] = useState([]);
    const [count , setCount] = useState(0);


    useEffect(() => {
        getCollectionTypes(currentPage , postsPerPage , setCollections , setCount);
    },[currentPage , postsPerPage]);

    const columns = useMemo(
        () => [
                  { field: 'collection_type', headerName: 'Collection Type', width: 200 , editable : 'false'},
                  { field: 'collection_sub_type', headerName: 'Collection Sub Type', width: 300 , editable : 'false'},
                  { field: 'value', headerName: 'Value', width: 140 , editable : 'false'},
                  {
                    field: 'edit',
                    headerName: '',
                    width: 100,
                    renderCell: (params) => {
                        return <EditChangesPopUp data={params} setCollections={setCollections} setCount={setCount} postsPerPage={postsPerPage} currentPage={currentPage}/>
                    }
                  },
        ],[currentPage , postsPerPage]
    );

    // form validation
    const INITIAL_FORM_STATE = {
        collectionType:  "",
        collectionSubType:  "",
        value : ""
    };

    const FORM_VALIDATION = Yup.object().shape({
        collectionType : Yup.string()
        .required('Please Enter collection type'),
        collectionSubType : Yup.string()
        .required('Please Enter collection subtype'),
        value : Yup.number().required('please enter a value')
      });


    return (
        <div style={styles.mainDiv}>
            <Card sx={{ maxWidth: 750 }} style={styles.card}>
                <div style={styles.insideDiv}>
                    <Grid container spacing={3} >
                        <Grid item xs={12} sm={6} >
                            <Typography variant="h5" fontWeight={'bold'} gutterBottom>
                                Add New Collection Type
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                            <Typography variant="h8" fontWeight={'bold'} gutterBottom>
                                Collection Type Details
                            </Typography>
                        </Grid>
                    </Grid>
                    <Formik
                    initialValues={{
                        ...INITIAL_FORM_STATE,
                    }}
                    validationSchema={FORM_VALIDATION}
                    onSubmit={(values , {resetForm}) => {
                        setLoading(true);
                        addCollectionType(values.collectionType , values.collectionSubType , values.value , currentPage , postsPerPage, setCollections , setCount , setLoading)
                        resetForm()
                    }}
                    >
                    <Form>
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={4} >
                        <Textfield
                            type={"text"}
                            name="collectionType"
                            label="Collection Type"
                            placeholder="Collection Type"
                        />
                        </Grid>
                        <Grid item xs={12} sm={4} >
                        <Textfield
                            type={"text"}
                            name="collectionSubType"
                            label="Collection Sub Type"
                            placeholder="Collection Sub Type"
                        />
                        </Grid>
                        <Grid item xs={12} sm={4} >
                        <Textfield
                            type={"text"}
                            name="value"
                            label="value"
                            placeholder="value"
                        />
                        </Grid>
                    </Grid>  
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                            <Button variant="contained" style={{width : '100%' , height : '55px'}} type='submit'>Submit</Button>
                        </Grid>
                        <Grid item xs={12} sm={6} >
                            <Button variant="contained" style={{width : '100%' , height : '55px'}} type='reset'>Cancel</Button>
                        </Grid>
                    </Grid> 
                    </Form>
                    </Formik>
                    </div>
                    <Box sx={{ height: 400, width: '100%' }}>
                        <DataGrid
                            pagination
                            paginationMode="server"
                            rowCount={count}
                            rows={collections}
                            columns={columns}
                            pageSize={postsPerPage}
                            page={currentPage}
                            onPageSizeChange={(newValue) => setPostsPerPage(newValue)}
                            onPageChange={(newValue) => setCurrentPage(newValue)}
                            rowsPerPageOptions={[5 , 10]}
                            experimentalFeatures={{ newEditingApi: true }}
                        />
                    </Box>
            </Card>
            {loading && <Loader />}
        </div>
    )
}

export default CollectionTable;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    card: {
        margin: 'auto'
    },
    insideDiv : {
        padding : '40px'
    }
}
