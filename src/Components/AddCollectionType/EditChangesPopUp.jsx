import {useState} from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import Textfield from '../FormsUI/Textfield';
import * as Yup from "yup";
import { Form, Formik } from "formik";
import { editCollectionType } from '../../Services/CollectionService';
import Loader from '../Loader';

export default function FormDialog(props) {
  const [open, setOpen] = useState(false);
  const [loading , setLoading] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  // form validation
  const INITIAL_FORM_STATE = {
    collectionType:  props.data.row.collection_type,
    collectionSubType : props.data.row.collection_sub_type,
    value : props.data.row.value
};

const FORM_VALIDATION = Yup.object().shape({
    collectionType : Yup.string().required("Please Enter collection type"),
    collectionSubType : Yup.string().required("Please Enter collection sub type"),
    value : Yup.number().required('Please Enter value'),
});

  return (
    <div>
      <Button variant="contained" onClick={handleClickOpen} style={{width : '80px' , height : '40px'}}>
        Edit
      </Button>
      <div style={{width : '1000px'}}>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Edit Collection Details</DialogTitle>
        <Formik
        initialValues={{
            ...INITIAL_FORM_STATE,
        }}
        validationSchema={FORM_VALIDATION}
        onSubmit={(values , {resetForm}) => {
            setLoading(true);
            editCollectionType(props.data.id , values.collectionType , values.collectionSubType , values.value ,props.currentPage , props.postsPerPage ,  props.setCollections, props.setCount , setLoading);
            setOpen(false);
        }}
        >
        <Form>
        <DialogContent>
          <Grid container spacing={3} >
            <Grid item xs={12} sm={4} >
                <Textfield
                    type={"text"}
                    name="collectionType"
                    label="Collection Type"
                    placeholder="Collection Type"
                />
                </Grid>
                <Grid item xs={12} sm={4} >
                <Textfield
                    type={"text"}
                    name="collectionSubType"
                    label="Collection Sub Type"
                    placeholder="Collection Sub Type"
                />
                </Grid>
                <Grid item xs={12} sm={4} >
                <Textfield
                    type={"text"}
                    name="value"
                    label="value"
                    placeholder="value"
                />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button  type='submit'>Save</Button>
        </DialogActions>
        </Form></Formik>
        {loading && <Loader />}
      </Dialog>
      </div>
    </div>
  );
}