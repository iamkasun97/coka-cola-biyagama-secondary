import * as api from '../api/index';

export const getCollectionTypes = async (currentPage , postsPerPage , setCollections , setCount) => {
    try{
        const data = await api.getCollectionTypes(currentPage , postsPerPage);
        setCollections(data.data.data.data_for_the_loop);
        setCount(data.data.data.count);
    }catch(error){

    }
}

export const addCollectionType = async (collectionType , collectionSubType , value , currentPage , postsPerPage , setCollections ,setCount, setLoading) => {
    try{
        const data = await api.addCollectionType(collectionType , collectionSubType , value);
        const dataNext = await api.getCollectionTypes(currentPage , postsPerPage);
        setCollections(dataNext.data.data.data_for_the_loop);
        setCount(dataNext.data.data.count);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const editCollectionType = async (id , collectionType , collectionSubType , value , currentPage , postsPerPage , setCollections ,setCount, setLoading) => {
    try{
        const data = await api.editCollectionType(id , collectionType , collectionSubType , value);
        const dataNext = await api.getCollectionTypes(currentPage , postsPerPage);
        setCollections(dataNext.data.data.data_for_the_loop);
        setCount(dataNext.data.data.count);

        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

