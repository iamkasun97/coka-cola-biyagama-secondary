import { useEffect, useMemo, useState } from 'react';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { DataGrid } from '@mui/x-data-grid';
import Box from '@mui/material/Box';
import EditChangesPopUp from './EditChangesPopUp';
import Loader from '../Loader';
import * as Yup from "yup";
import { Form, Formik } from "formik";
import Textfield from '../FormsUI/Textfield/index';
import { getAllRouteDetails , addNewRouteDetails , deleteRouteDetails} from '../../Services/RouteService';
import ConfirmDeleteButton from '../CustomComponent/ConfirmDeleteButton';

function RoutesTable(){

    const [loading , setLoading] = useState(false);
    const [currentPage , setCurrentPage] = useState(0);
    const [postsPerPage , setPostsPerPage] = useState(5);
    const [count , setCount] = useState(0);

    const [routes , setRoutes] = useState([]);

    useEffect(() => {
        getAllRouteDetails(currentPage , postsPerPage , setRoutes , setCount);
    },[currentPage , postsPerPage]);

    const columns = useMemo(
        () => [
                  { field: 'route_name', headerName: 'Route Name', width: 300 , editable : 'true'},
                  { field: 'route_owner', headerName: 'Route Owner', width: 300 , editable : 'true'},
                  {
                    field: 'edit',
                    headerName: '',
                    width: 100,
                    renderCell: (params) => {
                        return <EditChangesPopUp data={params} setRoutes={setRoutes} setCount={setCount} postsPerPage={postsPerPage} currentPage={currentPage}/>
                    }
                  },
                 
        ],[currentPage , postsPerPage]
    );

    // form validation
    const INITIAL_FORM_STATE = {
        routeName:  "",
        owner:  "",
    };

    const FORM_VALIDATION = Yup.object().shape({
        routeName : Yup.string()
        .required('Please Enter Route name'),
        owner : Yup.string()
        .required('Please Enter Owner name')
      });


    return (
        <div style={styles.mainDiv}>
            <Card sx={{ maxWidth: 750 }} style={styles.card}>
                <div style={styles.insideDiv}>
                    <Grid container spacing={3} >
                        <Grid item xs={12} sm={6} >
                            <Typography variant="h5" fontWeight={'bold'} gutterBottom>
                                Add New Route
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                            <Typography variant="h8" fontWeight={'bold'} gutterBottom>
                                Route Details
                            </Typography>
                        </Grid>
                    </Grid>
                    <Formik
                    initialValues={{
                        ...INITIAL_FORM_STATE,
                    }}
                    validationSchema={FORM_VALIDATION}
                    onSubmit={(values , {resetForm}) => {
                        setLoading(true);
                        // console.log(values);
                        addNewRouteDetails(values.routeName , values.owner , currentPage , postsPerPage , setRoutes , setCount , setLoading);
                        resetForm()
                    }}
                    >
                    <Form>
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                        <Textfield
                            type={"text"}
                            name="routeName"
                            label="Route Name"
                            placeholder="Route Name"
                        />
                        </Grid>
                        <Grid item xs={12} sm={6} >
                        <Textfield
                            type={"text"}
                            name="owner"
                            label="Owner Name"
                            placeholder="Owner Name"
                        />
                        </Grid>
                    </Grid>  
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                            <Button variant="contained" style={{width : '100%' , height : '55px'}} type='submit'>Submit</Button>
                        </Grid>
                        <Grid item xs={12} sm={6} >
                            <Button variant="contained" style={{width : '100%' , height : '55px'}} type='reset'>Cancel</Button>
                        </Grid>
                    </Grid> 
                    </Form>
                    </Formik>
                    </div>
                    <Box sx={{ height: 400, width: '100%' }}>
                        <DataGrid
                            pagination
                            paginationMode="server"
                            rowCount={count}
                            rows={routes}
                            columns={columns}
                            pageSize={postsPerPage}
                            page={currentPage}
                            onPageSizeChange={(newValue) => setPostsPerPage(newValue)}
                            onPageChange={(newValue) => setCurrentPage(newValue)}
                            rowsPerPageOptions={[5 , 10 , 25]}
                            experimentalFeatures={{ newEditingApi: true }}
                        />
                    </Box>
            </Card>
            {loading && <Loader />}
        </div>
    )
}

export default RoutesTable;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    card: {
        margin: 'auto'
    },
    insideDiv : {
        padding : '40px'
    }
}
