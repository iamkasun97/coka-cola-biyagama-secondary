import Grid from '@mui/material/Grid';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import * as React from 'react';
import dayjs from 'dayjs';
import { useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import Box from '@mui/material/Box';
import { DataGrid , GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import ExcelExport from '../CustomComponent/ExcelExport2';
import { getStockVariationDetails } from '../../Services/ReportService';
import Loader from '../Loader';


const useFakeMutation = () => {
    return React.useCallback(
      (user) =>
        new Promise((resolve, reject) =>
          setTimeout(() => {
            if (user.name?.trim() === '') {
              reject(new Error("Error while saving user: name can't be empty."));
            } else {
              resolve({ ...user, name: user.name});
            }
          }, 200),
        ),
      [],
    );
  };

  function CustomToolbar(props) {
    return (
      <GridToolbarContainer>
        <ExcelExport count={props.count} startDate={props.startDate} endDate={props.endDate} phase={props.phase}/>
        <GridToolbarExport 
        csvOptions={{
            fileName: new Date(),
        }}
        />
      </GridToolbarContainer>
    );
  }


function DailyStockVariation(){

    const [visible , setVisible] = useState(false);
    const [loading , setLoading] = useState(false);
    const [rows , setRows] = useState([]);
    const [phase , setPhase] = useState('afterIssue');
    const [currentPage , setCurrentPage] = useState(0);
    const [postsPerPage , setPostsPerPage] = useState(5);
    const [count , setCount] = useState(0);

    const [date1 , setDate1] = useState(dayjs().format('YYYY-MM-DD'));
    const handleDateChange1 = (newValue) => {
        setDate1(dayjs(newValue).format('YYYY-MM-DD'));
    };

    const [date2 , setDate2] = useState(dayjs().format('YYYY-MM-DD'));
    const handleDateChange2 = (newValue) => {
        setDate2(dayjs(newValue).format('YYYY-MM-DD'));
    }

    // table data
  const [editedRow , setEditedRow] = useState([]);   
  const mutateRow = useFakeMutation();

  const [snackbar, setSnackbar] = React.useState(null);

  const handleCloseSnackbar = () => setSnackbar(null);

  const processRowUpdate = React.useCallback(
      async (newRow) => {
      // Make the HTTP request to save in the backend
      const response = await mutateRow(newRow);
      let temp = editedRow;
      temp.push(response);
      setEditedRow(temp);
      setSnackbar({ children: 'Data successfully Added to Submit', severity: 'success' });
      return response;
      },
      [mutateRow],
  );

  const handleProcessRowUpdateError = React.useCallback((error) => {
      setSnackbar({ children: error.message, severity: 'error' });
  }, []);

  const handleStart = () => {
    setVisible(true);
    setLoading(true);
    getStockVariationDetails(date1 , date2 , phase , currentPage , postsPerPage , setLoading , setRows , setCount);
  }

  //pagination functions
  const handleChangePage = (currentPage) => {
    setCurrentPage(currentPage);
    getStockVariationDetails(date1 , date2 , phase ,currentPage , postsPerPage, setLoading , setRows , setCount);
}

const handleChangePageSize = (postsPerPage) => {
    setPostsPerPage(postsPerPage);
    getStockVariationDetails(date1 , date2 , phase ,currentPage , postsPerPage, setLoading , setRows , setCount);
}

    return (
        <div style={styles.mainDiv}>
            <Grid container spacing={3} >
                <Grid item xs={12} sm={3} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                        <DesktopDatePicker
                            inputFormat="MM/DD/YYYY"
                            value={date1}
                            onChange={handleDateChange1}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={3} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                        <DesktopDatePicker
                            inputFormat="MM/DD/YYYY"
                            value={date2}
                            onChange={handleDateChange2}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                </Grid>
                {/* <Grid item xs={12} sm={3} >
                </Grid> */}
                <Grid item xs={12} sm={3} >
                    <Button variant="contained" style={{width : '100%' , height : 55}} disabled={loading || date1 === 'Invalid Date' || date2 === 'Invalid Date'} onClick={() => handleStart()}>Start</Button>
                </Grid>
            </Grid>
            {
                visible ? 
                <div style={styles.scroll}>
                    <Box sx={{ height: 400, width: 600 }} style={styles.tableBox}>
                        <DataGrid
                            pagination
                            paginationMode="server"
                            rowCount={count}
                            rows={rows}
                            columns={columns}
                            pageSize={postsPerPage}
                            page={currentPage}
                            onPageSizeChange={(newValue) => handleChangePageSize(newValue)}
                            onPageChange={(newValue) => handleChangePage(newValue)}
                            rowsPerPageOptions={[5 , 10]}
                            processRowUpdate={processRowUpdate}
                            onProcessRowUpdateError={handleProcessRowUpdateError}
                            experimentalFeatures={{ newEditingApi: true }}
                            components={{
                                Toolbar: CustomToolbar,
                            }}
                            componentsProps={{ 
                                toolbar: { count: count , startDate : date1 , endDate : date2 , phase : phase} ,
                            }}
                        />
                        {!!snackbar && (
                            <Snackbar
                            open
                            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                            onClose={handleCloseSnackbar}
                            autoHideDuration={6000}
                            >
                            <Alert {...snackbar} onClose={handleCloseSnackbar} />
                            </Snackbar>
                        )}
                    </Box>
                </div> : null
            }
            {loading && <Loader />}
        </div>
    )
}

export default DailyStockVariation;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    buttonDiv : {
        marginTop : '10px'
    },
    scroll : {
        overflowX : 'scroll',
        display : 'flex',
        justifyContent : 'center',
        marginTop : '10px'
    }
}

const columns = [
    { field: 'item', headerName: 'Item' ,width: 300, editable: false },
    {
      field: 'stock',
      headerName: 'Stock',
      width: 250,
      editable: false,
    },
  ];

  const rows = [
    { 
        id: 1,
        date: '01/01/2023',
        item: 'coka cola 175ml',
        stock : 100,
    },
    { 
        id: 2,
        date: '01/01/2023',
        item: 'coka cola 175ml',
        stock : 100,
    },
    { 
        id: 3,
        date: '01/01/2023',
        item: 'coka cola 175ml',
        stock : 100,
    },
    { 
        id: 4,
        date: '01/01/2023',
        item: 'coka cola 175ml',
        stock : 100,
    },
  ];