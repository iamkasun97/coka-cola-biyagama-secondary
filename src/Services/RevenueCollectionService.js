import * as api from '../api/index';
import { openSuccessDialog } from '../Components/FormsUI/PopUps/SuccessDialog/index.tsx';
import { openErrorDialog } from "../Components/FormsUI/PopUps/ErrorDialog/index.tsx";

export const addRevenueCollection = async (date , route , issueNumber , revenueCollection ,total, setLoading , setRoute , setIssueNumber , setTransactionForm , setVisible , setSalesdata) => {
    try{
        const data = await api.addRevenueCollection(date , route , issueNumber , revenueCollection , total);
        openSuccessDialog(data.data.status , data.data.comment);

        setVisible(false);
        setRoute('');
        setIssueNumber('');
        setLoading(false);

        const dataNext = await api.loadConfiguration();
        let cash = [];
        let others = [
            {
                sub_type : 'Credit',
                value : 0,
                amount : 0
            },
            {
                sub_type : 'Banked',
                value : 0,
                amount : 0
            },
            {
                sub_type : 'Expenses',
                value : 0,
                amount : 0
            },
            {
                sub_type : 'Discounts',
                value : 0,
                amount : 0
            },
            {
                sub_type : 'Cheques',
                value : 0,
                amount : 0
            },
            {
                sub_type : 'Credit Collections',
                value : 0,
                amount : 0
            },
        ]
        dataNext.data.data.collection?.map(collection => {
            cash.push({
                sub_type : collection.collection_sub_type,
                value : Number(collection.value),
                amount : 0
            });
        });

        let collections = {
            cash : cash,
            others : others
        };
        setTransactionForm(collections);
        setSalesdata({
            totalSales : 0,
            totalProfit : 0
        });
    }catch(error){
        // console.log(error);
        setLoading(false);
    }
}

export const searchRevenueCollection = async (date , route , issueNumber , setLoading , setTransactionForm , setVisible , setSalesdata) => {
    try{
        const data = await api.searchRevenueCollection(date , route , issueNumber);
        if(data.data.status == 'failed'){
            openErrorDialog(400, 'No Transactions found to allocate collections!');
        }else {
            setSalesdata({
                totalSales : data?.data?.data?.profitIfFound.total_sales,
                totalProfit : data?.data?.data?.profitIfFound.total_profit
            })
            setVisible(true);
        }
        if(data?.data?.data?.revenue?.revenue_collection !== undefined){
            setTransactionForm(JSON.parse(data?.data?.data?.revenue?.revenue_collection));
        }
        
        
        setLoading(false);
    }catch(error){
        setLoading(false);
        // console.log(error);
    }
}