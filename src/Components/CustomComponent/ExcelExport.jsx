import {useState} from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogTitle from '@mui/material/DialogTitle';
import XLSX from 'sheetjs-style';
import { createSvgIcon } from '@mui/material/utils';
import { getDetailsForDailySales } from '../../Services/ReportService';

function ExcelExport({ count , startDate , endDate , routesIds}){

    const [open, setOpen] = useState(false);
    const [loading , setLoading] = useState(false);

      const handleClose = () => {
        setOpen(false);
      };

      const ExportIcon = createSvgIcon(
        <path d="M19 12v7H5v-7H3v7c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-7h-2zm-6 .67l2.59-2.58L17 11.5l-5 5-5-5 1.41-1.41L11 12.67V3h2z" />,
        'SaveAlt',
      );

      const buttonBaseProps = {
        color: 'primary',
        size: 'small',
        startIcon: <ExportIcon />,
    };

    const [excelData , setExcelData] = useState([]);
    const [count1 , setCount] = useState(0);

    const getReportData = () => {
        getDetailsForDailySales(startDate , endDate , routesIds , 0 , count+1 , setLoading , setExcelData , setCount);
        setOpen(true);
    }

    const exportExcelData = async () => {
        const fileExtension = '.xlsx';

        const ws = XLSX.utils.json_to_sheet(excelData);
        const wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
        XLSX.writeFile(wb, new Date() + fileExtension);

        setOpen(false);
    }

    return (
        <div>
            <Button  onClick={getReportData} {...buttonBaseProps}>
                EXPORT ALL ROWS
            </Button>
            <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                {`Want to DownLoad ${startDate} to ${endDate} Report Details?`}
                </DialogTitle>
                <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <Button onClick={exportExcelData} autoFocus>
                    Yes
                </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}

export default ExcelExport;