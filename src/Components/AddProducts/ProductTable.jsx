import { useEffect, useMemo, useState } from 'react';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { DataGrid } from '@mui/x-data-grid';
import Box from '@mui/material/Box';
import EditChangesPopUp from './EditChangesPopUp';
import { loadCaseConfigurationAndProducts } from '../../Services/ConfigurationService';
import { getProducts , addProduct } from '../../Services/ProductService';
import Textfield from '../FormsUI/Textfield';
import Select from '../FormsUI/Select';
import Loader from '../Loader';
import * as Yup from "yup";
import { Form, Formik } from "formik";



function ProductTable(){

    const [cases , setCases] = useState([]);
    const [products , setProducts] = useState([]);
    const [currentPage , setCurrentPage] = useState(0);
    const [postsPerPage , setPostsPerPage] = useState(5);
    const [count , setCount] = useState(0);
    const [loading , setLoading] = useState(false);

    useEffect(() => {
        loadCaseConfigurationAndProducts(setCases , currentPage , postsPerPage , setProducts , setCount);
    },[currentPage , postsPerPage]);

    const columns = useMemo(
        () => [
                  { field: 'item_name', headerName: 'Product Name', width: 300 , editable : 'true'},
                  { field: 'case_number_of_bottles', headerName: 'Case Type', width: 300 , editable : 'true'},
                  {
                    field: 'save',
                    headerName: '',
                    width: 100,
                    renderCell: (params) => {
                        return <EditChangesPopUp data={params}  setProducts={setProducts} setCount={setCount} currentPage={currentPage} postsPerPage={postsPerPage}/>
                    }
                    ,
                  },
        ],[currentPage , postsPerPage]
    );

    // form validation
    const INITIAL_FORM_STATE = {
        productName:  "",
        caseType : ""
    };

    const FORM_VALIDATION = Yup.object().shape({
        productName : Yup.string().required('Please Enter product name'),
        caseType : Yup.number().required('please select a case type')
    });

    return (
        <div style={styles.mainDiv}>
            <Card sx={{ maxWidth: 750 }} style={styles.card}>
                <div style={styles.insideDiv}>
                    <Grid container spacing={3} >
                        <Grid item xs={12} sm={6} >
                            <Typography variant="h5" fontWeight={'bold'} gutterBottom>
                                Add New Product
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                            <Typography variant="h8" fontWeight={'bold'} gutterBottom>
                                Product Details
                            </Typography>
                        </Grid>
                    </Grid>
                    <Formik
                    initialValues={{
                        ...INITIAL_FORM_STATE,
                    }}
                    validationSchema={FORM_VALIDATION}
                    onSubmit={(values , {resetForm}) => {
                        setLoading(true);
                        addProduct(values.productName , values.caseType , currentPage , postsPerPage , setProducts , setCount , setLoading);
                        resetForm();
                    }}
                    >
                    <Form>
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                        <Textfield
                            type={"text"}
                            name="productName"
                            label="Product Name"
                            placeholder="Product Name"
                        />
                        </Grid>
                        <Grid item xs={12} sm={6} >
                            <Select name='caseType' options={cases}/>
                            {/* <SelectCustom name='caseType' items={cases} setValue={setCaseType}/> */}
                        </Grid>
                    </Grid>  
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                            <Button variant="contained" style={{width : '100%' , height : '55px'}} type='submit'>Submit</Button>
                        </Grid>
                        <Grid item xs={12} sm={6} >
                            <Button variant="contained" style={{width : '100%' , height : '55px'}} type='reset'>Cancel</Button>
                        </Grid>
                    </Grid> 
                    </Form></Formik>
                    </div>
                    {
                        products && 
                        <Box sx={{ height: 400, width: '100%' }}>
                        <DataGrid
                            pagination
                            paginationMode="server"
                            rowCount={count}
                            rows={products === undefined ? [] : products}
                            columns={columns}
                            page={currentPage}
                            pageSize={postsPerPage}
                            rowsPerPageOptions={[5 , 10 , 25]}
                            onPageSizeChange={(newValue) => setPostsPerPage(newValue)}
                            onPageChange={(newValue) => setCurrentPage(newValue)}
                            experimentalFeatures={{ newEditingApi: true }}
                        />
                    </Box>
                    }
            </Card>
            {loading && <Loader />}
        </div>
    )
}

export default ProductTable;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    card: {
        margin: 'auto'
    },
    insideDiv : {
        padding : '40px'
    }
}
