import { useMemo, useState , useEffect } from 'react';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { DataGrid } from '@mui/x-data-grid';
import Box from '@mui/material/Box';
import EditChangesPopUp from './EditChangesPopUp';
import { getAllCaseDetails , addNewCaseDetails ,  } from '../../Services/CaseService';
import Loader from '../Loader';
import * as Yup from "yup";
import { Form, Formik } from "formik";
import Textfield from '../FormsUI/Textfield/index';
import ConfirmDeleteButton from '../CustomComponent/ConfirmDeleteButton';

function CaseTable(){

    const [loading , setLoading] = useState(false);
    const [currentPage , setCurrentPage] = useState(0);
    const [postsPerPage , setPostsPerPage] = useState(5);
    const [count , setCount] = useState(0);

    const [cases , setCases] = useState([]);

    useEffect(() => {
        getAllCaseDetails(currentPage , postsPerPage , setCases , setCount);
    },[currentPage , postsPerPage]);

    const columns = useMemo(
        () => [
                  { field: 'case_name', headerName: 'Case Name', width: 300 , editable : 'true'},
                  { field: 'number_of_bottles', headerName: 'Number of Bottles', width: 300 , editable : 'true'},
                  {
                    field: 'edit',
                    headerName: '',
                    width: 100,
                    renderCell: (params) => {
                        return <EditChangesPopUp data={params} setCases={setCases} setCount={setCount} currentPage={currentPage} postsPerPage={postsPerPage}/>
                    }
                  },
        ],[currentPage , postsPerPage]
    );

    // form validation
    const INITIAL_FORM_STATE = {
        caseName:  "",
        numberOfBottles:  "",
    };

    const FORM_VALIDATION = Yup.object().shape({
        caseName : Yup.string()
        .required('Please Enter Route name'),
        numberOfBottles : Yup
        .number()
        .required('Please Enter Owner name')
      });

    return (
        <div style={styles.mainDiv}>
            <Card sx={{ maxWidth: 750 }} style={styles.card}>
                <div style={styles.insideDiv}>
                    <Grid container spacing={3} >
                        <Grid item xs={12} sm={6} >
                            <Typography variant="h5" fontWeight={'bold'} gutterBottom>
                                Add New Case
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                            <Typography variant="h8" fontWeight={'bold'} gutterBottom>
                                Case Details
                            </Typography>
                        </Grid>
                    </Grid>
                    <Formik
                    initialValues={{
                        ...INITIAL_FORM_STATE,
                    }}
                    validationSchema={FORM_VALIDATION}
                    onSubmit={(values , {resetForm}) => {
                        setLoading(true);
                        addNewCaseDetails(values.caseName , values.numberOfBottles , currentPage , postsPerPage , setCases , setCount , setLoading);
                        resetForm()
                    }}
                    >
                    <Form>
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                        <Textfield
                            type={"text"}
                            name="caseName"
                            label="Case Name"
                            placeholder="Case Name"
                        />
                        </Grid>
                        <Grid item xs={12} sm={6} >
                        <Textfield
                            type={"text"}
                            name="numberOfBottles"
                            label="Number of Bottles"
                            placeholder="Number of Bottles"
                        />
                        </Grid>
                    </Grid>  
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                            <Button variant="contained" style={{width : '100%' , height : '55px'}} type='submit'>Submit</Button>
                        </Grid>
                        <Grid item xs={12} sm={6} >
                            <Button variant="contained" style={{width : '100%' , height : '55px'}} type='reset'>Cancel</Button>
                        </Grid>
                    </Grid> 
                    </Form>
                    </Formik>
                    </div>
                    <Box sx={{ height: 400, width: '100%' }}>
                        <DataGrid
                            pagination
                            paginationMode="server"
                            rowCount={count}
                            rows={cases}
                            columns={columns}
                            pageSize={postsPerPage}
                            page={currentPage}
                            onPageSizeChange={(newValue) => setPostsPerPage(newValue)}
                            onPageChange={(newValue) => setCurrentPage(newValue)}
                            rowsPerPageOptions={[5 , 10 , 25]}
                            experimentalFeatures={{ newEditingApi: true }}
                        />
                    </Box>
            </Card>
            {loading && <Loader />}
        </div>
    )
}

export default CaseTable;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    card: {
        margin: 'auto'
    },
    insideDiv : {
        padding : '40px'
    }
}

const routes = [
    { 
        id: 1,
        caseName: '12 small',
        numberOfBottles: 12,
    },
    { 
        id: 2,
        caseName: '12 small',
        numberOfBottles: 12,
    },
    { 
        id: 3,
        caseName: '12 small',
        numberOfBottles: 12,
    },
    { 
        id: 5,
        caseName: '12 small',
        numberOfBottles: 12,
    },
    { 
        id: 6,
        caseName: '12 small',
        numberOfBottles: 12,
    },
  ];