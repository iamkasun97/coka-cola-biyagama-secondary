import * as api from '../api/index';

export const issueStockView = async (date , route , issueNumber , setLoading , setTransactionId , setRows) => {
    try{
        const data = await api.issueStockView(date , route , issueNumber);
        setTransactionId(data.data.data.trsnaction.id);
        let rows = [];
        data.data.data.stock.map(item => {
            let temp = {
                id : item.id,
                item : item.item_name,
                currentStock : 200,
                sellingPrice : item.bottle_selling_price,
                broughtPrice : item.bottle_brought_price,
                caseType : item.item_case_number_of_bottles,
                casesIssued : Math.floor(Number(item.transaction_quantity)/Number(item.item_case_number_of_bottles)),
                itemsIssued : Number(item.transaction_quantity)%Number(item.item_case_number_of_bottles),
                totalItemsIssued : item.transaction_quantity
            };

            rows.push(temp);
        })
        setRows(rows);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const routeReturnStockView = async (date , route , issueNumber , setLoading , setTransactionId , setRows) => {
    try{
        const data = await api.routeReturnStockView(date , route , issueNumber);
        setTransactionId(data.data.data.trsnaction.id);
        let rows = [];
        let ids = [];
        data.data.data.stock.map(item => {
            let temp = {
                id : item.id,
                item : item.item_name,
                currentStock : 200,
                sellingPrice : item.bottle_selling_price,
                broughtPrice : item.bottle_brought_price,
                caseType : item.item_case_number_of_bottles,
                returnedCases : Math.floor(Number(item.transaction_quantity)/Number(item.item_case_number_of_bottles)),
                returnedItems : Number(item.transaction_quantity)%Number(item.item_case_number_of_bottles),
                totalReturns : item.transaction_quantity,
                status : Number(item.transaction_quantity) > 0 && 'Open'
            };

            ids.push()
            rows.push(temp);
        })
        setRows(rows);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const inputStockView = async (date , invoiceNumber , setLoading , setTransactionId , setRows) => {
    try{
        const data = await api.inputStockView(date , invoiceNumber);
        setTransactionId(data.data.data.trasnaction.id);
        let rows = [];
        data.data.data.stock?.map(item => {
            let temp = {
                id : item.id,
                item : item.item_name,
                caseType : item.item_case_number_of_bottles,
                totalItems : item.transaction_quantity,
                cases : Math.floor(Number(item.transaction_quantity)/Number(item.item_case_number_of_bottles)),
                items : Number(item.transaction_quantity)%Number(item.item_case_number_of_bottles),
                broughtPrice : item.bottle_brought_price,
                sellingPrice : item.bottle_selling_price
            };

            rows.push(temp);
        });
        setRows(rows);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const returnStockView = async (date , returnInvoiceNumber , setLoading , setTransactionId , setRows) => {
    try{
        const data = await api.returnStockView(date , returnInvoiceNumber);
        setTransactionId(data.data.data.trasnaction.id);
        let rows = [];
        data.data.data.stock?.map(item => {
            let temp = {
                id : item.id,
                item : item.item_name,
                caseType : item.item_case_number_of_bottles,
                totalItems : item.transaction_quantity,
                cases : Math.floor(Number(item.transaction_quantity)/Number(item.item_case_number_of_bottles)),
                items : Number(item.transaction_quantity)%Number(item.item_case_number_of_bottles),
                broughtPrice : item.bottle_brought_price,
            };

            rows.push(temp);
        });
        setRows(rows);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const statForRouteAllocation = async (date , route , issueNumber , setLoading , setTransactionId , setRows) => {
    try{
        const data = await api.statForRouteAllocation(date , route , issueNumber);
        // console.log(data.data.data?.route_stock_list);
        // console.log(data.data.data?.return_stock_list);
        let rows = [];
        data.data.data?.route_stock_list?.map(item => {
            const res = data.data.data?.return_stock_list?.filter(e => e.stock_summary_id == item.stock_summary_id);
            if(res.length !== 0){
                let temp = {
                    id : item.stock_summary_id,
                    item : item.item_name,
                    totalItemsIssued : item.transaction_quantity,
                    caseType : item.item_case_number_of_bottles,
                    casesIssued : Math.floor(Number(item.transaction_quantity)/Number(item.item_case_number_of_bottles)),
                    itemIssued : Number(item.transaction_quantity)%Number(item.item_case_number_of_bottles),
                    totalReturns : res[0].transaction_quantity,
                    returnedCases : Math.floor(Number(res[0].transaction_quantity)/Number(res[0].item_case_number_of_bottles)),
                    returnedItems : Number(res[0].transaction_quantity)%Number(res[0].item_case_number_of_bottles),
                    broughtPrice : item.bottle_brought_price,
                    sellingPrice : item.bottle_selling_price,
                }
                rows.push(temp);
            }else if(res.length === 0){
                let temp = {
                    id : item.stock_summary_id,
                    item : item.item_name,
                    totalItemsIssued : item.transaction_quantity,
                    caseType : item.item_case_number_of_bottles,
                    casesIssued : Math.floor(Number(item.transaction_quantity)/Number(item.item_case_number_of_bottles)),
                    itemIssued : Number(item.transaction_quantity)%Number(item.item_case_number_of_bottles),
                    totalReturns : 0,
                    returnedCases : 0,
                    returnedItems : 0,
                    broughtPrice : item.bottle_brought_price,
                    sellingPrice : item.bottle_selling_price,
                }
                rows.push(temp);
            }
        })

        data.data.data?.return_stock_list?.map(item => {
            if(data.data.data?.route_stock_list?.find(e => e.stock_summary_id == item.stock_summary_id) == undefined){
                let temp = {
                    id : item.stock_summary_id,
                    item : item.item_name,
                    totalItemsIssued : 0,
                    caseType : item.item_case_number_of_bottles,
                    casesIssued : 0,
                    itemIssued : 0,
                    totalReturns : item.transaction_quantity,
                    returnedCases : Math.floor(Number(item.transaction_quantity)/Number(item.item_case_number_of_bottles)),
                    returnedItems : Number(item.transaction_quantity)%Number(item.item_case_number_of_bottles),
                    broughtPrice : item.bottle_brought_price,
                    sellingPrice : item.bottle_selling_price,
                }
                rows.push(temp);
            }
        });
        setRows(rows);
        setTransactionId(1);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}