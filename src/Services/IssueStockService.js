import * as api from '../api/index';
import { openSuccessDialog } from '../Components/FormsUI/PopUps/SuccessDialog/index.tsx';

export const startIssueStockTransaction = async (date , routeId , issueNumber , setLoading , setTransactionId , setRows) => {
    try{
        const data = await api.startIssueStockTransaction(date , routeId , issueNumber);
        setTransactionId(data.data.data.id);
        const dataNext = await api.getStockSummaryForIssue();
        let rows = [];
        dataNext.data.data?.map(item => {
            let temp = {
                id : item.id,
                item : item.item_name,
                caseType : item.item_case_number_of_bottles,
                currentStock : item.total_quantity,
                broughtPrice : item.bottle_brought_price,
                sellingPrice : item.bottle_selling_price,
                casesIssued : 0,
                itemsIssued : 0
            };

            rows.push(temp);
        });
        setRows(rows);
        setLoading(false);
    }catch(error){
        // console.log(error);
        setLoading(false);
    }
}

export const publishIssueStockTransaction = async (transactionId , items , setTransactionId , setLoading , setRoute , setIssueNumber , setEditedRow) => {
    try{
        const data = await api.addItemsToRoute(transactionId , items);
        openSuccessDialog(data.data.status , data.data.comment);
        setRoute('');
        setIssueNumber('');
        setTransactionId(null);
        setLoading(false);
        setEditedRow([]);
    }catch(error){
        setLoading(false);
    }
}