import * as api from '../api/index';
import { openSuccessDialog } from '../Components/FormsUI/PopUps/SuccessDialog/index.tsx';

export const startReturnRouteTransactions = async (date , route , issueNumber , setLoading , setTransactionId , setRows) => {
    try{
        const data = await api.startReturnRouteTransactions(date , route , issueNumber);
        console.log(data.data.data);
        setTransactionId(data.data.data.created_transaction.id);
        let rows = [];
        let ids = [];
        let status = '';
        data.data.data.route_allocated_stock?.map(item => {

            if(Number(item.transaction_quantity) > 0){
                status = 'Open'
            }

            let temp = {
                id : item.stock_summary_id,
                item : item.item_name,
                totalItemsIssued : item.transaction_quantity,
                caseType : item.item_case_number_of_bottles,
                casesIssued : Math.floor(Number(item.transaction_quantity)/Number(item.item_case_number_of_bottles)),
                itemIssued : Number(item.transaction_quantity)%Number(item.item_case_number_of_bottles),
                returnedCases : 0,
                returnedItems : 0,
                status : Number(item.transaction_quantity) > 0 && 'Open',
                broughtPrice : item.bottle_brought_price,
                sellingPrice : item.bottle_selling_price
            };

            rows.push(temp);
            ids.push(item.stock_summary_id);
        });

        const dataNext = await api.getStockSummaryForRouteReturn();
        const otherRows = []
        dataNext.data.data.map(item => {
            if(ids.includes(item.id.toString())){}else {
                otherRows.push({
                    id : item.id,
                    item : item.item_name,
                    currentStock : item.total_quantity,
                    totalItemsIssued : 0,
                    caseType : item.item_case_number_of_bottles,
                    casesIssued : 0,
                    itemIssued : 0,
                    returnedCases : 0,
                    returnedItems : 0,broughtPrice : item.bottle_brought_price,
                    sellingPrice : item.bottle_selling_price
                });
            }
        });
        rows = rows.concat(otherRows);
        setRows(rows);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const publishRouteReturnTransaction = async (transactionId , items , setTransactionId , setLoading , setRoute , setIssueNumber , setEditedRow) => {
    try{
        const data = await api.addItemsToRouteReturn(transactionId , items);
        openSuccessDialog(data.data.status , data.data.comment);
        setRoute('');
        setIssueNumber('');
        setTransactionId(null);
        setEditedRow([]);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}