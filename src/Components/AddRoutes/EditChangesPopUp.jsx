import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import Textfield from '../FormsUI/Textfield';
import * as Yup from "yup";
import { Form, Formik } from "formik";
import { editRouteDetails } from '../../Services/RouteService';
import Loader from '../Loader';

export default function FormDialog(props) {
  const [open, setOpen] = React.useState(false);
  const [loading , setLoading] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  // form validation
  const INITIAL_FORM_STATE = {
    routeName:  props.data.row.route_name,
    owner : props.data.row.route_owner,
};

const FORM_VALIDATION = Yup.object().shape({
  routeName: Yup.string().required("Please Enter your username"),
  owner : Yup.string().required('Please Enter Your Mobile Number'),
});

  return (
    <div>
      <Button variant="contained" onClick={handleClickOpen} style={{width : '80px' , height : '40px'}}>
        Edit
      </Button>
      <div style={{width : '1000px'}}>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Edit User Details</DialogTitle>
        <Formik
        initialValues={{
            ...INITIAL_FORM_STATE,
        }}
        validationSchema={FORM_VALIDATION}
        onSubmit={(values , {resetForm}) => {
            setLoading(true);
            // console.log(values);
            // console.log(props.data);
            editRouteDetails(props.data.id , values.routeName , values.owner , props.currentPage , props.postsPerPage , props.setRoutes , props.setCount , setLoading);
            setOpen(false);
        }}
        >
        <Form>
        <DialogContent>
          <DialogContentText>
          </DialogContentText>
          <Grid container spacing={3} >
            <Grid item xs={12} sm={6} >
              <Textfield
                    type={"text"}
                    name="routeName"
                    label="Route Name"
                    placeholder="Route Name"
                />
            </Grid>
            <Grid item xs={12} sm={6} >
              <Textfield
                    type={"text"}
                    name="owner"
                    label="Owner"
                    placeholder="Owner"
                />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button type='submit'>Save</Button>
        </DialogActions>
        </Form>
        </Formik>
        {loading && <Loader />}
      </Dialog>
      </div>
    </div>
  );
}