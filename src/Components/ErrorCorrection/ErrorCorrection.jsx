import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import RouteErrorCorrectionIssue from './RouteErrorCorrectionIssue';
import RouteErrorCorrectionReturn from './RouteErrorCorrectionReturn';
import InputErrorCorrection from './InputErrorCorrection';
import ReturnErrorCorrection from './ReturnErrorCorrection';
import { useDispatch, useSelector } from "react-redux";


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function BasicTabs() {
  const [value, setValue] = React.useState(0);
  const user = useSelector((state) => state.auth.authData);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div style={styles.mainDiv}>
    
      {
        user?.data?.user_role == 'ADMIN' ? 
        <Box sx={{ width: '100%' }} style={styles.mainBox}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" variant="scrollable" scrollButtons="auto">
              <Tab label="Route Vise Error Correction (Daily Issue)" {...a11yProps(0)} />
              <Tab label="Route Vise Error Correction (Daily Return)" {...a11yProps(1)} />
              <Tab label="Input Error Correction" {...a11yProps(2)} />
              <Tab label="Return Error Correction" {...a11yProps(3)} />
            </Tabs>
          </Box>
            {
              value === 0 && <RouteErrorCorrectionIssue />
            }
            {
              value === 1 && <RouteErrorCorrectionReturn />
            }
            {
              value === 2 && <InputErrorCorrection />
            }
            {
              value === 3 && <ReturnErrorCorrection />
            }
        </Box> : null
      }
      {
        user?.data?.user_role == 'SUBADMIN' ? 
        <Box sx={{ width: '100%' }} style={styles.mainBox}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" variant="scrollable" scrollButtons="auto">
              <Tab label="Route Vise Error Correction (Daily Issue)" {...a11yProps(0)} />
              <Tab label="Route Vise Error Correction (Daily Return)" {...a11yProps(1)} />
            </Tabs>
          </Box>
            {
              value === 0 && <RouteErrorCorrectionIssue />
            }
            {
              value === 1 && <RouteErrorCorrectionReturn />
            }
        </Box> : null
      }

    
    </div>
  );
}

const styles = {
    mainDiv : {
        margin : '20px'
    }
}