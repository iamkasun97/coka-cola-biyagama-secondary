import * as React from 'react';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router-dom';
import UserContext from '../../context/UserContext';
import { saveUserDetals } from '../../Services/UserManageService';
import {  getAllUsers } from '../../Services/UserManageService';

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

function createData(id , email , password ) {
  return {id, email , password };
}

const LoadData = [
  createData(1 , 'kasun@gmail.com' , '{{}*&*^&%^$^#'),
  createData(2 , 'tharindu@gmail.com' , '9%*^$^^#'),
];
// .sort((a, b) => (a.calories < b.calories ? -1 : 1))

export default function CustomPaginationActionsTable(props) {



  const navigate = useNavigate();
  const userDetails = React.useContext(UserContext);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  


  // my functions
  const [editableId , setEditableId] = React.useState("");
  const [email , setEmail] = React.useState("");
  const [userName , setUserName] = React.useState('');
  const [fName , setFName] = React.useState('');
  const [mobile , setMobile] = React.useState('');
  const [rows , setRows] = React.useState(LoadData);
  const [success , setSuccess] = React.useState('');
  const [error , setError] = React.useState('');

  // table pagination
  const [currentPage , setCurrentPage] = React.useState(0);
  const [postsPerPage , setPostsPerPage] = React.useState(5);
  const [count , setCount] = React.useState(0);

  React.useEffect(() => {
    getAllUsers(currentPage , postsPerPage , userDetails.setUsers , setCount);
} , []);

  // load the one row data into the state
  const handleClick = (e , row) => {
    e.preventDefault();
    setEditableId(row.id);
    setEmail(row.email);
    setUserName(row.username);
    setFName(row.fname);
    setMobile(row.mobile);
  }

  // save the values
  const saveValues = (e) => {
    e.preventDefault();
    // console.log(editableId , userName , fName , email , mobile);
    saveUserDetals(editableId , userName , fName , email , mobile , setSuccess , setError , currentPage , postsPerPage , userDetails.setUsers);
    setEditableId("");
    setEmail("");
    setUserName('');
    setFName('');
    setMobile('');
  }

  const cancelSelect = (e) => {
    e.preventDefault();
    setEditableId("");
    setEmail("");
    setUserName('');
    setFName('');
    setMobile('');
  }

  const handleChangePage = (
    event,
    newPage
) => {
    getAllUsers(newPage , postsPerPage , userDetails.setUsers , setCount);
    setCurrentPage(newPage);
};

const handleChangeRowsPerPage = (
    event
) => {
    let updatePostsPerPage = event.target.value;
    setPostsPerPage(event.target.value);
    getAllUsers(currentPage , updatePostsPerPage , userDetails.setUsers , setCount);
    setCurrentPage(0);
};

  return (
    <TableContainer component={Paper}>
      <Table sx={{ width : 1200 }} aria-label="custom pagination table">
        <TableBody>
          {(userDetails.users)?.map((row , index) => (
            <TableRow key={row.id}>
              <TableCell>
              <TextField
                    disabled={row.id !== editableId ? true : false}
                    fullWidth
                    autoComplete="family-name"
                    variant="outlined"
                    label="user name"
                    value={row.id == editableId ? userName : row.username}
                    onChange={(event) => setUserName(event.target.value)}
                />
                </TableCell>
                <TableCell>
                <TextField
                      disabled={row.id !== editableId ? true : false}
                      fullWidth
                      autoComplete="family-name"
                      variant="outlined"
                      label="first name"
                      value={row.id == editableId ? fName : row.fname}
                      onChange={(event) => setFName(event.target.value)}
                  />
                </TableCell>
                <TableCell>
                <TextField
                      disabled={row.id !== editableId ? true : false}
                      fullWidth
                      autoComplete="family-name"
                      variant="outlined"
                      label="mobile number"
                      value={row.id == editableId ? mobile : row.mobile}
                      onChange={(event) => setMobile(event.target.value)}
                  />
                </TableCell>
              <TableCell>
              <TextField
                    disabled={row.id !== editableId ? true : false}
                    fullWidth
                    autoComplete="family-name"
                    variant="outlined"
                    label="email"
                    value={row.id == editableId ? email : row.email}
                    onChange={(event) => setEmail(event.target.value)}
                />
                </TableCell>
                <TableCell>
              </TableCell>
              <TableCell style={{ width: 160 }} align="right">
              <div >
              {
                row.id !== editableId ? 
                  <div style={{display : 'flex' , width : 300}}>
                  <Button variant="contained" style={{ marginRight : '2px'}} onClick={(e) => handleClick(e , row)}>edit</Button> 
                  <Button variant="contained" style={{ marginRight : '2px'}} onClick={() => navigate('/change-password' , {state : {userData : row}})}>change password</Button> 
                  </div>
                  : 
                  <div style={{display : 'flex'}}>
                  <Button variant="contained" style={{ marginRight : '2px'}} onClick={(e) => saveValues(e)}>Save</Button>
                  <Button variant="contained" style={{ marginRight : '2px'}} onClick={(e) => cancelSelect(e)}>Cancel</Button>
                  </div>
              }
              </div>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
        <TablePagination
            rowsPerPageOptions={[5, 10, 25, 100]}
            component="div"
            count={count}
            rowsPerPage={postsPerPage}
            page={currentPage}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
            ActionsComponent={TablePaginationActions}
          />
    </TableContainer>
  );
}