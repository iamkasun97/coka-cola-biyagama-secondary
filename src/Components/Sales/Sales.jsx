import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import DailySales from './DailySales';
import DailyStockVariation from './DailyStockVariation';
import IssueStockView from './IssueStockView';
import RouteReturnView from './RouteReturnView';
import InputStockView from './InputStockView';
import ReturnInventoryView from './ReturnInventoryView';
import CollectionView from './CollectionView';
import { useDispatch, useSelector } from "react-redux";
import StatRouteView from './StatRouteView';
import ProductViseDaily from './ProductViseDaily';
import ProductViseMonthly from './ProductViseMonthly';
import CollectionSummary from './CollectionSummary';
import MonthlySalesDifference from './MonthlySalesDifference';
import MonthlyDiscountSummary from './MonthlyDiscountSummary';
import MonthlyPurchaseSummary from './MonthlyPurchaseSummary';


function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
  };
  
  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }


function Sales(){

    const user = useSelector((state) => state.auth.authData);

    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div style={styles.mainDiv}> 
            <Box sx={{ width: '100%' }} style={styles.mainBox}>
              
              <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
              <Tabs value={value} onChange={handleChange} aria-label="basic tabs example"  variant="scrollable" scrollButtons="auto">
                  <Tab label="Daily Sales" {...a11yProps(0)} />
                  <Tab label="Daily Stock Variation" {...a11yProps(1)} />
                  <Tab label="Issue Stock View" {...a11yProps(2)} />
                  <Tab label="Return Route View" {...a11yProps(3)} />
                  <Tab label="Input Stock View" {...a11yProps(4)} />
                  <Tab label="Return Inventory View" {...a11yProps(5)} />
                  <Tab label="Collection View" {...a11yProps(6)} />
                  <Tab label="Route Statics" {...a11yProps(7)} />
                  <Tab label="Product Vise Reports(Daily)" {...a11yProps(8)} />
                  <Tab label="Product Vise Reports(Monthly)" {...a11yProps(9)} />
                  <Tab label="Collection Summary" {...a11yProps(10)} />
                  <Tab label="Monthly Sales Difference" {...a11yProps(11)} />
                  <Tab label="Monthly Discount Summary" {...a11yProps(12)} />
                  <Tab label="Monthly Purchase Summary" {...a11yProps(13)} />
              </Tabs>

              {
                value === 0 && <DailySales />
              }
              {
                value === 1 && <DailyStockVariation />
              }
              {
                value === 2 && <IssueStockView />
              }
              {
                value === 3 && <RouteReturnView/>
              }
              {
                value === 4 && <InputStockView />
              }
              {
                value === 5 && <ReturnInventoryView />
              }
              {
                value === 6 && <CollectionView />
              }
              {
                value === 7 && <StatRouteView />
              }
              {
                value === 8 && <ProductViseDaily />
              }
              {
                value === 9 && <ProductViseMonthly />
              }
              {
                value === 10 && <CollectionSummary />
              }
              {
                value === 11 && <MonthlySalesDifference />
              }
              {
                value === 12 && <MonthlyDiscountSummary />
              }
              {
                value === 13 && <MonthlyPurchaseSummary />
              }
            </Box>      
              
            </Box>
        </div>
    )
}

export default Sales;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    buttonDiv : {
        marginTop : '10px'
    }
}