import Grid from '@mui/material/Grid';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';
import TextField from '@mui/material/TextField';
import * as React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { inputStockView } from '../../Services/ViewService';
import { GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid';
import Loader from '../Loader';

  function CustomFooterStatusComponent(props) {
    return (
      <Box  style={{  display: 'flex' , marginTop : '10px' , marginBottom : '10px' }}>
        <Grid container spacing={3} >
                <Grid item xs={12} sm={2} >
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                  Cases Issued - {props.sumCases}
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Items Issued - {props.sumItems}
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                total Items Issued - {props.totalSumItems}
                </Grid>
                <Grid item xs={12} sm={4} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Total Items Brought Price - {props.total}
                </Grid>
        </Grid>
      </Box>
    );
  }

  function CustomToolbar() {
    return (
      <GridToolbarContainer>
        <GridToolbarExport 
        csvOptions={{
            fileName: new Date(),
        }}
        />
      </GridToolbarContainer>
    );
  }


function InputStockTable(){
    
    const [sumCases , setSumCases] = useState(0);
    const [sumItems , setSumItems] = useState(0);
    const [totalSumItems , setTotalSumItems] = useState(0);
    const [total , setTotal] = useState(0);

    const [date , setDate] = useState(dayjs().format('YYYY-MM-DD'));
    const [invoiceNumber , setInvoiceNumber] = useState('');
    const [transactionId , setTransactionId] = useState(null);
    const [loading , setLoading] = useState(false);

    const handleDateChange = (newValue) => {
        setDate(dayjs(newValue).format('YYYY-MM-DD'));
    };

    const [rows , setRows] = useState([]);

    const handleStart = () => {
      setLoading(true);
      inputStockView(date , invoiceNumber , setLoading , setTransactionId , setRows);
    }

    const handleCancel = () => {
      setTransactionId(null);
      setInvoiceNumber('');
    }
    return (
        <div style={styles.mainDiv}>
            <Grid container spacing={3} >
                <Grid item xs={12} sm={4} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                    <DesktopDatePicker
                        inputFormat="MM/DD/YYYY"
                        value={date}
                        onChange={handleDateChange}
                        renderInput={(params) => <TextField {...params} />}
                    />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={4} >
                        <TextField
                            label="invoice number"
                            fullWidth
                            autoComplete="family-name"
                            variant="outlined"
                            value={invoiceNumber}
                            onChange={(event) => setInvoiceNumber(event.target.value)}
                        />
                </Grid>
                <Grid item xs={12} sm={4} >
                    {
                      transactionId !== null ? <Button variant="contained" style={{width : '100%' , height : '100%'}} onClick={() => handleCancel()}>Cancel</Button>
                      : 
                      <Button variant="contained" style={{width : '100%' , height : '100%'}} disabled={loading || invoiceNumber === '' || date === 'Invalid Date'} onClick={() => handleStart()}>Search</Button>
                    }
                </Grid>
            </Grid>
            <hr color='black'/>
            {
              transactionId !== null ? 
              <div>
                  <Box sx={{ height: 400, width: '100%' }}>
                    <DataGrid
                        rows={rows}
                        columns={columns}
                        experimentalFeatures={{ newEditingApi: true }}
                        onStateChange={(state) => {
                          let sumCasesInput = 0;
                          let sumItemInput = 0;
                          let totalBroughtPrice = 0;
                          let sumTotalItemInput = 0;
                          state.rows.ids.map(item => {
                            sumCasesInput += Number(state.rows.idRowsLookup[item].cases);
                            sumItemInput += Number(state.rows.idRowsLookup[item].items);
                            sumTotalItemInput += Number(state.rows.idRowsLookup[item].totalItems);
                            totalBroughtPrice += (Number(state.rows.idRowsLookup[item].cases)*Number(state.rows.idRowsLookup[item].caseType)+Number(state.rows.idRowsLookup[item].items))*Number(state.rows.idRowsLookup[item].broughtPrice);
                          });
                          // setSumCases(new Intl.NumberFormat().format(parseFloat(sumCasesReturn).toFixed(2)));
                          setSumCases(new Intl.NumberFormat().format(parseFloat(sumCasesInput).toFixed(2)));
                          setTotal(new Intl.NumberFormat().format(parseFloat(totalBroughtPrice).toFixed(2)));
                          setSumItems(new Intl.NumberFormat().format(parseFloat(sumItemInput).toFixed(2)));
                          setTotalSumItems(new Intl.NumberFormat().format(parseFloat(sumTotalItemInput).toFixed(2)));
                        }}
                        components={{
                          Footer: CustomFooterStatusComponent,
                          Toolbar : CustomToolbar
                        }}
                        componentsProps={{
                          footer: { total , sumItems , sumCases , totalSumItems},
                        }}
                    />
                </Box>
              </div> : null
            }
            {loading && <Loader />}
        </div>
    )
}

export default InputStockTable;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    buttonDiv : {
        marginTop : '10px'
    }
}

const columns = [
    { field: 'item', headerName: 'Item', width: 180, editable: false },
    { field: 'caseType', headerName: 'Case Type' ,width: 180, editable: false },
    { 
      field: 'cases', 
      headerName: 'Cases',
      width: 180, 
      editable: false
    },
    {
      field: 'items',
      headerName: 'Items',
      width: 180,
      editable: false,
    },
    {
      field: 'totalItems',
      headerName: 'Total Items',
      width: 180,
      editable: false,
    },
    {
      field: 'broughtPrice',
      headerName: 'Brought Price',
      width: 180,
      editable: false,
    },
    {
      field: 'totalBroughtPrice',
      headerName: 'Total Brought Price',
      width: 180,
      editable: false,
      renderCell : (params) => {
        if(params.row.caseType === undefined || params.row.cases === undefined || params.row.items === undefined || params.row.broughtPrice === undefined){
          return 0;
        }
        return new Intl.NumberFormat().format(parseFloat((Number(params.row.caseType)*Number(params.row.cases) + Number(params.row.items))*Number(params.row.broughtPrice)).toFixed(2));
      }
    },
    {
      field: 'sellingPrice',
      headerName: 'sellingPrice',
      width: 180,
      editable: false,
    }
  ];
