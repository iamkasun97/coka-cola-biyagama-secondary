import Axios from 'axios';
import { openErrorDialog } from "../Components/FormsUI/PopUps/ErrorDialog/index.tsx";
import { decrypt } from "../utils/utils";


const interceptor = Axios.create({baseURL : 'http://localhost:8080/inv-cm'}); // https://smartconnector.lk/anjelo/inv-cm/api/v1
// const interceptor = Axios.create({baseURL : 'https://smartconnector.lk/anjelo/inv-cm'}); // http://localhost:8080/inv-cm

// const interceptor = Axios.create({
//   baseURL:
//     process.env.NODE_ENV === "development"
//       ? process.env.REACT_APP_API_DEV
//       : process.env.REACT_APP_API_PROD,
// });

// const interceptor = Axios.create({baseURL : 'https://smartconnector.lk/anjelo/inv-cm'});

interceptor.interceptors.request.use(
    (req) => {
      if (localStorage.getItem("profile")) {
        const user = JSON.parse(localStorage.getItem("profile"));
        let decryptedDefaultData = decrypt(user);
        req.headers[
          "Authorization"
        ] = `Bearer ${decryptedDefaultData.data.token}`;
      }
  
      return req;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
  

  interceptor.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      if (
        error.response.status === 401 &&
        error.response.data.comment === "auth token expired or not found"
      ) {
        openErrorDialog(error.response.data.status, error.response.data.comment);
        localStorage.removeItem("profile");
        window.location.href = "/anjelo/login";
        // window.location.href = "/login";
      }else if(error.response.status === 403){
        localStorage.removeItem("profile");
        window.location.href = "/anjelo/login";
        // window.location.href = "/login";
      } else {
        openErrorDialog(error.response.data.status, error.response.data.comment);
      }
    }
  );
  

export default interceptor;