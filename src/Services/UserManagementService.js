import * as api from '../api/index';

export const getAllUsers = async (currentPage , postsPerPage , setUsers , setCount) => {
    try{
        const data = await api.getAllUsers(currentPage , postsPerPage);
        if(data.data.status === 'success'){
            setUsers(data.data.data.data_for_the_loop);
            setCount(data.data.data.count);
        }
    }catch(error){
        // console.log(error);
    }
}

export const registerSubUser = async (userName , fName , email , mobile , password , cPassword , currentPage , postsPerPage , setUsers , setCount , setLoading) => {
    try{
        const data = await api.registerSubUser(userName , fName , email , mobile , password , cPassword);
        if(data.data.status === 'success'){
            const dataNext = await api.getAllUsers(currentPage , postsPerPage);
            if(dataNext.data.status === 'success'){
                setUsers(dataNext.data.data.data_for_the_loop);
                setCount(dataNext.data.data.count);
            }
        }
        setLoading(false);
    }catch(error){
        // console.log(error);
        setLoading(false);
    }
}

export const saveUserDetails = async (id , userName , fName , email , mobile , currentPage , postsPerPage , setUsers , setCount , setLoading) => {
    try{
        const data = await api.saveUserDetails(id , userName , fName , email , mobile);
        if(data.data.status === 'success'){
            const dataNext = await api.getAllUsers(currentPage , postsPerPage);
            if(dataNext.data.status === 'success'){
                setUsers(dataNext.data.data.data_for_the_loop);
                setCount(dataNext.data.data.count);
            }
        }
        setLoading(false);
    }catch(error){
        // console.log(error);
        setLoading(false);
    }
}

export const updatepassword = async (id , password , cPassword , setLoading) => {
    try{
        const data = await api.updatepassword(id , password , cPassword);

        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}