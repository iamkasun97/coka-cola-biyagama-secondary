import Sidebar from "./Components/Sidebar";
import "./App.css";
import { useState } from 'react';
import { useSelector } from "react-redux";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { BrowserRouter, Routes, Route, useLocation } from "react-router-dom";
import HomePage from './Components/Home/Home';
import IssueStockTable from "./Components/IssueStock/IssueStockTable";
import InputStockTable from "./Components/InputStock/InputStockTable";
import DailyCollections from "./Components/Collections/DailyCollections";
import ReturnInventoryTable from "./Components/ReturnInventory/ReturnInventoryTable";
import Sales from "./Components/Sales/Sales";
import ErrorCorrection from './Components/ErrorCorrection/ErrorCorrection';
import RouteReturn from "./Components/RouteReturn/RouteReturnTable";
import UsersTable from './Components/AddUsers/UsersTable';
import RoutesTable from './Components/AddRoutes/RoutesTable';
import CaseTable from "./Components/AddCases/CaseTable";
import ProductTable from './Components/AddProducts/ProductTable';
import Collections from './Components/AddCollectionType/CollectionTable';
import Login from './Components/Login';
import ErrorDialog from './Components/FormsUI/PopUps/ErrorDialog/index.tsx';
import SuccessDialog from "./Components/FormsUI/PopUps/SuccessDialog/index.tsx";
import RequireAuth from "./Components/RequireAuth";
import UnauthorizeAccess from "./Components/UnauthorizeAccess";
import Layout from "./Components/Layout";

function App() {
  const theme = createTheme({
    typography: {
      fontFamily: ['"Open Sans"', "sans-serif"].join(","),
    },
    palette: {
      primary: {
        main: "#0A0A0A",
      },
      secondary: {
        main: "#d3d6db",
      },
      success: {
        main: "#008567",
      },
      error: {
        main: "#e02020",
      },
    },
  });


  const [show, setShow] = useState(false);
  const location = useLocation();
  // redux 
  const user = useSelector((state) => state.auth.authData);
  return (
    <ThemeProvider theme={theme}>
      {
        user !== undefined &&  location.pathname !== '/' && location.pathname !== '/unauthorized' && (
          <Sidebar show={show} setShow={setShow}/>
        )
      }
      <div className='main main-with-sidebar'>
        <Routes>
          {/* <Route path="/" element={<Layout />} /> */}
          <Route path="/" element={<Login />}></Route>
          <Route extract path="/login" element={<Login />}></Route>
          <Route path="/home" element={<HomePage />}></Route>
          <Route path="/issue-stock" element={<IssueStockTable />}></Route>
          <Route path="/route-returns" element={<RouteReturn />}></Route>
          <Route path="/collections" element={<DailyCollections />}></Route>
          <Route path="/sales" element={<Sales />}></Route>
          <Route path="/input-stock" element={<InputStockTable />} ></Route>
          <Route path="/return-inventory" element={<ReturnInventoryTable />} ></Route>
          <Route path="/error-correction" element={<ErrorCorrection />} ></Route>
          {/* <Route  element={<RequireAuth allowedRoles={['ADMIN']} auth={user?.data?.user_role} />}>
            <Route path="/input-stock" element={<InputStockTable />} />
          </Route> */}
          {/* <Route  element={<RequireAuth allowedRoles={['ADMIN']} auth={user?.data?.user_role} />}>
            <Route path="/return-inventory" element={<ReturnInventoryTable />} />
          </Route> */}
          {/* <Route  element={<RequireAuth allowedRoles={['ADMIN']} auth={user?.data?.user_role} />}>
            <Route path="/sales" element={<Sales />} />
          </Route> */}
          {/* <Route  element={<RequireAuth allowedRoles={['ADMIN']} auth={user?.data?.user_role} />}>
            <Route path="/error-correction" element={<ErrorCorrection />} />
          </Route> */}
          <Route  element={<RequireAuth allowedRoles={['ADMIN']} auth={user?.data?.user_role} />}>
            <Route path="/add-users" element={<UsersTable />} />
          </Route>
          <Route  element={<RequireAuth allowedRoles={['ADMIN']} auth={user?.data?.user_role} />}>
            <Route path="/add-routes" element={<RoutesTable />} />
          </Route>
          <Route  element={<RequireAuth allowedRoles={['ADMIN']} auth={user?.data?.user_role} />}>
            <Route path="/add-cases" element={<CaseTable />} />
          </Route>
          <Route  element={<RequireAuth allowedRoles={['ADMIN']} auth={user?.data?.user_role} />}>
            <Route path="/add-products" element={<ProductTable />} />
          </Route>
          <Route  element={<RequireAuth allowedRoles={['ADMIN']} auth={user?.data?.user_role} />}>
            <Route path="/add-collections" element={<Collections />} />
          </Route>
          <Route path="/unauthorized" element={<UnauthorizeAccess />}></Route>
        </Routes>    
      </div>
      <ErrorDialog />
      <SuccessDialog/>
    </ThemeProvider>
  );
}

export default App;
// java -jar coke-secondary-inv-0.0.1-SNAPSHOT.war
