import React, { PureComponent , useContext } from 'react';
import { PieChart, Pie, Sector, Cell, ResponsiveContainer , Legend} from 'recharts';
import salesContext from '../../context/salesContext';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

export default function PieChart2(){

    const salesData = useContext(salesContext);
    return (
      <ResponsiveContainer width="100%" height="100%" >
      <div style={styles.pieCard}> 
      <Typography variant="h6" gutterBottom>
          Route Vise Sales
      </Typography>
      <Typography variant="h10" gutterBottom>
      </Typography>
      <PieChart width={500} height={300}>
        <Legend align='right' layout='vertical' verticalAlign='middle'/>
        <Pie
          data={salesData?.routeSales}
          cx={140}
          cy={150}
          innerRadius={80}
          outerRadius={100}
          fill="#8884d8"
          paddingAngle={5}
          dataKey="value"
        >
          {salesData?.routeSales?.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={salesData?.colors[index % salesData?.colors?.length]} />
          ))}
        </Pie>
      </PieChart>
      </div>
      </ResponsiveContainer>
    );
  }


  const styles = {
    pieCard : {
      height : '395px',
      overflowX: 'scroll',
      margin : '20px'
    }
  }
  