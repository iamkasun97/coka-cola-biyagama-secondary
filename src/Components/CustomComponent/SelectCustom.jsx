import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export default function SelectLabels(props) {

  let initialValue = '';
  if(props?.initialValue === '' || props?.initialValue === undefined){
    initialValue = '';
  }else {
    initialValue = props?.initialValue;
  }

  const [age, setAge] = React.useState(initialValue);
  const handleChange = (event) => {
    setAge(event.target.value);
    props?.setValue(event.target.value);
  };
  return (
    <div>
      <FormControl sx={{ width : '100%' }} >
        <InputLabel id="demo-simple-select-helper-label">{props?.name}</InputLabel>
        <Select
          variant='outlined'
          fullWidth={true}
          disabled={props?.disabled}
          label={props?.name}
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={props?.initialValue === '' ? '' : age}
          onChange={handleChange}
        >
        {   
            props?.name ==='Brands' ? props?.items?.map((item ) => {
                return <MenuItem value={parseInt(item.id)} key={parseInt(item.id)}>{item.brand_name}</MenuItem>
            }) : null
        }
        {
          props?.name === 'Bottle Size(ml)' ? props?.items?.map((item ) => {
            return <MenuItem value={parseInt(item.id)} key={parseInt(item.id)}>{item.volume_type}</MenuItem>
        }) : null
        }
        {
          props?.name === 'caseType' ? props?.items?.map((item) => {
            return <MenuItem value={parseInt(item.id)} key={parseInt(item.id)}>{item.case_name}</MenuItem>
          }) : null
        }
        {
          props?.name === 'routes' ? props?.items?.map(item => {
            return <MenuItem value={parseInt(item.id)} key={parseInt(item.id)}>{item.route_name}</MenuItem>
          }) : null
        }
        {
          props?.name === 'issue number' ? props?.items?.map(item => {
            return <MenuItem value={parseInt(item?.id)} key={parseInt(item?.id)}>{item.value}</MenuItem>
          }) : null
        }
        {
          props?.name === 'ItemData' && props?.items?.length !== 0 ? props?.items?.map(item => {
            return <MenuItem value={parseInt(item?.id)} key={parseInt(item?.id)}>{'Brand Name : ' + item.brand_name + '  ,  ' + 'Bottle Size : '+ item.volume_name + '  ,  ' +'Selling Price : '+ item.bottle_selling_price }</MenuItem>
          }) : null
        }
        {
          props?.name === 'Collections' && props?.items?.map(item => {
            return <MenuItem value={item?.collection_name} key={item?.collection_name}>{item?.collection_name}</MenuItem>
          })
        }
        </Select>
      </FormControl>
    </div>
  );
}