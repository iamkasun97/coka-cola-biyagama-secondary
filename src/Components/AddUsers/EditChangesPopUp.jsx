import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import Textfield from '../FormsUI/Textfield';
import * as Yup from "yup";
import { Form, Formik } from "formik";
import {saveUserDetails} from '../../Services/UserManagementService';
import Loader from '../Loader';

export default function FormDialog(props) {
  const [open, setOpen] = React.useState(false);
  const [loading , setLoading] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  // form validation
  const INITIAL_FORM_STATE = {
    userName:  props.data.row.username,
    fName : props.data.row.fname,
    email : props.data.row.email,
    mobile : props.data.row.mobile,
};

const FORM_VALIDATION = Yup.object().shape({
  userName: Yup.string().required("Please Enter your username"),
  mobile : Yup.string()
  .matches(
      /^(?:0|94|\+94)?(?:(11|21|23|24|25|26|27|31|32|33|34|35|36|37|38|41|45|47|51|52|54|55|57|63|65|66|67|81|912)(0|2|3|4|5|7|9)|7(0|1|2|4|5|6|7|8)\d)\d{6}$/,
      "Invalid mobile format"
  )
  .required('Please Enter Your Mobile Number'),
  fName : Yup.string()
  .required('Please Enter Your first name'),
  email : Yup.string().matches(
    /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i,
    "Enter a valid email address"
  )
  .required('Please Enter Your Email')
});

  return (
    <div>
      <Button variant="contained" onClick={handleClickOpen} style={{width : '80px' , height : '40px'}}>
        Edit
      </Button>
      <div style={{width : '100px'}}>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Edit User Details</DialogTitle>
        <Formik
        initialValues={{
            ...INITIAL_FORM_STATE,
        }}
        validationSchema={FORM_VALIDATION}
        onSubmit={(values , {resetForm}) => {
            setLoading(true);
            // console.log(props.currentPage , props.postsPerPage);
            saveUserDetails(props.data.id , values.userName , values.fName , values.email , values.mobile , props.currentPage , props.postsPerPage , props.setUsers , props.setCount , setLoading);
            setOpen(false);
        }}
        >
        <Form>
        <DialogContent>
          <Grid container spacing={3} >
            <Grid item xs={12} sm={6} >
                <Textfield
                    type={"text"}
                    name="userName"
                    label="Username"
                    placeholder="Username"
                />
            </Grid>
            <Grid item xs={12} sm={6} >
              <Textfield
                    type={"text"}
                    name="email"
                    label="email"
                    placeholder="email"
                />
            </Grid>
          </Grid>
          <Grid container spacing={3} >
            <Grid item xs={12} sm={6} >
              <Textfield
                    type={"text"}
                    name="fName"
                    label="First Name"
                    placeholder="First Name"
                />
            </Grid>
            <Grid item xs={12} sm={6} >
              <Textfield
                    type={"text"}
                    name="mobile"
                    label="Mobile"
                    placeholder="Mobile"
                />
            </Grid>
          </Grid>
          
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button type='submit'>Save</Button>
        </DialogActions>
        </Form>
        </Formik>
        {loading && <Loader />}
      </Dialog>
      </div>
    </div>
  );
}