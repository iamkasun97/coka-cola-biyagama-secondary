import * as React from 'react';
import Grid from '@mui/material/Grid';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import dayjs from 'dayjs';
import { useState  , useEffect} from 'react';
import TextField from '@mui/material/TextField';
import SelectRoutesCustom from '../CustomComponent/SelectRoutesCustom';
// import SelectCustomNew from '../CustomComponent/SelectCustomNew';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import { DataGrid , GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import ExcelExport from '../CustomComponent/ExcelExport';
import { loadRouteConfiguration } from '../../Services/ConfigurationService';
import { getDetailsForDailySales } from '../../Services/ReportService';
import Loader from '../Loader';
import Test from './test';

const useFakeMutation = () => {
    return React.useCallback(
      (user) =>
        new Promise((resolve, reject) =>
          setTimeout(() => {
            if (user.name?.trim() === '') {
              reject(new Error("Error while saving user: name can't be empty."));
            } else {
              resolve({ ...user, name: user.name});
            }
          }, 200),
        ),
      [],
    );
  };
  

  function CustomToolbar(props) {

    return (
      <GridToolbarContainer>
        <ExcelExport count={props.count} startDate={props.startDate} endDate={props.endDate} routesIds={props.routesIds}/>
        <GridToolbarExport 
        csvOptions={{
            fileName: new Date(),
        }}
        />
      </GridToolbarContainer>
    );
  }


function DailySales(){

    const [visible , setVisible] = useState(false);
    const [rows , setRows] = useState([]);
    const [count , setCount] = useState(0);
    const [routes , setRoutes] = useState([]);
    const [routesIds , setRouteIds] = useState([]);
    const [loading , setLoading] = useState(false);
    const [currentPage , setCurrentPage] = useState(0);
    const [postsPerPage , setPostsPerPage] = useState(5);

    useEffect(() => {
        loadRouteConfiguration(setRoutes);
    },[]);


    const [date1 , setDate1] = useState(dayjs().format('YYYY-MM-DD'));
    const handleDateChange1 = (newValue) => {
        setDate1(dayjs(newValue).format('YYYY-MM-DD'));
    };

    const [date2 , setDate2] = useState(dayjs().format('YYYY-MM-DD'));
    const handleDateChange2 = (newValue) => {
        setDate2(dayjs(newValue).format('YYYY-MM-DD'));
    }

    // table data
    const [editedRow , setEditedRow] = useState([]);   
    
    const mutateRow = useFakeMutation();

    const [snackbar, setSnackbar] = React.useState(null);

    const handleCloseSnackbar = () => setSnackbar(null);

    const processRowUpdate = React.useCallback(
        async (newRow) => {
        // Make the HTTP request to save in the backend
        const response = await mutateRow(newRow);
        let temp = editedRow;
        temp.push(response);
        setEditedRow(temp);
        setSnackbar({ children: 'Data successfully Added to Submit', severity: 'success' });
        return response;
        },
        [mutateRow],
    );

    const handleProcessRowUpdateError = React.useCallback((error) => {
        setSnackbar({ children: error.message, severity: 'error' });
    }, []);

    const handleStart = () => {
        setVisible(true);
        setLoading(true);
        getDetailsForDailySales(date1 , date2 , routesIds.toString() ,currentPage , postsPerPage, setLoading , setRows , setCount);
    }

    //pagination functions
    const handleChangePage = (currentPage) => {
        setCurrentPage(currentPage);
        getDetailsForDailySales(date1 , date2 , routesIds.toString() ,currentPage , postsPerPage, setLoading , setRows , setCount);
    }

    const handleChangePageSize = (postsPerPage) => {
        setPostsPerPage(postsPerPage);
        getDetailsForDailySales(date1 , date2 , routesIds.toString() ,currentPage , postsPerPage, setLoading , setRows , setCount);
    }

    return (
        <div style={styles.mainDiv}>
            <Grid container spacing={3} >
                <Grid item xs={12} sm={3} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                        <DesktopDatePicker
                            inputFormat="MM/DD/YYYY"
                            value={date1}
                            onChange={handleDateChange1}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={3} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                        <DesktopDatePicker
                            inputFormat="MM/DD/YYYY"
                            value={date2}
                            onChange={handleDateChange2}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={3} >
                    <SelectRoutesCustom routes={routes} setRouteIds={setRouteIds} initialValue={routesIds}/>
                </Grid>
                <Grid item xs={12} sm={3} >
                    {
                    //   visible ? <Button variant="contained" style={{width : '100%' , height : '100%'}} onClick={() => handleCancel()}>Cancel</Button>
                    //   : 
                      <Button variant="contained" style={{width : '100%' , height : '100%'}} disabled={loading || routesIds.length == 0 || date1 === 'Invalid Date' || date2 === 'Invalid Date'} onClick={() => handleStart()}>Search</Button>
                    }
                </Grid>
            </Grid>
            {
            visible ? 
            <div style={styles.scroll}>
                <Box sx={{ height: 400, width: 600 }} style={styles.tableBox}>
                    <DataGrid
                        pagination
                        paginationMode="server"
                        rowCount={count}
                        rows={rows}
                        columns={columns}
                        pageSize={postsPerPage}
                        page={currentPage}
                        onPageSizeChange={(newValue) => handleChangePageSize(newValue)}
                        onPageChange={(newValue) => handleChangePage(newValue)}
                        rowsPerPageOptions={[5 , 10]}
                        processRowUpdate={processRowUpdate}
                        onProcessRowUpdateError={handleProcessRowUpdateError}
                        experimentalFeatures={{ newEditingApi: true }}
                        components={{
                            Toolbar: CustomToolbar,
                        }}
                        componentsProps={{ 
                            toolbar: { count: count , startDate : date1 , endDate : date2 , routesIds : routesIds.toString()} ,
                        }}
                    />
                    {!!snackbar && (
                        <Snackbar
                        open
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                        onClose={handleCloseSnackbar}
                        autoHideDuration={6000}
                        >
                        <Alert {...snackbar} onClose={handleCloseSnackbar} />
                        </Snackbar>
                    )}
                </Box>
            </div> : null
            }
            {loading && <Loader />}
        </div>
    )
}

export default DailySales;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    buttonDiv : {
        marginTop : '10px'
    },
    tableBox : {
        marginTop : '10px',
    },
    scroll : {
        overflowX : 'scroll',
        display : 'flex',
        justifyContent : 'center'
    }
}

const columns = [
    { field: 'date', 
    headerName: 'Date', 
    width: 200, 
    editable: false ,
    },
    { field: 'dailySales', 
        headerName: 'Daily Sales' ,
        width: 200, 
        editable: false ,
    },
    {
      field: 'dailyProfits',
      headerName: 'Daily Profits',
      width: 180,
      editable: false,
    },
  ];