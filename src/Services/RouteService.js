import * as api from '../api/index';

export const getAllRouteDetails = async (currentPage , postsPerPage , setRoutes , setCount) => {
    try{
        const data = await api.getRouteDetails(currentPage , postsPerPage);
        if(data.data.status === 'success'){
            setRoutes(data.data.data.data_for_the_loop);
            setCount(data.data.data.count);
        }
    }catch(error){
        // console.log(error);
    }
}

export const addNewRouteDetails = async (routeName , routeOwner ,currentPage , postsPerPage ,setRoutes , setCount , setLoading) => {
    try{
        const data = await api.addRouteDetails(routeName , routeOwner);
        if(data.data.status === 'success'){
            const dataNext = await api.getRouteDetails(currentPage , postsPerPage);
            if(dataNext.data.status === 'success'){
                setRoutes(dataNext.data.data.data_for_the_loop);
                setCount(dataNext.data.data.count);
            }
        }
        setLoading(false);
    }catch(error){
        // console.log(error);
        setLoading(false);
    }
}

export const editRouteDetails = async (id , routeName , routeOwner , currentPage , postsPerPage , setRoutes , setCount , setLoading) => {
    try{
        const data = await api.editRouteDetails(id , routeName , routeOwner);
        if(data.data.status === 'success'){
            const dataNext = await api.getRouteDetails(currentPage , postsPerPage);
            if(dataNext.data.status === 'success'){
                setRoutes(dataNext.data.data.data_for_the_loop);
                setCount(dataNext.data.data.count);
            }
        }
        setLoading(false);
    }catch(error){
        // console.log(error);
        setLoading(false);
    }
}

export const deleteRouteDetails = async (id , currentPage , postsPerPage , setRoutes , setCount , setLoading) => {
    try{
        const data = await api.deleteRouteDetails(id);
        if(data.data.status === 'success'){
            const dataNext = await api.getRouteDetails(currentPage , postsPerPage);
            if(dataNext.data.status === 'success'){
                setRoutes(dataNext.data.data.data_for_the_loop);
                setCount(dataNext.data.data.count);
            }
        }
        setLoading(false);
    }catch(error){
        // console.log(error);
        setLoading(false);
    }
}