import { Link, useLocation } from "react-router-dom";
import "./index.css";
import { SidebarData } from "./SidebarData";
import SubMenu from "./SubMenu";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { LOGOUT } from "../../actions/constants";
import { Avatar, Typography } from "@mui/material";
import { stringAvatar } from "../../utils/utils";

const Sidebar = (prop) => {
  const location = useLocation();
  const user = useSelector((state) => state.auth.authData);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const logout = () => {
    dispatch({ type: LOGOUT });
    window.location.href = "/anjelo/login";
    // window.location.href = "/login";
  };

  console.log(process.env.REACT_APP_NAME);

  return (
    <div>
      <header
        className={`header ${prop.show ? "space-toggle" : null}`}
        style={{
          width: location.pathname !== "/home" && "100%",
          height: location.pathname !== "/home" && "var(--header-height)",
          backgroundColor: location.pathname !== "/home" && "var(--white-color)",
        }}
      >
        <div className="header-toggle" onClick={() => prop.setShow(!prop.show)}>
          <i
            className={`fas fa-bars ${prop.show ? "fa-solid fa-xmark" : null}`}
          ></i>
        </div>
      </header>

      <aside className={`sidebar ${prop.show ? "show" : null}`}>
        <nav className="nav">
          <div style={{ minHeight: "80vh" }}>
            <Link to="/home" className="nav-logo">
              <img
                className="nav-logo-icon"
                src={require("../../assets/images/digiPOS logo.jpeg")}
                alt=""
              />
              <span className="nav-logo-name">
                <b>{process.env.REACT_APP_NAME}</b>
              </span>
            </Link>

            <div className="nav-user">
              <Avatar
                className="nav-user-avatar"
                {...stringAvatar(
                  // `${user?.userData?.fname} ${user?.userData?.lname}`
                  'A U'
                )}
              />
              <Typography className="nav-user-name" color="primary">
                {user?.userData?.fname} {user?.userData?.lname}
              </Typography>
            </div>

            <div className="nav-list">
              {SidebarData.map((item, index) => {
                if(item.role.includes(user?.data?.user_role)){
                  return <SubMenu show={prop.show} item={item} key={index} />;
                }
              })}
            </div>
          </div>

          <Link className="nav-link" onClick={() => logout()}>
            <i className="fas fa-sign-out nav-link-icon"></i>
            <span className="nav-link-name">Logout</span>
          </Link>
        </nav>
      </aside>
    </div>
  );
};

export default Sidebar;
