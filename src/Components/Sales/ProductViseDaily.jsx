import * as React from 'react';
import Grid from '@mui/material/Grid';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import dayjs from 'dayjs';
import { useState  , useEffect} from 'react';
import TextField from '@mui/material/TextField';
import SelectRoutesCustom from '../CustomComponent/SelectRoutesCustom';
// import SelectCustomNew from '../CustomComponent/SelectCustomNew';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import { DataGrid , GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import ExcelExport3 from '../CustomComponent/ExcelExport3';
import { loadConfigurationForProductWiseSummary } from '../../Services/ConfigurationService';
import { productWiseDaily } from '../../Services/ReportService';
import Loader from '../Loader';
import SelectCustom from '../CustomComponent/SelectCustom';
import SearchCustom from '../CustomComponent/SearchComponent';
import isEmpty from 'is-empty';

  function CustomToolbar(props) {

    return (
      <GridToolbarContainer>
        <ExcelExport3 props={props} />
        <GridToolbarExport 
            csvOptions={{
                fileName: new Date(),
            }}
        />
      </GridToolbarContainer>
    );
  }


function ProductViseDaily(){

    const [visible , setVisible] = useState(false);
    const [rows , setRows] = useState([]);
    const [count , setCount] = useState(0);
    const [routes , setRoutes] = useState([]);
    const [routesIds , setRouteIds] = useState([]);
    const [loading , setLoading] = useState(false);
    const [currentPage , setCurrentPage] = useState(0);
    const [postsPerPage , setPostsPerPage] = useState(5);
    const [products , setProducts] = useState([]);
    const [product , setProduct] = useState({});

    useEffect(() => {
        loadConfigurationForProductWiseSummary(setRoutes , setProducts);
    },[]);


    const [date1 , setDate1] = useState(dayjs().format('YYYY-MM-DD'));
    const handleDateChange1 = (newValue) => {
        setDate1(dayjs(newValue).format('YYYY-MM-DD'));
    };

    const [date2 , setDate2] = useState(dayjs().format('YYYY-MM-DD'));
    const handleDateChange2 = (newValue) => {
        setDate2(dayjs(newValue).format('YYYY-MM-DD'));
    }


    const handleStart = () => {
        setVisible(true);
        setLoading(true);
        productWiseDaily(date1 , date2 , routesIds , product.id ,product.sellingPrice , product.broughtPrice, currentPage , postsPerPage , setLoading , routes , setRows , setCount , product.name);
    }

    //pagination functions
    const handleChangePage = (currentPage) => {
        setCurrentPage(currentPage);
        productWiseDaily(date1 , date2 , routesIds , product.id ,product.sellingPrice , product.broughtPrice, currentPage , postsPerPage , setLoading , routes , setRows , setCount , product.name);
    }

    const handleChangePageSize = (postsPerPage) => {
        setPostsPerPage(postsPerPage);
        productWiseDaily(date1 , date2 , routesIds , product.id ,product.sellingPrice , product.broughtPrice, currentPage , postsPerPage , setLoading , routes , setRows , setCount , product.name);
    }

    return (
        <div style={styles.mainDiv}>
            <Grid container spacing={3} >
                <Grid item xs={12} sm={2} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                        <DesktopDatePicker
                            inputFormat="MM/DD/YYYY"
                            value={date1}
                            onChange={handleDateChange1}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={2} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                        <DesktopDatePicker
                            inputFormat="MM/DD/YYYY"
                            value={date2}
                            onChange={handleDateChange2}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={3} >
                    <SelectRoutesCustom routes={routes} setRouteIds={setRouteIds} initialValue={routesIds}/>
                </Grid>
                <Grid item xs={12} sm={2} >
                    <SearchCustom items={products} product={product} setProduct={setProduct}/>
                </Grid>
                <Grid item xs={12} sm={3} >
                    {
                      <Button variant="contained" style={{width : '100%' , height : '100%'}} disabled={loading || routesIds.length == 0 || date1 === 'Invalid Date' || date2 === 'Invalid Date' || isEmpty(product.id)} onClick={() => handleStart()}>Search</Button>
                    }
                </Grid>
            </Grid>
            {
            visible ? 
            <div >
                <Box  style={styles.tableBox}>
                    <DataGrid
                        pagination
                        paginationMode="server"
                        rowCount={count}
                        rows={rows}
                        columns={columns}
                        pageSize={postsPerPage}
                        page={currentPage}
                        onPageSizeChange={(newValue) => handleChangePageSize(newValue)}
                        onPageChange={(newValue) => handleChangePage(newValue)}
                        rowsPerPageOptions={[5 , 10]}
                        experimentalFeatures={{ newEditingApi: true }}
                        components={{
                            Toolbar: CustomToolbar,
                        }}
                        componentsProps={{ 
                            toolbar: { function : productWiseDaily , date1 , date2 , routesIds , product : product.id , sellingPrie : product.sellingPrice , broughtPrice : product.broughtPrice , routes , count , productName : product.name} 
                        }}
                    />
                </Box>
            </div> : null
            }
            {loading && <Loader />}
        </div>
    )
}

export default ProductViseDaily;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    buttonDiv : {
        marginTop : '10px'
    },
    tableBox : {
        marginTop : '10px',
        width : '100%',
        height : '400px'
    },
    scroll : {
        overflowX : 'scroll',
        display : 'flex',
        justifyContent : 'center'
    }
}

const columns = [
    { field: 'date', 
    headerName: 'Date', 
    width: 100, 
    editable: false ,
    },
    { field: 'route', 
        headerName: 'route' ,
        width: 200, 
        editable: false ,
    },
    {
      field: 'product',
      headerName: 'product',
      width: 250,
      editable: false,
    },
    {
        field: 'saleQty',
        headerName: 'sale quantity',
        width: 180,
        editable: false,
    },
    {
        field: 'salePrice',
        headerName: 'sale price',
        width: 180,
        editable: false,
    },
  ];
