import * as api from '../api/index';

export const getAllCaseDetails = async (currentPage , postsPerPage , setCases , setCount) => {
    try{
        const data = await api.getCaseDetails(currentPage , postsPerPage);
        if(data.data.status === 'success'){
            setCases(data.data.data.data_for_the_loop);
            setCount(data.data.data.count);
        }
    }catch(error){

    }
}

export const addNewCaseDetails = async (caseName , numberOfBottles ,currentPage,postsPerPage, setCases , setCount , setLoading) => {
    try{
        const data = await api.addCaseDetails(caseName , numberOfBottles);
        if(data.data.status === 'success'){
            const dataNext = await api.getCaseDetails(currentPage , postsPerPage);
            if(dataNext.data.status === 'success'){
                setCases(dataNext.data.data.data_for_the_loop);
                setCount(dataNext.data.data.count);
            }
        }

        setLoading(false);
    }catch(error){
        console.log(error);
        setLoading(false);
    }
}

export const editCaseDetails = async (id , caseName , numberOfBottles , currentPage , postsPerPage , setCases , setCount , setLoading) => {
    try{
        const data = await api.editCaseDetails(id , caseName , numberOfBottles);
        if(data.data.status === 'success'){
            const dataNext = await api.getCaseDetails(currentPage , postsPerPage);
            if(dataNext.data.status === 'success'){
                setCases(dataNext.data.data.data_for_the_loop);
                setCount(dataNext.data.data.count);
            }
        }

        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}