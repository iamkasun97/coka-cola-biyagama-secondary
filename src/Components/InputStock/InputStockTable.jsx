import Grid from '@mui/material/Grid';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';
import TextField from '@mui/material/TextField';
import * as React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import Loader from '../Loader';
import { startInputStockTransaction , publishInputStockTransaction } from '../../Services/InputStockService';

  const useFakeMutation = () => {
    return React.useCallback(
      (item) =>
        new Promise((resolve, reject) =>
          setTimeout(() => {
            if(item.cases === '' || isNaN(item.cases)){
              reject(new Error('Enter a valid value for cases!!'));
            }
            if(item.items === '' || isNaN(item.items)){
              reject(new Error('Enter a valid value for cases!!'));
            }
            if(item.broughtPrice === '' || isNaN(item.broughtPrice)){
              reject(new Error('Enter a valid value for brought price!!'));
            }
            if(item.sellingPrice === '' || isNaN(item.sellingPrice)){
              reject(new Error('Enter a valid value for selling price!!'));
            }
            resolve({...item , name : item.id});
          }, 200),
        ),
      [],
    );
  };

  function CustomFooterStatusComponent(props) {
    return (
      <Box  style={{  display: 'flex' , marginTop : '10px' , marginBottom : '10px' }}>
        <Grid container spacing={3} >
                <Grid item xs={12} sm={2} >
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                  Cases Issued - {props.sumCases}
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Items Issued - {props.sumItems}
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Total Items - {props.totalItems}
                </Grid>
                <Grid item xs={12} sm={4} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Total Items Brought Price - {props.total}
                </Grid>
        </Grid>
      </Box>
    );
  }


function InputStockTable(){

    const [sumCases , setSumCases] = useState(0);
    const [sumItems , setSumItems] = useState(0);
    const [totalItems , setTotalItems] = useState(0);
    const [total , setTotal] = useState(0);
    const [error , setError] = useState(false)

    // table data
    const [editedRow , setEditedRow] = useState([]);   
    const mutateRow = useFakeMutation();

    const [snackbar, setSnackbar] = React.useState(null);

    const handleCloseSnackbar = () => setSnackbar(null);

    const processRowUpdate = React.useCallback(
        async (newRow) => {
        // Make the HTTP request to save in the backend
        const response = await mutateRow(newRow);
        console.log(response);
        setEditedRow(prevState => [...prevState , {
          id : response.id,
          sellingPrice : response.sellingPrice,
          broughtPrice : response.broughtPrice,
          totalItems : Number(response.caseType)*Number(response.cases) + Number(response.items),
        }]);
        if(response.broughtPrice == 0 || response.sellingPrice == 0){
          setSnackbar({ children: 'selling or brought price rs.0 items will not added to submit', severity: 'success' });
        }else {
          setSnackbar({ children: 'Data successfully Added to Submit', severity: 'success' });
        }
        setError(false);
        return response;
        },
        [mutateRow],
    );
    const handleProcessRowUpdateError = React.useCallback((error) => {
        setSnackbar({ children: error.message, severity: 'error' });
        setError(true);
    }, []);
    

    const [transactionId , setTransactionId] = useState(null);
    const [loading , setLoading] = useState(false);
    const [date , setDate] = useState(dayjs().format('YYYY-MM-DD'));
    const [invoiceNumber , setInvoiceNumber] = useState('');

    // data
    const [rows , setRows] = useState([]);

    const handleDateChange = (newValue) => {
      setDate(dayjs(newValue).format('YYYY-MM-DD'));
  };

    const handleStart = () => {
      setLoading(true);
      startInputStockTransaction(date , invoiceNumber , setLoading , setTransactionId , setRows);
    }

    const handleCancel = () => {
      setTransactionId(null);
      setInvoiceNumber('');
      setEditedRow([]);
    }

    const handleSubmit = () => {
      let items = [];
      let ids = editedRow.map(item => item.id);
      let unique = ids.filter((v, i, a) => a.indexOf(v) === i);
      unique.forEach(i => {
        let temp = editedRow.filter(e => e.id === i);
        temp = temp.reverse();
        if(temp[0].broughtPrice == 0 || temp.sellingPrice == 0){}
        else {
          items.push({
            item_id : temp[0].id,
            total_bottle_quantity : temp[0].totalItems,
            bottle_brought_price : temp[0].broughtPrice,
            bottle_selling_price : temp[0].sellingPrice
          });
        }
      });
      setLoading(true);
      publishInputStockTransaction(transactionId , items , setTransactionId , setLoading , setInvoiceNumber , setEditedRow);
    }

    return (
        <div style={styles.mainDiv}>
            <Grid container spacing={3} >
                <Grid item xs={12} sm={4} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                    <DesktopDatePicker
                        inputFormat="MM/DD/YYYY"
                        value={date}
                        onChange={handleDateChange}
                        renderInput={(params) => <TextField {...params} />}
                    />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={4} >
                        <TextField
                            label="invoice number"
                            fullWidth
                            autoComplete="family-name"
                            variant="outlined"
                            value={invoiceNumber}
                            onChange={(event) => setInvoiceNumber(event.target.value)}
                        />
                </Grid>
                <Grid item xs={12} sm={1} >
                </Grid>
                <Grid item xs={12} sm={3} >
                    {
                      transactionId !== null ? <Button variant="contained" style={{width : '100%' , height : '100%'}} onClick={() => handleCancel()}>Cancel</Button>
                      : 
                      <Button variant="contained" style={{width : '100%' , height : '100%'}} disabled={loading || invoiceNumber === '' || date === 'Invalid Date'} onClick={() => handleStart()}>Start</Button>
                    }
                </Grid>
            </Grid>
            <hr color='black'/>
            {
              transactionId != null ?
              <div>
                  <Box sx={{ 
                      height: 400, 
                      width: '100%' ,
                      '& .super-app-theme--cell': {
                        backgroundColor: '#CCF1F1',
                        color: '#1a3e72',
                        fontWeight: '600',
                      }
                      }}>
                      <DataGrid
                          rows={rows}
                          columns={columns}
                          processRowUpdate={processRowUpdate}
                          onProcessRowUpdateError={handleProcessRowUpdateError}
                          experimentalFeatures={{ newEditingApi: true }}
                          onStateChange={(state) => {
                            let sumCasesInput = 0;
                            let sumItemInput = 0;
                            let totalBroughtPrice = 0;
                            let sumTotalItems = 0;
                            state.rows.ids.map(item => {
                              sumCasesInput += Number(state.rows.idRowsLookup[item].cases);
                              sumItemInput += Number(state.rows.idRowsLookup[item].items);
                              totalBroughtPrice += (Number(state.rows.idRowsLookup[item].cases)*Number(state.rows.idRowsLookup[item].caseType)+Number(state.rows.idRowsLookup[item].items))*Number(state.rows.idRowsLookup[item].broughtPrice);
                              sumTotalItems += Number(state.rows.idRowsLookup[item].cases)*Number(state.rows.idRowsLookup[item].caseType)+Number(state.rows.idRowsLookup[item].items);
                            });
                            setSumCases(new Intl.NumberFormat().format(sumCasesInput));
                            setTotal(new Intl.NumberFormat().format(parseFloat(totalBroughtPrice).toFixed(2)));
                            setSumItems(new Intl.NumberFormat().format(sumItemInput));
                            setTotalItems(new Intl.NumberFormat().format(sumTotalItems));
                          }}
                          components={{
                            Footer: CustomFooterStatusComponent,
                          }}
                          componentsProps={{
                            footer: { total , sumItems , sumCases , totalItems},
                          }}
                      />
                      {!!snackbar && (
                          <Snackbar
                          open
                          anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                          onClose={handleCloseSnackbar}
                          autoHideDuration={6000}
                          >
                          <Alert {...snackbar} onClose={handleCloseSnackbar} />
                          </Snackbar>
                      )}
                  </Box>
                  <Grid container spacing={3} >
                      <Grid item xs={12} sm={9} >
                      </Grid>
                      <Grid item xs={12} sm={3} >
                          <div style={styles.buttonDiv}>
                          <Button variant="contained" style={{width : '100%' , height : '55px'}} onClick={() => handleSubmit()} disabled={error || editedRow.length === 0}>Submit</Button>
                          </div>
                      </Grid>
                  </Grid>
            </div> : null
            }
            {loading && <Loader />}
        </div>
    )
}

export default InputStockTable;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    buttonDiv : {
        marginTop : '10px'
    }
}

const columns = [
    { field: 'item', headerName: 'Item', width: 160, editable: false },
    { field: 'caseType', headerName: 'Case Type' ,width: 140, editable: false },
    { 
      field: 'cases', 
      headerName: 'Cases',
      width: 140, 
      editable: true,
      cellClassName: 'super-app-theme--cell',
      renderCell : (params) => {
        return new Intl.NumberFormat().format(params.row.cases);
      }
    },
    {
      field: 'items',
      headerName: 'Items',
      width: 140,
      editable: true,
      cellClassName: 'super-app-theme--cell',
      renderCell : (params) => {
        return new Intl.NumberFormat().format(params.row.items);
      }
    },
    {
      field: 'totalItems',
      headerName: 'Total Items',
      width: 140,
      editable: false,
      renderCell : (params) => {
        if(params.row.caseType === undefined || params.row.cases === undefined || params.row.items === undefined){
          return 0;
        }
        return new Intl.NumberFormat().format(Number(params.row.caseType)*Number(params.row.cases) + Number(params.row.items));
      }
    },
    {
      field: 'broughtPrice',
      headerName: 'Brought Price',
      width: 140,
      editable: true,
      cellClassName: 'super-app-theme--cell',
    },
    {
      field: 'totalBroughtPrice',
      headerName: 'Total Brought Price',
      width: 160,
      editable: false,
      renderCell : (params) => {
        if(params.row.caseType === undefined || params.row.cases === undefined || params.row.items === undefined || params.row.broughtPrice === undefined){
          return 0;
        }
        return new Intl.NumberFormat().format(parseFloat((Number(params.row.caseType)*Number(params.row.cases) + Number(params.row.items))*Number(params.row.broughtPrice)).toFixed(2));
      }
    },
    {
      field: 'sellingPrice',
      headerName: 'sellingPrice',
      width: 140,
      editable: true,
      cellClassName: 'super-app-theme--cell',
    }
  ];
