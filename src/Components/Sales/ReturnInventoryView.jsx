import Grid from '@mui/material/Grid';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';
import TextField from '@mui/material/TextField';
import * as React from 'react';
import Button from '@mui/material/Button';
import { DataGrid } from '@mui/x-data-grid';
import Box from '@mui/material/Box';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { returnStockView } from '../../Services/ViewService';
import { GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid';
import Loader from '../Loader';

  function CustomFooterStatusComponent(props) {
    return (
      <Box  style={{  display: 'flex' , marginTop : '10px' , marginBottom : '10px' }}>
        <Grid container spacing={3} >
                <Grid item xs={12} sm={1} >
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                  Cases Issued - {props.sumCases}
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                  Items Issued - {props.sumItems}
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                  Total Items Issued - {props.totalItems}
                </Grid>
                <Grid item xs={12} sm={4} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Total Items Brought Price - {props.totalBroughtPrice}
                </Grid>
        </Grid>
      </Box>
    );
  }

  function CustomToolbar() {

    return (
      <GridToolbarContainer>
        <GridToolbarExport 
        csvOptions={{
            fileName: new Date(),
        }}
        />
      </GridToolbarContainer>
    );
  }

function ReturnInventoryTable(){

    const [sumCases , setSumCases] = useState(0);
    const [sumItems , setSumItems] = useState(0);
    const [totalItems , setTotalItems] = useState(0);
    const [totalBroughtPrice , setTotalBroughtPrice] = useState(0);

    const [date , setDate] = useState(dayjs().format('YYYY-MM-DD'));
    const [returnInvoice , setReturnInvoice] = useState('');
    const [transactionId , setTransactionId] = useState(null);
    const [loading , setLoading] = useState(false);

    const handleDateChange = (newValue) => {
        setDate(dayjs(newValue).format('YYYY-MM-DD'));
    };

    const [rows , setRows] = useState([]);

    const handleStart = () => {
      setLoading(true);
      returnStockView(date , returnInvoice , setLoading , setTransactionId , setRows);
    }

    const handleCancel = () => {
      setTransactionId(null);
      setReturnInvoice('');
    }
    

    return (
        <div style={styles.mainDiv}>
            <Grid container spacing={3} >
                <Grid item xs={12} sm={4} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                    <DesktopDatePicker
                        inputFormat="MM/DD/YYYY"
                        value={date}
                        onChange={handleDateChange}
                        renderInput={(params) => <TextField {...params} />}
                    />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={4} >
                        <TextField
                            label="return invoice number"
                            fullWidth
                            autoComplete="family-name"
                            variant="outlined"
                            value={returnInvoice}
                            onChange={(event) => setReturnInvoice(event.target.value)}
                        />
                </Grid>
                <Grid item xs={12} sm={4} >
                    {
                      transactionId !== null ? <Button variant="contained" style={{width : '100%' , height : '100%'}} onClick={() => handleCancel()}>Cancel</Button>
                      : 
                      <Button variant="contained" style={{width : '100%' , height : '100%'}} disabled={loading || returnInvoice === '' || date === 'Invalid Date'} onClick={() => handleStart()}>Search</Button>
                    }
                </Grid>
            </Grid>
            <hr color='black'/>
            {
              transactionId !== null ?
              <div>
                <Box sx={{ height: 400, width: '100%' }}>
                  <DataGrid
                      rows={rows}
                      columns={columns}
                      experimentalFeatures={{ newEditingApi: true }}
                      onStateChange={(state) => {
                        let sumCasesReturn = 0;
                        let sumItemReturn = 0;
                        let totalBroughtPrice = 0;
                        let totalItemsReturned = 0;
                        state.rows.ids.map(item => {
                          sumCasesReturn += Number(state.rows.idRowsLookup[item].cases);
                          sumItemReturn += Number(state.rows.idRowsLookup[item].items);
                          totalBroughtPrice += (Number(state.rows.idRowsLookup[item].cases)*Number(state.rows.idRowsLookup[item].caseType)+Number(state.rows.idRowsLookup[item].items))*Number(state.rows.idRowsLookup[item].broughtPrice);
                          totalItemsReturned += Number(state.rows.idRowsLookup[item].cases)*Number(state.rows.idRowsLookup[item].caseType)+Number(state.rows.idRowsLookup[item].items);
                        });
                        setSumCases(new Intl.NumberFormat().format(parseFloat(sumCasesReturn).toFixed(2)));
                        setTotalBroughtPrice(new Intl.NumberFormat().format(parseFloat(totalBroughtPrice).toFixed(2)));
                        setSumItems(new Intl.NumberFormat().format(parseFloat(sumItemReturn).toFixed(2)));
                        setTotalItems(new Intl.NumberFormat().format(parseFloat(totalItemsReturned).toFixed(2)));
                      }}
                      components={{
                        Footer: CustomFooterStatusComponent,
                        Toolbar : CustomToolbar
                      }}
                      componentsProps={{
                        footer: { totalItems , sumItems , sumCases , totalBroughtPrice},
                      }}
                  />
              </Box>
              </div> : null
            }
            {loading && <Loader />}
        </div>
    )
}

export default ReturnInventoryTable;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    buttonDiv : {
        marginTop : '10px'
    }
}

const columns = [
    { field: 'item', headerName: 'Item', width: 180, editable: false },
    // { field: 'totalStock', headerName: 'Total Stock' ,width: 180, editable: false },
    {
      field: 'caseType',
      headerName: 'Case Type',
      width: 180,
      editable: false,
    },
    {
      field: 'cases',
      headerName: 'Cases',
      width: 180,
      editable: false,
    },
    {
      field: 'items',
      headerName: 'Items',
      width: 180,
      editable: false,
    },
    {
      field: 'totalItems',
      headerName: 'Total Items',
      width: 180,
      editable: false,
    },
    {
      field: 'broughtPrice',
      headerName: 'Brought Price',
      width: 180,
      editable: false,
    },
    {
      field: 'totalBroughtPrice',
      headerName: 'Total Brought Price',
      width: 180,
      editable: false,
      renderCell : (params) => {
        if(params.row.caseType === undefined || params.row.cases === undefined || params.row.items === undefined || params.row.broughtPrice === undefined){
          return 0;
        }
        new Intl.NumberFormat().format(parseFloat((Number(params.row.caseType)*Number(params.row.cases) + Number(params.row.items))*Number(params.row.broughtPrice)).toFixed(2));
        return Math.floor;
      }
    }
  ];
