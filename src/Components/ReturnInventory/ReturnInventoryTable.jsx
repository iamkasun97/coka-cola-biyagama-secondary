import Grid from '@mui/material/Grid';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';
import TextField from '@mui/material/TextField';
import * as React from 'react';
import Button from '@mui/material/Button';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import { DataGrid } from '@mui/x-data-grid';
import Box from '@mui/material/Box';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { startReturnStockTransaction , publishReturnStockTransaction } from '../../Services/ReturnStockService';
import Loader from '../Loader';


const useFakeMutation = () => {
    return React.useCallback(
      (item) =>
        new Promise((resolve, reject) =>
          setTimeout(() => {
            if(item.cases === '' || isNaN(item.cases)){
              reject(new Error('enter a valid number for cases!!'));
            }
            if(item.items === '' || isNaN(item.items)){
              reject(new Error('enter a valid number for items!!'));
            }
            if(item.broughtPrice === '' || isNaN(item.broughtPrice)){
              reject(new Error('enter a valid number for items!!'));
            }
            if(Number(item.caseType)*Number(item.cases)+Number(item.items) > Number(item.totalStock)){
              reject(new Error('Return Amount exceed total stock!!'));
            }
            resolve({...item , name : item.id})
          }, 200),
        ),
      [],
    );
  };

  function CustomFooterStatusComponent(props) {
    return (
      <Box  style={{  display: 'flex' , marginTop : '10px' , marginBottom : '10px' }}>
        <Grid container spacing={3} >
                <Grid item xs={12} sm={1} >
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                  Cases Issued - {props.sumCases}
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                  Items Issued - {props.sumItems}
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                  Total Items Issued - {props.totalItems}
                </Grid>
                <Grid item xs={12} sm={4} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Total Items Brought Price - {props.totalBroughtPrice}
                </Grid>
        </Grid>
      </Box>
    );
  }

function ReturnInventoryTable(){

    const [sumCases , setSumCases] = useState(0);
    const [sumItems , setSumItems] = useState(0);
    const [totalItems , setTotalItems] = useState(0);
    const [totalBroughtPrice , setTotalBroughtPrice] = useState(0);
    const [error , setError] = useState(false);

    // table data
    const [editedRow , setEditedRow] = useState([]);   
    const mutateRow = useFakeMutation();

    const [snackbar, setSnackbar] = React.useState(null);

    const handleCloseSnackbar = () => setSnackbar(null);

    const processRowUpdate = React.useCallback(
        async (newRow) => {
        // Make the HTTP request to save in the backend
        const response = await mutateRow(newRow);
        setEditedRow(prevState => [...prevState , {
          id : response.id,
          totalItems : Number(response.caseType)*Number(response.cases) + Number(response.items),
        }]);
        setSnackbar({ children: 'Data successfully Added to Submit', severity: 'success' });
        setError(false);
        return response;
        },
        [mutateRow],
    );

    const handleProcessRowUpdateError = React.useCallback((error) => {
        setSnackbar({ children: error.message, severity: 'error' });
        setError(true);
    }, []);

    const handleDateChange = (newValue) => {
        setDate(dayjs(newValue).format('YYYY-MM-DD'));
    };


    const [transactionId , setTransactionId] = useState(null);
    const [loading , setLoading] = useState(false);
    const [returnInvoiceNumber , setReturnInvoiceNumber] = useState('');
    const [date , setDate] = useState(dayjs().format('YYYY-MM-DD'));

    // data
    const [rows , setRows] = useState([]);

    const handleStart = () => {
      setLoading(true);
      startReturnStockTransaction(date , returnInvoiceNumber , setLoading , setTransactionId , setRows);
    }

    const handleCancel = () => {
      setTransactionId(null);
      setReturnInvoiceNumber('');
      setEditedRow([]);
    }

    const handleSubmit = () => {
      let items = [];
      let ids = editedRow.map(item => item.id);
      let unique = ids.filter((v, i, a) => a.indexOf(v) === i);
      unique.forEach(i => {
        let temp = editedRow.filter(e => e.id === i);
        temp = temp.reverse();
        items.push({
          stock_summary_data_id : temp[0].id,
          total_bottle_quantity : temp[0].totalItems,
        });
      });
      setLoading(true);
      publishReturnStockTransaction(transactionId , items , setTransactionId , setLoading , setReturnInvoiceNumber , setEditedRow);
    }

    return (
        <div style={styles.mainDiv}>
            <Grid container spacing={3} >
                <Grid item xs={12} sm={4} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                    <DesktopDatePicker
                        inputFormat="MM/DD/YYYY"
                        value={date}
                        onChange={handleDateChange}
                        renderInput={(params) => <TextField {...params} />}
                    />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={4} >
                        <TextField
                            label="return invoice number"
                            fullWidth
                            autoComplete="family-name"
                            variant="outlined"
                            value={returnInvoiceNumber}
                            onChange={(event) => setReturnInvoiceNumber(event.target.value)}
                        />
                </Grid>
                <Grid item xs={12} sm={1} >
                </Grid>
                <Grid item xs={12} sm={3} >
                    {
                      transactionId !== null ? <Button variant="contained" style={{width : '100%' , height : '100%'}} onClick={() => handleCancel()}>Cancel</Button>
                      : 
                      <Button variant="contained" style={{width : '100%' , height : '100%'}} disabled={loading || returnInvoiceNumber === '' || date === 'Invalid Date'} onClick={() => handleStart()}>Start</Button>
                    }
                </Grid>
            </Grid>
            <hr color='black'/>
            {
              transactionId != null ? 
              <div>
                  <Box sx={{ 
                        height: 400, 
                        width: '100%' ,
                        '& .super-app-theme--cell': {
                          backgroundColor: '#CCF1F1',
                          color: '#1a3e72',
                          fontWeight: '600',
                        }
                        }}>
                  <DataGrid
                      rows={rows}
                      columns={columns}
                      processRowUpdate={processRowUpdate}
                      onProcessRowUpdateError={handleProcessRowUpdateError}
                      experimentalFeatures={{ newEditingApi: true }}
                      onStateChange={(state) => {
                        let sumCasesReturn = 0;
                        let sumItemReturn = 0;
                        let totalBroughtPrice = 0;
                        let totalItemsReturned = 0;
                        state.rows.ids.map(item => {
                          sumCasesReturn += Number(state.rows.idRowsLookup[item].cases);
                          sumItemReturn += Number(state.rows.idRowsLookup[item].items);
                          totalBroughtPrice += (Number(state.rows.idRowsLookup[item].cases)*Number(state.rows.idRowsLookup[item].caseType)+Number(state.rows.idRowsLookup[item].items))*Number(state.rows.idRowsLookup[item].broughtPrice);
                          totalItemsReturned += Number(state.rows.idRowsLookup[item].cases)*Number(state.rows.idRowsLookup[item].caseType)+Number(state.rows.idRowsLookup[item].items);
                        });
                        setSumCases(sumCasesReturn);
                        setTotalBroughtPrice(parseFloat(totalBroughtPrice).toFixed(2));
                        setSumItems(sumItemReturn);
                        setTotalItems(totalItemsReturned);
                      }}
                      components={{
                        Footer: CustomFooterStatusComponent,
                      }}
                      componentsProps={{
                        footer: { totalItems , sumItems , sumCases , totalBroughtPrice},
                      }}
                  />
                  {!!snackbar && (
                      <Snackbar
                      open
                      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                      onClose={handleCloseSnackbar}
                      autoHideDuration={6000}
                      >
                      <Alert {...snackbar} onClose={handleCloseSnackbar} />
                      </Snackbar>
                  )}
              </Box>
                <Grid container spacing={3} >
                    <Grid item xs={12} sm={9} >
                    </Grid>
                    <Grid item xs={12} sm={3} >
                        <div style={styles.buttonDiv}>
                        <Button variant="contained" style={{width : '100%' , height : '55px'}} disabled={error} onClick={() => handleSubmit()}>Submit</Button>
                        </div>
                    </Grid>
                </Grid>
              </div> : null
            }
            {loading && <Loader />}
        </div>
    )
}

export default ReturnInventoryTable;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    buttonDiv : {
        marginTop : '10px'
    }
}

const columns = [
    { field: 'item', headerName: 'Item', width: 180, editable: false },
    { field: 'totalStock', headerName: 'Total Stock' ,width: 180, editable: false },
    {
      field: 'caseType',
      headerName: 'Case Type',
      width: 180,
      editable: false,
    },
    {
      field: 'cases',
      headerName: 'Cases',
      width: 180,
      editable: true,
      cellClassName: 'super-app-theme--cell',
    },
    {
      field: 'items',
      headerName: 'Items',
      width: 180,
      editable: true,
      cellClassName: 'super-app-theme--cell',
    },
    {
      field: 'totalItems',
      headerName: 'Total Items',
      width: 180,
      editable: false,
      renderCell : (params) => {
        if(params.row.caseType === undefined || params.row.cases === undefined || params.row.items === undefined){
          return 0;
        }
        return Number(params.row.caseType)*Number(params.row.cases) + Number(params.row.items);
      }
    },
    {
      field: 'broughtPrice',
      headerName: 'Brought Price',
      width: 180,
      editable: false,
    },
    {
      field: 'totalBroughtPrice',
      headerName: 'Total Brought Price',
      width: 180,
      editable: false,
      renderCell : (params) => {
        if(params.row.caseType === undefined || params.row.cases === undefined || params.row.items === undefined || params.row.broughtPrice === undefined){
          return 0;
        }
        return parseFloat((Number(params.row.caseType)*Number(params.row.cases) + Number(params.row.items))*Number(params.row.broughtPrice)).toFixed(2);
      }
    },
    {
      field: 'sellingPrice',
      headerName: 'Selling Price',
      width: 180,
      editable: false,
    },
  ];

