import {useState} from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { Visibility } from '@mui/icons-material';
import DialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import Textfield from '../FormsUI/Textfield';
import * as Yup from "yup";
import { Form, Formik } from "formik";
import {updatepassword} from '../../Services/UserManagementService';
import Loader from '../Loader';

export default function FormDialog(props) {
  const [open, setOpen] = useState(false);
  const [loading , setLoading] = useState(false);
  const [showPassword , setShowPassword] = useState(false);
  const [showPassword1 , setShowPassword1] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  // form validation
  const INITIAL_FORM_STATE = {
    password:  "",
    cPassword : "",
};

const FORM_VALIDATION = Yup.object().shape({
    password: Yup.string()
    .required("Please Enter your password")
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/,
      "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
    ),
    cPassword: Yup.string()
          .required("Please Enter Confirm password")
          .oneOf([Yup.ref("password")], "Passwords do not match")
});

  return (
    <div>
      <Button variant="contained" onClick={handleClickOpen} style={{width : '160px' , height : '40px'}}>
        Change Password
      </Button>
      <div style={{width : '100px'}}>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Change User Password</DialogTitle>
        <Formik
        initialValues={{
            ...INITIAL_FORM_STATE,
        }}
        validationSchema={FORM_VALIDATION}
        onSubmit={(values , {resetForm}) => {
            setLoading(true);
            setTimeout(() => {
                // console.log(values);
                updatepassword(props.data.id , values.password , values.cPassword , setLoading);
                setOpen(false);
            }, 1000);
        }}
        >
        <Form>
        <DialogContent>
          <Grid container spacing={3} >
            <Grid item xs={12} sm={6} >
                <Textfield
                    type={showPassword ? "text" : "password"}
                    InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                        <IconButton
                            aria-label="toggle password visibility"
                            onClick={() => setShowPassword(!showPassword)}
                            edge="end"
                        >
                            {showPassword ? (
                            <VisibilityOff />
                            ) : (
                            <Visibility />
                            )}
                        </IconButton>
                        </InputAdornment>
                    ),
                    }}
                    name="password"
                    label="Password"
                    placeholder="Password"
                />
            </Grid>
            <Grid item xs={12} sm={6} >
                <Textfield
                    type={showPassword1 ? "text" : "password"}
                    InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                        <IconButton
                            aria-label="toggle password visibility"
                            onClick={() => setShowPassword1(!showPassword1)}
                            edge="end"
                        >
                            {showPassword1 ? (
                            <VisibilityOff />
                            ) : (
                            <Visibility />
                            )}
                        </IconButton>
                        </InputAdornment>
                    ),
                    }}
                    name="cPassword"
                    label="Confirm Password"
                    placeholder="Confirm Password"
                />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button type='submit'>Save</Button>
        </DialogActions>
        </Form>
        </Formik>
        {loading && <Loader />}
      </Dialog>
      </div>
    </div>
  );
}