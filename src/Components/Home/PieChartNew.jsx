import React, { PureComponent , useContext } from 'react';
import { PieChart, Pie, Sector, Cell, ResponsiveContainer , Tooltip } from 'recharts';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import salesContext from '../../context/salesContext';

function PieChart1(){

  const salesData = useContext(salesContext);
    return (
            <ResponsiveContainer width="100%" height="100%" >
              <Card style={styles.pieCard}>
                <CardContent>
                  <Typography variant="h6" gutterBottom>
                          Route Vise Sales
                  </Typography>
                  <PieChart width={400} height={400}>
                    <Pie
                      dataKey="value"
                      data={salesData?.routeSales}
                      cx={200}
                      cy={150}
                      innerRadius={80}
                      outerRadius={120}
                      fill="##000505"
                    />
                    <Tooltip />
                </PieChart>
                </CardContent>
                </Card>
            </ResponsiveContainer>
    )
}

export default PieChart1;

const styles = {
  pieCard : {
    height : '395px',
    overflowX: 'scroll',
    margin : '20px'
  }
}
