import api from './interceptor';

export const login = (data) => {
    // console.log(data);

    let sendData = {
        username : data.username,
        password : data.password
    };


    return api.post('/api/v1/user/autheticate' , sendData);
}

// manage users apis
export const registerSubUser = (userName , fName , email , mobile , password , cPassword) => {
    let sendData = {
        username : userName,
        fname : fName,
        email : email,
        mobile : mobile,
        password : password,
        c_password : cPassword
    };
    // console.log(sendData);

    return api.post('/api/v1/user/account-sub-management/add-sub/account' , sendData);
}

export const getAllUsers = (currentPage , postsPerPage) => {
    let sendData = null;

    let params = {
        currentPage : currentPage,
        postsPerPage : postsPerPage
    };

    return api.post('/api/v1/user/account-sub-management/view-all-sub/accounts' , sendData , {params : params});
}

export const saveUserDetails = (id , userName , fName , email , mobile ) => {
    let sendData = {
        id : id,
        username : userName,
        email : email,
        fname : fName,
        mobile : mobile
    };

    return api.post('/api/v1/user/account-sub-management/update-sub/account' , sendData);
}

export const updatepassword = (id , password , cPassword) => {
    let sendData = {
        id : id,
        password : password,
        c_password : cPassword
    };
  
    return api.post('/api/v1/user/account-sub-management/update-sub/account-password' , sendData);
}

// manage route apis
export const getRouteDetails = (currentPage , postsPerPage) => {
    let sendData = null;

    let params = {
        currentPage: currentPage,
        postsPerPage: postsPerPage,
    };
    
    return api.post("/api/v1/user/route/management/get-all-routes", sendData, {
    params: params,
    });
}

export const addRouteDetails = (routeName , routeOwner) => {
    let sendData = {
        route_name: routeName,
        route_owner: routeOwner,
      };
    
      return api.post("/api/v1/user/route/management/add-route", sendData);
}


export const editRouteDetails = (id , routeName , routeOwner) => {
    let sendData = {
        id: id,
        route_name: routeName,
        route_owner: routeOwner,
      };
    
      return api.post("/api/v1/user/route/management/edit-route", sendData);
}

export const deleteRouteDetails = (id) => {
    let sendData = {
        id: id,
      };
    
      return api.post("/api/v1/user/route/management/delete-route", sendData);
}


// manage case details
export const getCaseDetails = (currentPage , postsPerPage) => {
    let sendData = null;
    let params = {
        currentPage: currentPage,
        postsPerPage: postsPerPage,
    };

    return api.post(
        "/api/v1/user/case-type/management/get-all-case-types",
        sendData,
        { params: params }
    );
}

export const addCaseDetails = (caseName , numberOfBottles) => {
    let sendData = {
        case_name: caseName,
        number_of_bottles: numberOfBottles,
      };
    
      return api.post("/api/v1/user/case-type/management/add-case-type", sendData);
}

export const editCaseDetails = (id , caseName , numberOfBottles) => {
    let sendData = {
        id: id,
        case_name: caseName,
        number_of_bottles: numberOfBottles,
      };
    
      return api.post("/api/v1/user/case-type/management/edit-case-type", sendData);
}

export const deleteCaseDetails = (id) => {
    let sendData = {
        id: id,
      };
    
      return api.post(
        "/api/v1/user/case-type/management/delete-case-type",
        sendData
      );
}

// input stock apis
export const startInputStockTransaction = (date , invoiceId) => {
    let sendData = {
        to_date : date,
        reference_id : invoiceId
    };

    return api.post('/api/v1/user/vendor-stock/increment/add-stock-begin-transaction' , sendData);
}

export const addItemsToStock = (transactionId , items) => {
    let sendData = {
        transaction_id : transactionId,
        items : items
    };

    return api.post('/api/v1/user/vendor-stock/increment/transaction/add-items' , sendData);
}

export const publishInputStockTransaction = (transactionId) => {
    let sendData = {
        id : transactionId
    };

    return api.post('/api/v1/user/vendor-stock/increment/transaction/publish' , sendData);
}

// get stock items without pagination
export const getStockItems = () => {
    let sendData = null;

    return api.post('/api/v1/user/item/management/get-all-items/with-out-paging' , sendData);
}


// get configuration details
export const loadConfiguration = () => {
    let sendData = null;

    return api.post('/api/v1/user/common-func/management/get' , sendData);
}

// product details apis
export const getProducts = (currentPage , postsPerPage) => {
    let sendData = null;
    let params = {
        currentPage : currentPage,
        postsPerPage: postsPerPage
    };

    return api.post('/api/v1/user/item/management/get-all-items' , sendData , {params : params});
}

export const addProduct = (productName , caseType) => {
    let sendData = {
        item_name : productName,
        case_type_id : caseType
    };

    return api.post('/api/v1/user/item/management/add-item' , sendData);
}

export const editProduct = (id , productName , caseType) => {
    let sendData = {
        id : id,
        item_name : productName,
        case_type_id : caseType
    };

    return api.post('/api/v1/user/item/management/edit-item' , sendData);
}

// return stock for coka cola
export const startReturnStockTransaction = (date , returnInvoiceId) => {
    let sendData = {
        to_date : date,
        reference_id : returnInvoiceId
    }

    return api.post('/api/v1/user/vendor-stock/return/add-return-begin-transaction' , sendData)
}


export const addItemsToReturnStock = (transactionId , items) => {
    let sendData = {
        transaction_id : transactionId,
        items : items
    };

    return api.post('/api/v1/user/vendor-stock/return/transaction/add-items' , sendData);
}

export const publishReturnStockTransaction = (transactionId) => {
    let sendData = {
        id : transactionId
    };

    return api.post('/api/v1/user/vendor-stock/return/transaction/publish' , sendData);
}

export const getStockSummary = () => {
    let sendData = null;

    return api.post('/api/v1/user/common-func/management/get-all-visible-items/stock-summary' , sendData);
}

// issue stock apis
export const startIssueStockTransaction = (date , routeId , issueNumber) => {
    let sendData = {
        to_date : date,
        route_id : routeId,
        route_monitor_id : issueNumber
    };

    return api.post('/api/v1/user/route/daily-allocation/begin-transaction' , sendData);
}

export const getStockSummaryForIssue = () => {
    let sendData = null;

    return api.post('/api/v1/user/common-func/management/get-all-visible-items/stock-summary' , sendData);
}

export const addItemsToRoute = (transactionId , items) => {
    let sendData = {
        transaction_id : transactionId,
        items : items
    };

    return api.post('/api/v1/user/route/daily-allocation/transaction/add-items' , sendData);
}

export const publishIssueStockTransaction = (transactionId) => {
    let sendData = {
        id : transactionId
    };

    return api.post('/api/v1/user/route/daily-allocation/transaction/publish' , sendData);
}

// route returns api
export const startReturnRouteTransactions = (date , route , issueNumber) => {
    let sendData = {
        to_date : date,
        route_id : route,
        route_monitor_id : issueNumber
    };

    return api.post('/api/v1/user/route/return/begin-transaction' , sendData);
}

export const addItemsToRouteReturn = (transactionId , items) => {
    let sendData = {
        transaction_id : transactionId,
        items : items
    };

    return api.post('/api/v1/user/route/return/transaction/add-items' , sendData);
}

export const publishRouteReturnTransaction = (transactionId) => {
    let sendData = {
        id : transactionId
    };

    return api.post('/api/v1/user/route/return/transaction/publish' , sendData);
}

export const getStockSummaryForRouteReturn = () => {
    let sendData = null;

    return api.post('/api/v1/user/common-func/management/get-all-visible-items/stock-summary' , sendData);
}

// collection details apis
export const getCollectionTypes = (currentPage , postsPerPage) => {
    let sendData = null;
    let params = {
        currentPage : currentPage,
        postsPerPage : postsPerPage
    };

    return api.post('/api/v1/user/collection-type/management/view-all-collection-types' , sendData , {params : params});
}

export const addCollectionType = (collectionType , collectionSubType , value) => {
    let sendData = {
        collection_type : collectionType,
        collection_sub_type : collectionSubType,
        value : value
    };

    return api.post('/api/v1/user/collection-type/management/add-collection-type' , sendData);
}

export const editCollectionType = (id , collectionType , collectionSubType , value) => {
    let sendData = {
        id : id,
        collection_type : collectionType,
        collection_sub_type : collectionSubType,
        value : value
    }

    return api.post('/api/v1/user/collection-type/management/edit-collection-type' , sendData);
}


// issue stock view
export const issueStockView = (date , route , issueNumber) => {
    let sendData = {
        to_date : date,
        route_id : route,
        route_monitor_id : issueNumber
    };

    return api.post('/api/v1/user/route/daily-allocation/get-transaction-by-date-route-route-monitor-id' , sendData);
}

// route return stock view
export const routeReturnStockView = (date , route , issueNumber) => {
    let sendData = {
        to_date : date,
        route_id : route,
        route_monitor_id : issueNumber
    };

    return api.post('/api/v1/user/route/return/get-transaction-by-date-route-route-monitor-id' , sendData);
}

//  input stock view
export const inputStockView = (date , invoiceNumber) => {
    let sendData = {
        to_date : date,
        reference_id : invoiceNumber
    };

    return api.post('/api/v1/user/vendor-stock/increment/get-transaction-by-date-route-route-monitor-id' , sendData);
}

// return stock view
export const returnStockView = (date , returnInvoiceNumber) => {
    let sendData = {
        to_date : date,
        reference_id : returnInvoiceNumber
    };

    return api.post('/api/v1/user/vendor-stock/return/get-transaction-by-date-route-route-monitor-id' , sendData);
}

// error correction apis
export const correctInputStockError = (date , invoiceNumber , transactionId , items) => {
    let sendData = {
        to_date : date,
        reference_id : invoiceNumber,
        vendor_increment_transaction_tbl_id : transactionId,
        items : items
    };

    return api.post('/api/v1/user/error-correction/transaction/for-vendor-increment-correction' , sendData);
}

export const correctRetunStockError = (date , returnInvoice , transactionId , items) => {
    let sendData = {
        to_date : date,
        reference_id : returnInvoice,
        vendor_increment_transaction_tbl_id : transactionId,
        items : items
    };

    return api.post('/api/v1/user/error-correction/transaction/for-vendor-return-correction' , sendData);
}

export const correctIssueStockError = (date , transactionId , items) => {
    let sendData = {
        to_date : date,
        daily_route_allocation_transaction_tbl_id : transactionId,
        items : items
    };

    return api.post('/api/v1/user/error-correction/transaction/for-route-allocation-correction' , sendData);
}

export const correctRouteReturnError = (date , transactionId , items) => {
    let sendData = {
        to_date : date,
        daily_route_return_transaction_tbl_id : transactionId,
        items : items
    };

    return api.post('/api/v1/user/error-correction/transaction/for-route-return-correction' , sendData);
}

// add revenue collection
export const addRevenueCollection = (date , route , issueNumber , revenueCollection , total) => {
    let sendData = {
        date : date,
        route_id : route,
        route_monitor_id : issueNumber,
        revenue_collection : revenueCollection,
        total : total
    };

    // console.log(sendData);
    return api.post('/api/v1/user/revenue-collection/management/add-or-update/per-transaction' , sendData);
}

export const searchRevenueCollection = (date , route , issueNumber) => {
    let sendData = {
        date : date,
        route_id : route,
        route_monitor_id : issueNumber,
    }

    return api.post('/api/v1/user/revenue-collection/management/search/per-transaction' , sendData);
}

export const getDetailsForDailySales = (startDate , endDate , route , currentPage , postsPerPage) => {
    let sendData = {
        from_date : startDate,
        to_date : endDate,
        route_id : route
    };
    let params = {
        currentPage : currentPage,
        postsPerPage : postsPerPage
    };
    // console.log(sendData);

    return api.post('/api/v1/user/report/profit-per-route-and-date' , sendData , {params : params});
}

// daily stock variation
export const getStockVariationDetails = (startDate , endDate , phase , currentPage , postsPerPage) => {
    let sendData = {
        from_date : startDate,
        to_date : endDate,
    };
    let params = {
        currentPage : currentPage,
        postsPerPage : postsPerPage
    };

    return api.post('/api/v1/user/snap-shot-controller/get' , sendData , {params : params});
}

export const getDashBoardData = (startDate , endDate) => {
    let sendData = {
        from_date : startDate,
        to_date : endDate,
    };

    return api.post('/api/v1/user/common-func/management/load-dash-board' , sendData);
}

export const statForRouteAllocation = (date , route , issueNumber) => {
    let sendData = {
        date : date,
        route_id : route,
        route_monitor_id : issueNumber
    };

    return api.post('/api/v1/user/report/stat-for-route-allocation' , sendData);
}



export const loadConfigurationForProductWiseSummary = () => {

    let sendData = null;

    return api.post('/api/v1/user/common-func/management/get-product-wise-config' , sendData);
}

export const productWiseDaily = (startDate , endDate , routeId , product ,sellingPrice , broughtPrice, currentPage , postsPerPage) => {

    let sendData = {
        from_date : startDate,
        to_date : endDate,
        route_id : routeId,
        product_id : product,
        bottle_selling_price : sellingPrice,
        bottle_bought_price : broughtPrice
    };

    let params = {
        currentPage : currentPage,
        postsPerPage : postsPerPage
    };

    return api.post('/api/v1/user/report/prodct-wise-summary-report/daily' , sendData , {params : params});
}

export const productWiseMonthly = (startDate , endDate , routeId , product , sellingPrice , broughtPrice , currentPage , postsPerPage) => {
    let sendData = {
        from_date : startDate,
        to_date : endDate,
        route_id : routeId,
        product_id : product,
        bottle_selling_price : sellingPrice,
        bottle_bought_price : broughtPrice
    };

    let params = {
        currentPage : currentPage,
        postsPerPage : postsPerPage
    };
    
    return api.post('/api/v1/user/report/prodct-wise-summary-report/monthly' , sendData , {params : params});
}

export const monthlyPurchaseSummary = (startDate , endDate , currentPage , postsPerPage) => {
    let sendData = {
        from_date : startDate,
        to_date : endDate,
    };

    let params = {
        currentPage : currentPage,
        postsPerPage : postsPerPage
    };

    return api.post('/api/v1/user/report/monthly-purchase-summary' , sendData , {params : params});
}

export const collectionSummary = (startDate , endDate , routeId, currentPage , postsPerPage) => {
    let sendData = {
        from_date : startDate,
        to_date : endDate,
        route_id : routeId
    };

    let params = {
        currentPage : currentPage,
        postsPerPage : postsPerPage
    };

    return api.post('/api/v1/user/report/collection-summary' , sendData , {params : params});
}

export const monthlyDiscountSummary = (startDate , endDate , currentPage , postsPerPage) => {
    let sendData = {
        from_date : startDate,
        to_date : endDate,
    };

    let params = {
        currentPage : currentPage,
        postsPerPage : postsPerPage
    };

    return api.post('/api/v1/user/report/monthly-discount-summary' , sendData , {params : params});
}

export const monthlySalesDifference = (startDate , endDate , routeId , currentPage , postsPerPage) => {
    let sendData = {
        from_date : startDate,
        to_date : endDate,
        route_id : routeId
    };

    let params = {
        currentPage : currentPage,
        postsPerPage : postsPerPage
    };

    return api.post('/api/v1/user/report/sales-difference/monthly' , sendData , {params : params});
}