import React, { useState } from "react";
import "./index.css";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { login } from "../../actions/auth";
import Typography from "@mui/material/Typography";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import PersonIcon from "@mui/icons-material/Person";
import LockIcon from "@mui/icons-material/Lock";
import Stack from "@mui/material/Stack";
import Divider from "@mui/material/Divider";
import Box from "@mui/material/Box";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import Textfield from "../FormsUI/Textfield";
import Checkbox from "../FormsUI/Checkbox";
import Button from "../FormsUI/Button";
import Loader from "../Loader";

const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const rememberMe = useSelector(
    (state) => state.rememberUser.rememberUserData
  );
  const [loading, setLoading] = useState(false);
  const [values, setValues] = useState({
    password: "",
    showPassword: false,
  });

  const INITIAL_FORM_STATE = {
    username: rememberMe?.usuario ? rememberMe?.usuario : "",
    password: rememberMe?.clave ? rememberMe?.clave : "",
    rememberMe: rememberMe?.recuerdame ? rememberMe?.recuerdame : false,
  };

  const FORM_VALIDATION = Yup.object().shape({
    username: Yup.string().required("Please Enter your username"),
    password: Yup.string()
      .required("Please Enter your password")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/,
        "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
      ),
    rememberMe: Yup.boolean().required("Required"),
  });

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const getTimeWelcome = () => {
    const hours = new Date().getHours();

    if (+hours < 12) {
      return "Good Morning!";
    } else if ((+hours > 12 && +hours < 16) || +hours === 12) {
      return "Good Afternoon!";
    } else {
      return "Good Evening!";
    }
  };

  return (
    <div className="container">
      <div className="login-card">
        <div className="row no-gutters">
          <div className="col-md-7">
            <img
              src={require("../../assets/images/login-background.jpg")}
              alt="login"
              className="login-card-img"
            />
          </div>
          <div className="col-md-5">
            <div className="card-body">
              <div className="brand-wrapper">
                <img
                  src={require("../../assets/images/digiPOS logo.jpeg")}
                  alt="logo"
                  className="logo"
                />
              </div>
              <Typography variant="subtitle1" color="text.secondary">
                {getTimeWelcome()}
              </Typography>
              <p className="login-card-description">Sign into your account</p>
              <Box sx={{ my: 3, mx: 2 }}>
                <Typography
                  variant="subtitle2"
                  color="text.secondary"
                  margin={"0 0px 15px 0px"}
                ></Typography>
                <Divider variant="middle" />

                <Formik
                  initialValues={{
                    ...INITIAL_FORM_STATE,
                  }}
                  validationSchema={FORM_VALIDATION}
                  onSubmit={(values) => {
                    setLoading(true);
                    login(values , setLoading , navigate , dispatch);
                  }}
                >
                  <Form>
                    <Stack direction="row" margin={"15px 0px 15px 0px"}>
                      <Textfield
                        type={"text"}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <PersonIcon />
                            </InputAdornment>
                          ),
                        }}
                        name="username"
                        label="Username"
                        placeholder="Username"
                      />
                    </Stack>
                    <Stack direction="row" margin={"15px 0px 0px 0px"}>
                      <Textfield
                        type={values.showPassword ? "text" : "password"}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <LockIcon />
                            </InputAdornment>
                          ),
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                edge="end"
                              >
                                {values.showPassword ? (
                                  <VisibilityOff />
                                ) : (
                                  <Visibility />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                        name="password"
                        label="Password"
                        placeholder="Password"
                      />
                    </Stack>
                    <Stack direction="row" textAlign={"end"}>
                      <Checkbox
                        size="small"
                        name="rememberMe"
                        label="Remember Me"
                        color="primary"
                      />
                    </Stack>
                    <Stack direction="column" margin={"15px 0px 5px 0px"}>
                      <Button fullWidth variant="contained" sx={{color: 'white', fontWeight: 'bold'}}>
                        Log In
                      </Button>
                    </Stack>
                    <Stack
                      direction="row"
                      margin={"5px 0px 15px 0px"}
                      display={"flex"}
                      justifyContent={"center"}
                    >
                    </Stack>
                  </Form>
                </Formik>
              </Box>
              <a href="#!" className="forgot-password-link">
                Forgot password?
              </a>
              <p className="login-card-footer-text">
                Don't have an account?{" "}
                <a href="#!" className="text-reset">
                  Register here
                </a>
              </p>
              <nav className="login-card-footer-nav">
                <a href="#!">Terms of use.</a>
                <a href="#!">Privacy policy</a>
              </nav>
            </div>
          </div>
        </div>
      </div>
      {loading && <Loader />}
    </div>
  );
};

export default Login;
