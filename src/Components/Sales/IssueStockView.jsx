import Grid from '@mui/material/Grid';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import dayjs from 'dayjs';
import TextField from '@mui/material/TextField';
import { useEffect, useState } from 'react';
import SelectCustom from '../CustomComponent/SelectCustom';
import * as React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { loadRouteConfiguration } from '../../Services/ConfigurationService';
import { issueStockView } from '../../Services/ViewService';
import { GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid';
import Loader from '../Loader';

  function CustomFooterStatusComponent(props) {
    return (
      <Box  style={{  display: 'flex' , marginTop : '10px' , marginBottom : '10px' }}>
        <Grid container spacing={3} >
                <Grid item xs={12} sm={5} >
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                  Cases Issued - {props.sumCases}
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Items Issued - {props.sumItems}
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Total Item Issued - {props.total}
                </Grid>
        </Grid>
      </Box>
    );
  }

  function CustomToolbar() {

    return (
      <GridToolbarContainer>
        <GridToolbarExport 
        csvOptions={{
            fileName: new Date(),
        }}
        />
      </GridToolbarContainer>
    );
  }


function IssueStockTable(){
 
    const [sumCases , setSumCases] = useState(0);
    const [sumItems , setSumItems] = useState(0);
    const [total , setTotal] = useState(0);

    const columns = [
      { field: 'item',
        headerName: 'Item',
        width: 160, 
        editable: false ,
      },
      // { field: 'currentStock', headerName: 'Current Stock' ,width: 160, editable: false },
      {
        field: 'sellingPrice',
        headerName: 'Selling Price',
        width: 160,
        editable: false,
      },
      {
        field: 'broughtPrice',
        headerName: 'Brought Price',
        width: 160,
        editable: false,
      },
      {
        field: 'caseType',
        headerName: 'Case Type',
        width: 160,
        editable: false,
      },
      {
        field: 'casesIssued',
        headerName: 'Issued Cases',
        width: 160,
        editable: false,
      },
      {
        field: 'itemsIssued',
        headerName: 'Item Issued',
        width: 160,
        editable: false,
      },
      {
        field: 'totalItemsIssued',
        headerName: 'Total Item Issued',
        width: 160,
        editable: false,
      }
    ];


    // table data

    const handleStateChange = (state) => {
      let sumCasesIssued = 0;
      let sumItemIssued = 0;
      let total = 0;
      state.rows.ids.map(item => {
        if(item === 'TOTAL'){}else {
          sumCasesIssued += Number(state.rows.idRowsLookup[item].casesIssued);
          sumItemIssued += Number(state.rows.idRowsLookup[item].itemsIssued);
          total += Number(state.rows.idRowsLookup[item].totalItemsIssued);
        }
      });
      setSumCases(sumCasesIssued);
      setTotal(total);
      setSumItems(sumItemIssued);
    }

    const [issueNumbers , setIssueNumbers] = useState([{id : 1 , value : 1} , {id : 2 , value : 2} , {id : 3 , value : 3} , {id : 4 , value : 4} , {id : 5 , value : 5}]);
    const [routes , setRoutes] = useState([]);
    useEffect(() => {
      loadRouteConfiguration(setRoutes);
    },[]);

    const [date , setDate] = useState(dayjs().format('YYYY-MM-DD'));
    const [route , setRoute] = useState('');
    const [issueNumber , setIssueNumber] = useState('');
    const [transactionId , setTransactionId] = useState(null);
    const [loading , setLoading] = useState(false);


    const handleDateChange = (newValue) => {
      setDate(dayjs(newValue).format('YYYY-MM-DD'));
  };

  const [rows , setRows] = useState([]);

  const handleStart = () => {
    setLoading(true);
    issueStockView(date , route , issueNumber , setLoading , setTransactionId , setRows);
  }

  const handleCancel = () => {
    setTransactionId(null);
    setIssueNumber('');
    setRoute('');
  }

    return (
        <div style={styles.mainDiv}>
            <Grid container spacing={3} >
                <Grid item xs={12} sm={3} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                    <DesktopDatePicker
                        inputFormat="MM/DD/YYYY"
                        value={date}
                        onChange={handleDateChange}
                        renderInput={(params) => <TextField {...params} />}
                    />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={3} >
                    <SelectCustom name={"routes"} items={routes} setValue={setRoute} initialValue={route}/>
                </Grid>
                <Grid item xs={12} sm={3} >
                    <SelectCustom name={"issue number"} items={issueNumbers} setValue={setIssueNumber} initialValue={issueNumber}/>
                </Grid>
                <Grid item xs={12} sm={3} >
                    {
                      transactionId !== null ? <Button variant="contained" style={{width : '100%' , height : '100%'}} onClick={() => handleCancel()}>Cancel</Button>
                      : 
                      <Button variant="contained" style={{width : '100%' , height : '100%'}} disabled={loading || route === '' || issueNumber === '' || date === 'Invalid Date'} onClick={() => handleStart()}>Search</Button>
                    }
                </Grid>
            </Grid>
            <hr color='black'/>
            {
              transactionId !== null ?
              <div>
                  <Box sx={{ height: 400, width: '100%' }}>
                    <DataGrid
                        rows={rows}
                        columns={columns}
                        experimentalFeatures={{ newEditingApi: true }}
                        onStateChange={(state) => {
                          handleStateChange(state);
                        }}
                        components={{
                          Footer: CustomFooterStatusComponent,
                          Toolbar: CustomToolbar,
                        }}
                        componentsProps={{
                          footer: { total , sumItems , sumCases },
                        }}
                    />
                  </Box>
              </div> : null
            }
            {loading && <Loader />}
        </div>
    )
}

export default IssueStockTable;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    buttonDiv : {
        marginTop : '10px'
    }
}