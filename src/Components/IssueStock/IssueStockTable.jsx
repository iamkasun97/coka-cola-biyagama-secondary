import Grid from '@mui/material/Grid';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import dayjs from 'dayjs';
import TextField from '@mui/material/TextField';
import { useEffect, useState } from 'react';
import SelectCustom from '../CustomComponent/SelectCustom';
import * as React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import { loadRouteConfiguration } from '../../Services/ConfigurationService';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { startIssueStockTransaction , publishIssueStockTransaction} from '../../Services/IssueStockService';
import Loader from '../Loader';
import Test from './test';

  const useFakeMutation = () => {
    return React.useCallback(
      (item) =>
        new Promise((resolve, reject) =>
          setTimeout(() => {
            if(item.itemsIssued === '' || isNaN(item.itemsIssued)){
              reject(new Error('Enter a Valid Value for items issued!!'));
            }
            if(item.casesIssued === '' || isNaN(item.casesIssued)){
              reject(new Error('Enter a Valid Value for cases issued!!'));
            }
            if(Number(item.caseType)*Number(item.casesIssued)+Number(item.itemsIssued) > Number(item.currentStock)){
              reject(new Error('Current stock exceed!!'));
            }
            resolve({...item, name: item.id});
          }, 200),
        ),
      [],
    );
  };

  function CustomFooterStatusComponent(props) {
    return (
      <Box  style={{  
            display: 'flex' , 
            marginTop : '10px' , 
            marginBottom : '10px' ,
            }}
            >
        <Grid container spacing={3} >
                <Grid item xs={12} sm={5} >
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                  Cases Issued - {props.sumCases}
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Items Issued - {props.sumItems}
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Total Item Issued - {props.total}
                </Grid>
        </Grid>
      </Box>
    );
  }


function IssueStockTable(){

  
    const [sumCases , setSumCases] = useState(0);
    const [sumItems , setSumItems] = useState(0);
    const [total , setTotal] = useState(0);
    const [error , setError] = useState(false);

    const columns = [
      { field: 'item',
        headerName: 'Item',
        width: 160, 
        editable: false ,
      },
      { field: 'currentStock', headerName: 'Current Stock' ,width: 140, editable: false, 
        renderCell : (params) => {
          return new Intl.NumberFormat().format(params.row.currentStock);
        }
      },
      {
        field: 'sellingPrice',
        headerName: 'Selling Price',
        width: 140,
        editable: false,
      },
      {
        field: 'broughtPrice',
        headerName: 'Brought Price',
        width: 140,
        editable: false,
      },
      {
        field: 'caseType',
        headerName: 'Case Type',
        width: 140,
        editable: false,
      },
      {
        field: 'casesIssued',
        headerName: 'Issued Cases',
        width: 140,
        editable: true,
        cellClassName: 'super-app-theme--cell',
        renderCell : (params) => {
          return new Intl.NumberFormat().format(params.row.casesIssued);
        }
      },
      {
        field: 'itemsIssued',
        headerName: 'Item Issued',
        width: 140,
        editable: true,
        cellClassName: 'super-app-theme--cell',
        renderCell : (params) => {
          return new Intl.NumberFormat().format(params.row.itemsIssued);
        }
      },
      {
        field: 'totalItemsIssued',
        headerName: 'Total Item Issued',
        width: 140,
        editable: false,
        renderCell : (params) => {
          if(params.row.caseType === undefined || params.row.itemsIssued === undefined || params.row.casesIssued === undefined){
            return 0;
          }
          return new Intl.NumberFormat().format(Number(params.row.caseType) * Number(params.row.casesIssued) + Number(params.row.itemsIssued));
          // return Number(params.row.caseType) * Number(params.row.casesIssued) + Number(params.row.itemsIssued)
        },
      }
    ];


    // table data
    const [editedRow , setEditedRow] = useState([]);   
    const mutateRow = useFakeMutation();

    const [snackbar, setSnackbar] = React.useState(null);

    const handleCloseSnackbar = () => setSnackbar(null);

    const processRowUpdate = React.useCallback(
        async (newRow) => {
        // Make the HTTP request to save in the backend
        const response = await  mutateRow(newRow);
        setEditedRow(prevState => [...prevState , {
          id : response.id,
          item : response.item,
          broughtPrice : response.broughtPrice,
          sellingPrice : response.sellingPrice,
          caseType : response.caseType,
          casesIssued : response.casesIssued,
          itemsIssued : response.itemsIssued,
          currentStock : response.currentStock,
          totalItemsIssued : Number(response.caseType)*Number(response.casesIssued) + Number(response.itemsIssued)
        }]);
        setSnackbar({ children: 'Data successfully Added to Submit', severity: 'success' });
        setError(false);
        return response;
        },
        [mutateRow],
    );

    const handleProcessRowUpdateError = React.useCallback((error) => {
        setSnackbar({ children: error.message, severity: 'error' });
        setError(true);
    }, []);

    
    const handleStateChange = (state) => {
      let sumCasesIssued = 0;
      let sumItemIssued = 0;
      let total = 0;
      state.rows.ids.map(item => {
        if(item === 'TOTAL'){}else {
          sumCasesIssued += Number(state.rows.idRowsLookup[item].casesIssued);
          sumItemIssued += Number(state.rows.idRowsLookup[item].itemsIssued);
          total += Number(state.rows.idRowsLookup[item].casesIssued)*Number(state.rows.idRowsLookup[item].caseType)+Number(state.rows.idRowsLookup[item].itemsIssued);
        }
      });
      setSumCases(new Intl.NumberFormat().format(sumCasesIssued));
      setTotal(new Intl.NumberFormat().format(total));
      setSumItems(new Intl.NumberFormat().format(sumItemIssued));
    }

    const [issueNumbers , setIssueNumbers] = useState([{id : 1 , value : 1} , {id : 2 , value : 2} , {id : 3 , value : 3} , {id : 4 , value : 4} , {id : 5 , value : 5}]);
    const [routes , setRoutes] = useState([]);
    useEffect(() => {
      loadRouteConfiguration(setRoutes);
    },[]);

    const [date , setDate] = useState(dayjs().format('YYYY-MM-DD'));
    const [route , setRoute] = useState('');
    const [issueNumber , setIssueNumber] = useState('');
    const [transactionId , setTransactionId] = useState(null);
    const [loading , setLoading] = useState(false);


    const handleDateChange = (newValue) => {
      setDate(dayjs(newValue).format('YYYY-MM-DD'));
    };

  // data
  const [rows , setRows] = useState([]);

  const handleStart = () => {
    setLoading(true);
    startIssueStockTransaction(date , route , issueNumber , setLoading , setTransactionId , setRows);
  }

  const handleCancel = () => {
    setTransactionId(null);
    setIssueNumber('');
    setRoute('');
    setEditedRow([]);
  }

  const handleSubmit = () => {
    let items = [];
    let ids = editedRow.map(item => item.id);
    let unique = ids.filter((v, i, a) => a.indexOf(v) === i);
    unique.forEach(i => {
      let temp = editedRow.filter(e => e.id === i);
      temp = temp.reverse();
      items.push({
        stock_summary_data_id : temp[0].id,
        total_bottle_quantity : temp[0].totalItemsIssued,
      });
    });
    rows.forEach(e => {
      if(unique.includes(e.id) !== true){
        items.push({
          stock_summary_data_id : e.id,
          total_bottle_quantity : 0
        });
      }
    })
    setLoading(true);
    publishIssueStockTransaction(transactionId , items , setTransactionId , setLoading , setRoute , setIssueNumber , setEditedRow);
  }

    return (
        <div style={styles.mainDiv}>
            <Grid container spacing={3} >
                <Grid item xs={12} sm={3} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                    <DesktopDatePicker
                        inputFormat="MM/DD/YYYY"
                        value={date}
                        onChange={handleDateChange}
                        renderInput={(params) => <TextField {...params} />}
                    />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={3} >
                    <SelectCustom name={"routes"} items={routes} setValue={setRoute} initialValue={route}/>
                </Grid>
                <Grid item xs={12} sm={3} >
                    <SelectCustom name={"issue number"} items={issueNumbers} setValue={setIssueNumber} initialValue={issueNumber}/>
                </Grid>
                <Grid item xs={12} sm={3} >
                    {
                      transactionId !== null ? <Button variant="contained" style={{width : '100%' , height : '100%'}} onClick={() => handleCancel()}>Cancel</Button>
                      : 
                      <Button variant="contained" style={{width : '100%' , height : '100%'}} disabled={loading || route === '' || issueNumber === '' || date === 'Invalid Date'} onClick={() => handleStart()}>Start</Button>
                    }
                </Grid>
            </Grid>
            <hr color='black'/>
            {
              transactionId !== null ? 
              <div>
                <Box sx={{ 
                    height: 400, 
                    width: '100%' ,
                      '& .super-app-theme--cell': {
                        backgroundColor: '#CCF1F1',
                        color: '#1a3e72',
                        fontWeight: '600',
                      }
                    }}>
                    <DataGrid
                        rows={rows}
                        columns={columns}
                        processRowUpdate={processRowUpdate}
                        onProcessRowUpdateError={handleProcessRowUpdateError}
                        experimentalFeatures={{ newEditingApi: true }}
                        onStateChange={(state) => {
                          handleStateChange(state);
                        }}
                        components={{
                          Footer: CustomFooterStatusComponent,
                        }}
                        componentsProps={{
                          footer: { total , sumItems , sumCases },
                        }}
                    />
                    {!!snackbar && (
                        <Snackbar
                        open
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                        onClose={handleCloseSnackbar}
                        autoHideDuration={6000}
                        >
                        <Alert {...snackbar} onClose={handleCloseSnackbar} />
                        </Snackbar>
                    )}
                </Box>
                  <Grid container spacing={3} >
                      <Grid item xs={12} sm={9} >
                      </Grid>
                      <Grid item xs={12} sm={3} >
                          <div style={styles.buttonDiv}>
                          <Button variant="contained" style={{width : '100%' , height : '55px'}} onClick={() => handleSubmit()} disabled={error || editedRow.length === 0}>Submit</Button>
                          </div>
                      </Grid>
                  </Grid>
              </div> : null
            }
            {loading && <Loader />}
        </div>
    )
}

export default IssueStockTable;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    buttonDiv : {
        marginTop : '10px'
    }
}