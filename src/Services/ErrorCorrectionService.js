import * as api from '../api/index';
import { openSuccessDialog } from '../Components/FormsUI/PopUps/SuccessDialog/index.tsx';

export const correctInputStockError = async (date , invoiceNumber , transactionId , items , setLoading , setTransactionId , setInvoiceNumber , setEditedRow) => {
    try{
        const data = await api.correctInputStockError(date , invoiceNumber , transactionId , items);
        // console.log(data);
        openSuccessDialog(data.data.status , data.data.comment);

        setInvoiceNumber('');
        setTransactionId(null);
        setLoading(false);
        setEditedRow([]);
    }catch(error){
        setLoading(false);
    }
}

export const correctRetunStockError = async (date , returnInvoice , transactionId , items , setLoading , setTransactionId , setReturnInvoice , setEditedRow) => {
    try{
        const data = await api.correctRetunStockError(date , returnInvoice , transactionId , items);
        // console.log(data);
        openSuccessDialog(data.data.status , data.data.comment);
        setEditedRow([]);
        setReturnInvoice('');
        setTransactionId(null);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const correctIssueStockError = async (date , transactionId , items , setLoading , setTransactionId , setRoute , setIssueNumber , setEditedRow) => {
    try{
        const data = await api.correctIssueStockError(date , transactionId , items);
        openSuccessDialog(data.data.status , data.data.comment);
        setEditedRow([]);
        setTransactionId(null);
        setRoute('');
        setIssueNumber('');
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const correctRouteReturnError = async (date , transactionId , items , setLoading , setTransactionId , setRoute , setIssueNumber , setEditedRow) => {
    try{
        const data = await api.correctRouteReturnError(date , transactionId , items);
        openSuccessDialog(data.data.status , data.data.comment);
        setEditedRow([]);
        setTransactionId(null);
        setRoute('');
        setIssueNumber('');
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}