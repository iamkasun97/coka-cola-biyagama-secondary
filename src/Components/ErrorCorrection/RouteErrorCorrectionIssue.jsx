import * as React from 'react';
import { useEffect , useState } from 'react';
import Grid from '@mui/material/Grid';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import dayjs from 'dayjs';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import Box from '@mui/material/Box';
import { DataGrid , GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';
import { date } from 'yup';
import SelectCustom from '../CustomComponent/SelectCustom';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { loadRouteConfiguration } from '../../Services/ConfigurationService';
import { issueStockView } from '../../Services/ViewService';
import Loader from '../Loader';
import { correctIssueStockError } from '../../Services/ErrorCorrectionService';

const useFakeMutation = () => {
    return React.useCallback(
      (item) =>
        new Promise((resolve, reject) =>
          setTimeout(() => {
            if(item.itemsIssued === '' || isNaN(item.itemsIssued)){
              reject(new Error('Enter a Valid Value for items issued!!'));
            }
            if(item.casesIssued === '' || isNaN(item.casesIssued)){
              reject(new Error('Enter a Valid Value for cases issued!!'));
            }
            resolve({...item, name: item.id});
          }, 200),
        ),
      [],
    );
  };

  function CustomFooterStatusComponent(props) {
    return (
      <Box  style={{  display: 'flex' , marginTop : '10px' , marginBottom : '10px' }}>
        <Grid container spacing={3} >
                <Grid item xs={12} sm={5} >
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                  Cases Issued - {props.sumCases}
                </Grid>
                <Grid item xs={12} sm={2} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Items Issued - {props.sumItems}
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Total Item Issued - {props.total}
                </Grid>
        </Grid>
      </Box>
    );
  }

function RouteErrorCorrection(){

  const [sumCases , setSumCases] = useState(0);
    const [sumItems , setSumItems] = useState(0);
    const [total , setTotal] = useState(0);

    // table data
  const [editedRow , setEditedRow] = useState([]);   
  const mutateRow = useFakeMutation();

  const [snackbar, setSnackbar] = React.useState(null);

  const handleCloseSnackbar = () => setSnackbar(null);

  const processRowUpdate = React.useCallback(
      async (newRow) => {
      // Make the HTTP request to save in the backend
      const response = await mutateRow(newRow);
      setEditedRow(prevState => [...prevState , {
        id : response.id,
        totalItemsIssued : Number(response.caseType)*Number(response.casesIssued) + Number(response.itemsIssued)
      }]);
      setSnackbar({ children: 'Data successfully Added to Submit', severity: 'success' });
      return response;
      },
      [mutateRow],
  );

  const handleProcessRowUpdateError = React.useCallback((error) => {
      setSnackbar({ children: error.message, severity: 'error' });
  }, []);

  const handleStateChange = (state) => {
    let sumCasesIssued = 0;
    let sumItemIssued = 0;
    let total = 0;
    state.rows.ids.map(item => {
      if(item === 'TOTAL'){}else {
        sumCasesIssued += Number(state.rows.idRowsLookup[item].casesIssued);
        sumItemIssued += Number(state.rows.idRowsLookup[item].itemsIssued);
        total += Number(state.rows.idRowsLookup[item].casesIssued)*Number(state.rows.idRowsLookup[item].caseType)+Number(state.rows.idRowsLookup[item].itemsIssued);
      }
    });
    setSumCases(sumCasesIssued);
    setTotal(total);
    setSumItems(sumItemIssued);
  }

  const [issueNumbers , setIssueNumbers] = useState([{id : 1 , value : 1} , {id : 2 , value : 2} , {id : 3 , value : 3} , {id : 4 , value : 4} , {id : 5 , value : 5}]);
    const [routes , setRoutes] = useState([]);
    useEffect(() => {
      loadRouteConfiguration(setRoutes);
    },[]);

    const [date , setDate] = useState(dayjs().format('YYYY-MM-DD'));
    const [route , setRoute] = useState('');
    const [issueNumber , setIssueNumber] = useState('');
    const [transactionId , setTransactionId] = useState(null);
    const [loading , setLoading] = useState(false);


    const handleDateChange = (newValue) => {
      setDate(dayjs(newValue).format('YYYY-MM-DD'));
  };

  const [rows , setRows] = useState([]);

  const handleStart = () => {
    setLoading(true);
    issueStockView(date , route , issueNumber , setLoading , setTransactionId , setRows);
  }

  const handleCancel = () => {
    setTransactionId(null);
    setIssueNumber('');
    setRoute('');
    setEditedRow([]);
  }

  const handleSubmit = () => {
    let items = [];
    let ids = editedRow.map(item => item.id);
    let unique = ids.filter((v, i, a) => a.indexOf(v) === i);
    unique.forEach(i => {
      let temp = editedRow.filter(e => e.id === i);
      temp = temp.reverse();
      items.push({
        daily_route_allocation_stock_change_id : temp[0].id,
        total_bottle_quantity : temp[0].totalItemsIssued,
      });
    });

    setLoading(true);
    correctIssueStockError(date , transactionId , items , setLoading , setTransactionId , setRoute , setIssueNumber , setEditedRow);
  }
    return (
        <Box style={styles.mainDiv}>
             <Grid container spacing={3} >
                <Grid item xs={12} sm={3} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                        <DesktopDatePicker
                            inputFormat="MM/DD/YYYY"
                            value={date}
                            onChange={handleDateChange}
                            renderInput={(params) => <TextField {...params} />}
                        />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={3} >
                    <SelectCustom name='routes' items={routes} setValue={setRoute} initialValue={route}/>
                </Grid>
                <Grid item xs={12} sm={3} >
                    <SelectCustom name='issue number' items={issueNumbers} setValue={setIssueNumber} initialValue={issueNumber}/>
                </Grid>
                <Grid item xs={12} sm={3} >
                  {
                      transactionId !== null ? <Button variant="contained" style={{width : '100%' , height : '100%'}} onClick={() => handleCancel()}>Cancel</Button>
                      : 
                      <Button variant="contained" style={{width : '100%' , height : '100%'}} disabled={loading || route === '' || issueNumber === '' || date === 'Invalid Date'} onClick={() => handleStart()}>Search</Button>
                    }
                </Grid>
            </Grid>
            {
              transactionId !== null ? 
              <div >
                <Box sx={{ 
                      height: 400, 
                      width: '100%' ,
                      '& .super-app-theme--cell': {
                        backgroundColor: '#CCF1F1',
                        color: '#1a3e72',
                        fontWeight: '600',
                      }
                      }} 
                      style={styles.tableBox}>
                    <DataGrid
                        rows={rows}
                        columns={columns}
                        processRowUpdate={processRowUpdate}
                        onProcessRowUpdateError={handleProcessRowUpdateError}
                        experimentalFeatures={{ newEditingApi: true }}
                        onStateChange={(state) => {
                          handleStateChange(state);
                        }}
                        components={{
                          Footer: CustomFooterStatusComponent,
                        }}
                        componentsProps={{
                          footer: { total , sumItems , sumCases },
                        }}
                    />
                    {!!snackbar && (
                        <Snackbar
                        open
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                        onClose={handleCloseSnackbar}
                        autoHideDuration={6000}
                        >
                        <Alert {...snackbar} onClose={handleCloseSnackbar} />
                        </Snackbar>
                    )}
                </Box>
                
                <Grid container spacing={3} >
                  <Grid item xs={12} sm={9} >
                  </Grid>
                  <Grid item xs={12} sm={3} >
                      <div style={styles.buttonDiv}>
                      <Button variant="contained" style={{width : '100%' , height : '55px'}} disabled={editedRow.length === 0} onClick={() => handleSubmit()}>Submit</Button>
                      </div>
                  </Grid>
                </Grid>
            </div> : null
            } 
            {loading && <Loader />}
        </Box>
    )
}

export default RouteErrorCorrection;

const styles = {
    mainDiv : {
        margin : '20px',
        marginRight : '50px'
    },
    buttonDiv : {
        marginTop : '10px'
    },
    scroll : {
        overflowX : 'scroll'
    },
    tableBox : {
        marginTop : '10px'
    }
}

const columns = [
  { field: 'item',
    headerName: 'Item',
    width: 160, 
    editable: false ,
  },
  {
    field: 'sellingPrice',
    headerName: 'Selling Price',
    width: 160,
    editable: false,
  },
  {
    field: 'broughtPrice',
    headerName: 'Brought Price',
    width: 160,
    editable: false,
  },
  {
    field: 'caseType',
    headerName: 'Case Type',
    width: 160,
    editable: false,
  },
  {
    field: 'casesIssued',
    headerName: 'Issued Cases',
    width: 160,
    editable: true,
    cellClassName: 'super-app-theme--cell',
  },
  {
    field: 'itemsIssued',
    headerName: 'Item Issued',
    width: 160,
    editable: true,
    cellClassName: 'super-app-theme--cell',
  },
  {
    field: 'totalItemsIssued',
    headerName: 'Total Item Issued',
    width: 160,
    editable: false,
    renderCell : (params) => {
      if(params.row.caseType === undefined || params.row.itemsIssued === undefined || params.row.casesIssued === undefined){
        return 0;
      }
      return Number(params.row.caseType) * Number(params.row.casesIssued) + Number(params.row.itemsIssued);
    },
  }
];