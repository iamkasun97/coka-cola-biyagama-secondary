import Grid from '@mui/material/Grid';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';
import TextField from '@mui/material/TextField';
import * as React from 'react';
import Button from '@mui/material/Button';
import SelectCustom from '../CustomComponent/SelectCustom';
import { DataGrid } from '@mui/x-data-grid';
import Box from '@mui/material/Box';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { loadRouteConfiguration } from '../../Services/ConfigurationService';
import { routeReturnStockView } from '../../Services/ViewService';
import { GridToolbarContainer, GridToolbarExport } from '@mui/x-data-grid';
import Loader from '../Loader';

  function CustomFooterStatusComponent(props) {
    return (
      <Box  style={{    marginTop : '10px' , marginBottom : '10px' }}>
        {/* <Grid container spacing={3} >
                <Grid item xs={12} sm={3} >
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                  Cases Issued - {props.sumCases}
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Items Issued - {props.sumItems}
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Total Item Issued - {props.total}
                </Grid>
        </Grid> */}
        <Grid container spacing={3} >
                <Grid item xs={12} sm={3} >
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                   Returned Issued Cases - {props.sumReturnedCases}
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Returned Issued Items - {props.sumReturnedItems}
                </Grid>
                <Grid item xs={12} sm={3} >
                <FiberManualRecordIcon
                  fontSize="small"
                  sx={{
                    mr: 1,
                  }}
                />
                Total Returned Items - {props.totalReturns}
                </Grid>
        </Grid>
      </Box>
    );
  }

  function CustomToolbar() {

    return (
      <GridToolbarContainer>
        <GridToolbarExport 
        csvOptions={{
            fileName: new Date(),
        }}
        />
      </GridToolbarContainer>
    );
  }

function RouteReturn(){

    
    const [sumCases , setSumCases] = useState(0);
    const [sumItems , setSumItems] = useState(0);
    const [total , setTotal] = useState(0);
    const [sumReturnedCases , setSumReturnedCases] = useState(0);
    const [sumReturnedItems , setSumReturnedItems] = useState(0);
    const [totalReturns , setTotalReturns] = useState(0);

    const [issueNumbers , setIssueNumbers] = useState([{id : 1 , value : 1} , {id : 2 , value : 2} , {id : 3 , value : 3} , {id : 4 , value : 4} , {id : 5 , value : 5}]);
    const [routes , setRoutes] = useState([]);
    useEffect(() => {
      loadRouteConfiguration(setRoutes);
    },[]);


    const [date , setDate] = useState(dayjs().format('YYYY-MM-DD'));
    const [route , setRoute] = useState('');
    const [issueNumber , setIssueNumber] = useState('');
    const [transactionId , setTransactionId] = useState(null);
    const [loading , setLoading] = useState(false);


    const handleDateChange = (newValue) => {
        setDate(dayjs(newValue).format('YYYY-MM-DD'));
    };

  const [rows , setRows] = useState([]);

  const handleStart = () => {
    setLoading(true);
    routeReturnStockView(date , route , issueNumber , setLoading , setTransactionId , setRows);
  }

  const handleCancel = () => {
    setTransactionId(null);
    setIssueNumber('');
    setRoute('');
  }

    return (
        <div style={styles.mainDiv}>
            <Grid container spacing={3} >
                <Grid item xs={12} sm={3} >
                    <LocalizationProvider dateAdapter={AdapterDayjs} >
                    <DesktopDatePicker
                        inputFormat="MM/DD/YYYY"
                        value={date}
                        onChange={handleDateChange}
                        renderInput={(params) => <TextField {...params} />}
                    />
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12} sm={3} >
                    <SelectCustom name={"routes"} items={routes} setValue={setRoute} initialValue={route}/>
                </Grid>
                <Grid item xs={12} sm={3} >
                    <SelectCustom name={"issue number"} items={issueNumbers} setValue={setIssueNumber} initialValue={issueNumber}/>
                </Grid>
                <Grid item xs={12} sm={3} >
                    {
                      transactionId !== null ? <Button variant="contained" style={{width : '100%' , height : '100%'}} onClick={() => handleCancel()}>Cancel</Button>
                      : 
                      <Button variant="contained" style={{width : '100%' , height : '100%'}} disabled={loading || route === '' || issueNumber === '' || date === 'Invalid Date'} onClick={() => handleStart()}>Search</Button>
                    }
                </Grid>
            </Grid>
            <hr color='black'/>
            {
              transactionId !== null ? 
              <Box sx={{ height: 400, width: '100%' }}>
                <DataGrid
                    rows={rows}
                    columns={columns}
                    experimentalFeatures={{ newEditingApi: true }}
                    onStateChange={(state) => {
                      let sumCasesIssued = 0;
                      let sumItemIssued = 0;
                      let total = 0;
                      let sumReturnedCasesIssued = 0;
                      let sumReturnedItemsIssued = 0;
                      let totalRetuns = 0;
                      state.rows.ids.map(item => {
                        sumCasesIssued += Number(state.rows.idRowsLookup[item].casesIssued);
                        sumItemIssued += Number(state.rows.idRowsLookup[item].itemIssued);
                        total += Number(state.rows.idRowsLookup[item].casesIssued)*Number(state.rows.idRowsLookup[item].caseType)+Number(state.rows.idRowsLookup[item].itemIssued);
                        sumReturnedCasesIssued += Number(state.rows.idRowsLookup[item].returnedCases);
                        sumReturnedItemsIssued += Number(state.rows.idRowsLookup[item].returnedItems);
                        totalRetuns += Number(state.rows.idRowsLookup[item].totalReturns);
                      });
                      // setSumCases(new Intl.NumberFormat().format(parseFloat(sumCasesReturn).toFixed(2)));
                      setSumReturnedCases(new Intl.NumberFormat().format(parseFloat(sumReturnedCasesIssued).toFixed(2)));
                      setSumReturnedItems(new Intl.NumberFormat().format(parseFloat(sumReturnedItemsIssued).toFixed(2)));
                      setTotalReturns(new Intl.NumberFormat().format(parseFloat(totalRetuns).toFixed(2)));
                    }}
                    components={{
                      Footer: CustomFooterStatusComponent,
                      Toolbar : CustomToolbar
                    }}
                    componentsProps={{
                      footer: { total , sumItems , sumCases , sumReturnedCases , sumReturnedItems , totalReturns},
                    }}
                />
            </Box> : null
            }
            {loading && <Loader />}
        </div>
    )
}

export default RouteReturn;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    buttonDiv : {
        marginTop : '10px'
    }
}

const columns = [
    { field: 'item', headerName: 'Item', width: 180, editable: false },
    // { field: 'currentStock', headerName: 'Current Stock' ,width: 180, editable: false },
    {
      field: 'caseType',
      headerName: 'Case Type',
      width: 180,
      editable: false,
    },
    // {
    //   field: 'casesIssued',
    //   headerName: 'Cases Issued',
    //   width: 180,
    //   editable: false,
    // },
    // {
    //   field: 'itemIssued',
    //   headerName: 'Item Issued',
    //   width: 180,
    //   editable: false,
    // },
    // {
    //   field: 'totalItemsIssued',
    //   headerName: 'Total Items Issued',
    //   width: 180,
    //   editable: false,
    // },
    {
      field: 'returnedCases',
      headerName: 'Returned Cases',
      width: 180,
      editable: false,
    },
    {
        field: 'returnedItems',
        headerName: 'Returned Items',
        width: 180,
        editable: false,
      },
    {
        field: 'totalReturns',
        headerName: 'Total Returns',
        width: 180,
        editable: false,
      },
  ];
