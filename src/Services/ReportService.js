import * as api from '../api/index';

export const getDetailsForDailySales = async (startDate , endDate , route ,currentPage , postsPerPage, setLoading , setRows , setCount) => {
    try{
        const data = await api.getDetailsForDailySales(startDate , endDate , route , currentPage , postsPerPage);
        setCount(data.data.data.count);
        let rows = [];
        data.data.data.data_for_the_loop?.map(item => {
            let temp = {
                id : item.id,
                date : item.date,
                dailySales : item.total_sales,
                dailyProfits : item.total_profit
            };

            rows.push(temp);
        })
        setRows(rows);

        setLoading(false);
    }catch(error){
        setLoading(false);
        // console.log(error);
        setRows([]);
    }
}

export const getStockVariationDetails = async (startDate , endDate , phase , currentPage , postsPerPage , setLoading , setRows , setCount) => {
    try{
        const data = await api.getStockVariationDetails(startDate , endDate , phase , currentPage , postsPerPage);
        setCount(data.data.data.count);
        let rows = [];
        data.data.data.data_for_the_loop?.map(item => {
            let temp = {
                id : item[8],
                item : item[1],
                stock : item[2]
            };

            rows.push(temp);
        });
        setRows(rows)

        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const productWiseDaily = async (startDate , endDate , routeId , productId , sellingPrice , broughtPrice ,  currentPage , postsPerPage , setLoading , routes , setRows , setCount , product) => {
    try{
        const data = await api.productWiseDaily(startDate , endDate , routeId , productId ,sellingPrice , broughtPrice, currentPage , postsPerPage);
        console.log(data.data.data.data_for_the_loop);
        let rows = [];
        
        data.data.data.data_for_the_loop?.map((e , index) => {
            let routeName = routes.find(r => r.id == e.route_id);
            let temp = {
                id : index,
                date : e.created_time,
                route : routeName.route_name,
                product : product,
                saleQty : e.rem_transaction_quantity,
                salePrice : Number(e.rem_transaction_quantity)*Number(e.bottle_selling_price)
            };

            rows.push(temp);
        })

        setRows(rows);
        setCount(data.data.data?.count);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const productWiseMonthly = async (startDate , endDate , routeId , productId , sellingPrice , broughtPrice , currentPage , postsPerPage , setLoading , routes , setRows , setCount , product) => {
    try{
        const data = await api.productWiseMonthly(startDate , endDate , routeId , productId ,sellingPrice , broughtPrice, currentPage , postsPerPage);
        console.log(data.data.data.data_for_the_loop);

        let rows = [];
        data.data.data.data_for_the_loop?.map((e , index) => {
            let routeName = routes.find(r => r.id == e.route_id);
            let temp = {
                id : index,
                date : e.created_time,
                route : routeName.route_name,
                product : product,
                saleQty : e.rem_transaction_quantity,
                salePrice : Number(e.rem_transaction_quantity)*Number(e.bottle_selling_price)
            };

            rows.push(temp);
        });

        setRows(rows);
        setCount(data.data.data?.count);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const monthlyPurchaseSummary = async (startDate , endDate , currentPage , postsPerPage , setLoading , setRows , setCount) => {
    try{
        const data = await api.monthlyPurchaseSummary(startDate , endDate , currentPage , postsPerPage);
        console.log(data.data.data.data_for_the_loop);
        let rows = [];
        data.data.data.data_for_the_loop?.map((e , index) => {
            let temp = {
                id : index,
                month : e.created_time,
                purchasedCost : e.purchased_cost,
                totalItemsPurchased : e.bought_qunatity,
                returnItemsCost : e.returned_cost,
                totalItemsReturned : e.returned_qunatity
            };
            rows.push(temp);
        })

        setRows(rows);
        setCount(data.data.data?.count);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const collectionSummary = async (startDate , endDate , routeId , currentPage , postsPerPage , setLoading , setRows , setCount , routes) => {
    try{
        const data = await api.collectionSummary(startDate , endDate , routeId , currentPage , postsPerPage);
        console.log(data.data.data.data_for_the_loop);
        let rows = [];
        data.data.data.data_for_the_loop?.map((e , index) => {
            let routeName = routes.find(r => r.id == e.route_id);
            let temp = {
                id : index,
                date : e.transaction_tbl_date,
                route : routeName.route_name,
                cash : e.cash,
                cheque : e.cheque,
                credit : e.credit,
                creditCollection : e.credit_collection,
                bank : e.bank,
                expenses : e.expenses,
                discount : e.discount
            };

            rows.push(temp);
        })

        setRows(rows);
        setCount(data.data.data?.count);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const monthlyDiscountSummary = async (startDate , endDate , currentPage , postsPerPage , setLoading , setRows , setCount) => {
    try{
        const data = await api.monthlyDiscountSummary(startDate , endDate , currentPage , postsPerPage);
        console.log(data.data.data.data_for_the_loop);
        let rows = [];
        data.data.data.data_for_the_loop?.map((e , index) => {
            let temp = {
                id : index,
                month : e.month,
                discount : e.discount
            };

            rows.push(temp);
        })

        setRows(rows);
        setCount(data.data.data?.count);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const monthlySalesDifference = async (startDate , endDate , routeId , currentPage , postsPerPage , setLoading , setRows , setCount , routes) => {
    try{
        const data = await api.monthlySalesDifference(startDate , endDate , routeId , currentPage , postsPerPage);
        console.log(data.data.data.data_for_the_loop);
        let rows = [];
        data.data.data.data_for_the_loop?.map((e , index) => {
            let routeName = routes.find(r => r.id == e.route_id);
            let difference = 0;
            if(e.total_collection_for_all_modes != ' null'){
                difference = e.total_collection_for_all_modes;
            };
            let temp = {
                id : index,
                month : e.date,
                route : routeName.route_name,
                salesDifference : difference
            };

            setRows(rows);
            setCount(data.data.data?.count);
            rows.push(temp);
        })


        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}