import * as React from 'react';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Autocomplete from '@mui/material/Autocomplete';


export default function FreeSolo(props) {

  const [value, setValue] = React.useState('');

  return (
    <Stack >
      <Autocomplete
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
          let productId = props?.items?.find((product) => newValue == product.name);
          props.setProduct({
            id : productId?.id,
            sellingPrice : productId?.sellingPrice,
            broughtPrice : productId?.broughtPrice,
            name : productId?.name
          });
        }}
        freeSolo
        id="free-solo-2-demo"
        options={props?.items?.map((option) => option.name)}
        renderInput={(params) => <TextField {...params} label="search product" />}
      />
    </Stack>
  );
}
