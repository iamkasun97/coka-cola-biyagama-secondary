import * as api from '../api/index';

export const getProducts = async (currentPage , postsPerPage , setProducts , setCount) => {
    try{
        const data = await api.getProducts(currentPage , postsPerPage);
        // console.log(data);
        setProducts(data.data.data.data_for_the_loop);
        setCount(data.data.data.count);
    }catch(error){

    }
}

export const addProduct = async (productName , caseType , currentPage , postsPerPage , setProducts , setCount , setLoading) => {
    try{
        const data = await api.addProduct(productName , caseType);
        const dataNext = await api.getProducts(currentPage , postsPerPage);
        setProducts(dataNext.data.data.data_for_the_loop);
        setCount(dataNext.data.data.count);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const editProduct = async (id , productName , caseType , currentPage , postsPerPage , setProducts , setCount , setLoading) => {
    try{
        const data = await api.editProduct(id , productName , caseType);
        const dataNext = await api.getProducts(currentPage , postsPerPage);
        setProducts(dataNext.data.data.data_for_the_loop);
        setCount(dataNext.data.data.count);
        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}