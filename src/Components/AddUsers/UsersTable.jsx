import { useEffect, useMemo, useState } from 'react';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import { Visibility } from '@mui/icons-material';
import FormControl from '@mui/material/FormControl';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputAdornment from '@mui/material/InputAdornment';
import InputLabel from '@mui/material/InputLabel';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import Button from '@mui/material/Button';
import { DataGrid , useGridApiRef  } from '@mui/x-data-grid';
import Box from '@mui/material/Box';
import Test from './test';
import EditChangesPopUp from './EditChangesPopUp';
import {getAllUsers , registerSubUser} from '../../Services/UserManagementService';
import Loader from '../Loader';
import * as Yup from "yup";
import { Form, Formik } from "formik";
import Textfield from '../FormsUI/Textfield/index';
import ChangePassword from './Changepassword';

function UsersTable(){
    const [loading, setLoading] = useState(false);
    const [currentPage , setCurrentPage] = useState(0);
    const [postsPerPage , setPostsPerPage] = useState(5);
    const [count , setCount] = useState(0);
    const [showPassword , setShowPassword] = useState(false);
    const [showPassword1 , setShowPassword1] = useState(false);

    const [users , setUsers] = useState([]);
    useEffect(() => {
        getAllUsers(currentPage , postsPerPage , setUsers , setCount);
    }, [currentPage , postsPerPage]);
    const columns = useMemo(
        () => [
            { field: 'username', headerName: 'User Name', width: 150 , editable : 'true'},
                  { field: 'email', headerName: 'Email', width: 200 , editable : 'true'},
                  { field: 'fname', headerName: 'First Name', width: 100 , editable : 'true' },
                  { field: 'mobile', headerName: 'mobile', width: 120  , editable : 'true'},
                  {
                    field: 'save',
                    headerName: '',
                    action : 'submit',
                    width: 100,
                    renderCell: (params) => {
                        return <EditChangesPopUp data={params} setUsers={setUsers} setCount={setCount} currentPage={currentPage} postsPerPage={postsPerPage}/>
                    }
                    ,
                  },
                  {
                    field: 'changePassword',
                    headerName: '',
                    width: 170,
                    renderCell: (params) => <ChangePassword  data={params} setUsers={setUsers} setCount={setCount} currentPage={currentPage} postsPerPage={postsPerPage}/>
                    ,
                  },
        ],[currentPage , postsPerPage]
    );

    // form validation
    const INITIAL_FORM_STATE = {
        userName:  "",
        password:  "",
        cPassword : "",
        fName : "",
        email : "",
        mobile : "",
    };

    const FORM_VALIDATION = Yup.object().shape({
        userName: Yup.string().required("Please Enter your username"),
        password: Yup.string()
          .required("Please Enter your password")
          .matches(
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/,
            "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
          ),
        cPassword : Yup.string()
        .oneOf([Yup.ref("password")], "Passwords do not match")
        .required("Please Enter your Confirm password"),
        // .matches(password),
        mobile : Yup.string()
        .matches(
            /^(?:0|94|\+94)?(?:(11|21|23|24|25|26|27|31|32|33|34|35|36|37|38|41|45|47|51|52|54|55|57|63|65|66|67|81|912)(0|2|3|4|5|7|9)|7(0|1|2|4|5|6|7|8)\d)\d{6}$/,
            "Invalid mobile format"
        )
        .required('Please Enter Your Mobile Number'),
        fName : Yup.string()
        .required('Please Enter Your first name'),
        email : Yup.string().matches(
            /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i,
            "Enter a valid email address"
        )
        .required('Please Enter Your Email')
      });




    return (
        <div style={styles.mainDiv}>
            <Card sx={{ maxWidth: 750 }} style={styles.card}>
                    <div style={styles.insideDiv}>
                    <Grid container spacing={3} >
                        <Grid item xs={12} sm={6} >
                            <Typography variant="h5" fontWeight={'bold'} gutterBottom>
                                Add New User
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                            <Typography variant="h8" fontWeight={'bold'} gutterBottom>
                                User Details
                            </Typography>
                        </Grid>
                    </Grid>
                    <Formik
                    initialValues={{
                        ...INITIAL_FORM_STATE,
                    }}
                    validationSchema={FORM_VALIDATION}
                    onSubmit={(values , {resetForm}) => {
                        setLoading(true);
                        // console.log(values);
                        registerSubUser(values.userName , values.fName , values.email ,values.mobile, values.password , values.cPassword , currentPage , postsPerPage , setUsers , setCount , setLoading)
                        resetForm()
                    }}
                    >
                    <Form >
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                        <Textfield
                            type={'text'}
                            name="userName"
                            label="Username"
                            placeholder="Username"
                            autoComplete="chrome-off"
                        />
                        </Grid>
                        <Grid item xs={12} sm={6} >
                        <Textfield
                            type={"text"}
                            name="email"
                            label="email"
                            placeholder="email"
                        />
                        </Grid>
                    </Grid>
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                        <Textfield
                            type={"text"}
                            name="fName"
                            label="First Name"
                            placeholder="First Name"
                        />
                        </Grid>
                        <Grid item xs={12} sm={6} >
                        <Textfield
                            type={"text"}
                            name="mobile"
                            label="mobile"
                            placeholder="mobile"
                        />
                        </Grid>
                    </Grid>
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                        <Textfield
                            type={showPassword ? "text" : "text"}
                            InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={() => setShowPassword(!showPassword)}
                                    edge="end"
                                >
                                    {!showPassword ? (
                                    <VisibilityOff />
                                    ) : (
                                    <Visibility />
                                    )}
                                </IconButton>
                                </InputAdornment>
                            ),
                            }}
                            name="password"
                            label="Password"
                            placeholder="Password"
                            autoComplete='off'
                        />
                        </Grid>
                        <Grid item xs={12} sm={6} >
                        <Textfield
                            type={showPassword1 ? "text" : "text"}
                            InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={() => setShowPassword1(!showPassword1)}
                                    edge="end"
                                >
                                    {!showPassword1 ? (
                                    <VisibilityOff />
                                    ) : (
                                    <Visibility />
                                    )}
                                </IconButton>
                                </InputAdornment>
                            ),
                            }}
                            name="cPassword"
                            label="Confirm Password"
                            placeholder="Confirm Password"
                        />
                        </Grid>
                    </Grid>
                    <Grid container spacing={3} marginY={0}>
                        <Grid item xs={12} sm={6} >
                            <Button variant="contained" style={{width : '100%' , height : '55px'}} type='submit'>Submit</Button>
                        </Grid>
                        <Grid item xs={12} sm={6} >
                            <Button variant="contained" style={{width : '100%' , height : '55px'}} type='reset'>Cancel</Button>
                        </Grid>
                    </Grid> 
                    </Form>
                    </Formik>
                    </div>
                    <Box sx={{ height: 400, width: '100%' }}>
                        <DataGrid
                            pagination
                            paginationMode="server"
                            rowCount={count}
                            rows={users}
                            columns={columns}
                            pageSize={postsPerPage}
                            page={currentPage}
                            onPageSizeChange={(newValue) => setPostsPerPage(newValue)}
                            onPageChange={(newValue) => setCurrentPage(newValue)}
                            rowsPerPageOptions={[5 , 10 , 25]}
                            experimentalFeatures={{ newEditingApi: true }}
                        />
                    </Box>
            </Card>
            {loading && <Loader />}
        </div>
    )
}

export default UsersTable;

const styles = {
    mainDiv : {
        margin : '20px'
    },
    card: {
        margin: 'auto'
    },
    insideDiv : {
        padding : '40px'
    }
}

