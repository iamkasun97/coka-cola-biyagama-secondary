import * as api from '../api/index';

export const viewStock = async (setRows) => {
    try{
        const data = await api.getStockSummary();
        setRows(data.data.data);
    }catch(error){}
}