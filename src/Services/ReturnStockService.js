import * as api from '../api/index';
import { openSuccessDialog } from '../Components/FormsUI/PopUps/SuccessDialog/index.tsx';

export const startReturnStockTransaction = async (date , returnInvoiceNumber , setLoading , setTransactionId , setRows) => {
    try{
        const data = await api.startReturnStockTransaction(date , returnInvoiceNumber);
        setTransactionId(data.data.data.id);
        const dataNext = await api.getStockSummary();
        let rows = [];
        dataNext.data.data.map((item) => {
            let temp = {
                id : item.id,
                item : item.item_name,
                totalStock : item.total_quantity,
                caseType : item.item_case_number_of_bottles,
                cases : 0,
                items : 0,
                broughtPrice : item.bottle_brought_price,
                sellingPrice : item.bottle_selling_price
            }
            rows.push(temp);
        });
        setRows(rows);

        setLoading(false);
    }catch(error){
        setLoading(false);
    }
}

export const publishReturnStockTransaction = async (transactionId , items , setTransactionId , setLoading , setReturnInvoiceNumber , setEditedRow) => {
    try{
        const data = await api.addItemsToReturnStock(transactionId , items);
        openSuccessDialog(data.data.status , data.data.comment);
        setLoading(false);
        setTransactionId(null);
        setReturnInvoiceNumber('');
        setEditedRow([]);
    }catch(error){
        setLoading(false);
    }
}