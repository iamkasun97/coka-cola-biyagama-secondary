import {useState , useEffect , useContext} from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip } from 'recharts';
import Typography from '@mui/material/Typography';
import salesContext from '../../context/salesContext';

function Chart (){

    const salesData = useContext(salesContext);
    return (
        <div style={styles.mainDiv}>
            <div style={styles.chartDiv}>
                <Typography variant="h6" gutterBottom>
                        Sales Performance (Sales Rounds to Rs.1000)
                </Typography>    
                <Typography variant="h10" gutterBottom>
                        (Sales Rounds to Rs.1000)
                </Typography> 
                <LineChart width={700} height={315} data={salesData?.sales} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
                    <Line type="monotone" dataKey="sales" stroke="black" />
                    <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
                    <XAxis dataKey="date" />
                    <YAxis />
                    <Tooltip />
                </LineChart>
            </div>
        </div>
    )
}

export default Chart;

const styles = {
    chartDiv : {
        overflowX: 'scroll',
    },
    mainDiv : {
        margin : '20px'
    }
}