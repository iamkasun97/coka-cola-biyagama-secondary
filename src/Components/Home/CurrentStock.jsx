import {useState , useEffect} from 'react';
import Box from '@mui/material/Box';
import { DataGrid , GridToolbarContainer, GridToolbarExport} from '@mui/x-data-grid';
import { viewStock } from '../../Services/StockView';
import Typography from '@mui/material/Typography';

const columns = [
  {
    field: 'item_name',
    headerName: 'Item name',
    width: 200,
    tabIndex : -1,
  },
  {
    field: 'total_quantity',
    headerName: 'Total quantity',
    width: 200,
    tabIndex : 0,
    editable : true
  },
  {
    field: 'item_case_number_of_bottles',
    headerName: 'Case type',
    width: 200,
    tabIndex : 0,
  },
  {
    field: 'bottle_brought_price',
    headerName: 'Brought price',
    width: 200,
    tabIndex : 0,
    editable : true
  },
  {
    field: 'bottle_selling_price',
    headerName: 'Selling Price',
    width: 200,
    tabIndex : 0,
  },
];


function CustomToolbar(props) {

  return (
    <GridToolbarContainer>
      <GridToolbarExport 
      csvOptions={{
          fileName: new Date(),
      }}
      />
    </GridToolbarContainer>
  );
}



export default function CurrentStock() {

    const [rows , setRows] = useState([]);

    useEffect(() => {
        viewStock(setRows);
    },[]);

  return (
    <div style={{margin : '20px'}}>
    <Box sx={{ height: 400, width: '100%' }} >
    <Typography variant="h6" gutterBottom>
        Current Stock
    </Typography>
      <DataGrid
        pagination
        rows={rows}
        columns={columns}
        pageSize={5}
        rowsPerPageOptions={[5]}
        experimentalFeatures={{ newEditingApi: true }}
        components={{
          Toolbar: CustomToolbar,
      }}
      />
    </Box>
    </div>
  );
}