import * as api from '../api/index';


export const loadCaseConfiguration = async (setCases) => {
    try{
        const data = await api.loadConfiguration();
        let cases = [];
        data.data.data.case_types?.forEach(e => {
            let temp = {
                label  : e.case_name,
                value : e.id,
            }
            cases.push(temp)
        });
        setCases(cases);
    }catch(error){
        // console.log(error);
    }
}

export const loadCaseConfigurationAndProducts = async (setCases , currentPage , postsPerPage , setProducts , setCount) => {
    try{
        const data = await api.loadConfiguration();
        let cases = [];
        data.data.data.case_types?.forEach(e => {
            let temp = {
                label  : e.case_name,
                value : e.id,
            }
            cases.push(temp)
        });
        setCases(cases);
        const dataNext = await api.getProducts(currentPage , postsPerPage);
        setProducts(dataNext.data.data.data_for_the_loop);
        setCount(dataNext.data.data.count);
    }catch(error){
        // console.log(error);
    }
}

export const loadRouteConfiguration = async (setRoutes) => {
    try{
        const data = await api.loadConfiguration();
        setRoutes(data.data.data.routes);
    }catch(error){
        // console.log(error);
    }
}

export const loadRouteConfigurationAndProducts = async (setRoutes , setProducts) => {
    try{
        const data = await api.loadConfiguration();

    }catch(error){
        console.log(error);
    }
} 



export const loadRouteConfigurationAndCollections = async (setRoutes , setTransactionForm) => {
    try{
        const data = await api.loadConfiguration();
        setRoutes(data.data.data.routes);
        let cash = [];
        let others = [
            {
                sub_type : 'Credit',
                value : 0,
                amount : ''
            },
            {
                sub_type : 'Banked',
                value : 0,
                amount : ''
            },
            {
                sub_type : 'Expenses',
                value : 0,
                amount : ''
            },
            {
                sub_type : 'Discounts',
                value : 0,
                amount : ''
            },
            {
                sub_type : 'Cheques',
                value : 0,
                amount : ''
            },
            {
                sub_type : 'Credit Collections',
                value : 0,
                amount : ''
            },
        ]
        data.data.data.collection?.map(collection => {
            cash.push({
                sub_type : collection.collection_sub_type,
                value : Number(collection.value),
                amount : 0
            });
        });

        let collections = {
            cash : cash,
            others : others
        };
        // console.log(collections);
        setTransactionForm(collections);
    }catch(error){
        // console.log(error);
    }
}

export const loadConfigurationForProductWiseSummary = async (setRoutes , setProducts) => {
    try{
        const data = await api.loadConfigurationForProductWiseSummary();
        setRoutes(data.data.data.routes);
        let products = [];
        data.data.data?.products.forEach(pr => {
            products.push({
                id : pr.id,
                name : `${pr.item_name} (${pr.bottle_brought_price} / ${pr.bottle_selling_price})`,
                sellingPrice : pr.bottle_selling_price,
                broughtPrice : pr.bottle_brought_price
            });
        });
        setProducts(products);
    }catch(error){
        console.log(error);
    }
}