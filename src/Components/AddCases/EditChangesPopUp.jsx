import {useState} from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import Textfield from '../FormsUI/Textfield';
import * as Yup from "yup";
import { Form, Formik } from "formik";
import { editRouteDetails } from '../../Services/RouteService';
import Loader from '../Loader';
import { editCaseDetails } from '../../Services/CaseService';

export default function FormDialog(props) {
  const [open, setOpen] = useState(false);
  const [loading , setLoading] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  // form validation
  const INITIAL_FORM_STATE = {
    caseName:  props.data.row.case_name,
    numberOfBottles : props.data.row.number_of_bottles,
};

const FORM_VALIDATION = Yup.object().shape({
  caseName: Yup.string().required("Please Enter case name"),
  numberOfBottles : Yup.number().required('Please Enter number of bottles'),
});

  return (
    <div>
      <Button variant="contained" onClick={handleClickOpen} style={{width : '80px' , height : '40px'}}>
        Edit
      </Button>
      <div style={{width : '1000px'}}>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Edit User Details</DialogTitle>
        <Formik
        initialValues={{
            ...INITIAL_FORM_STATE,
        }}
        validationSchema={FORM_VALIDATION}
        onSubmit={(values , {resetForm}) => {
            setLoading(true);
            // console.log(values);
            // console.log(props.data);
            editCaseDetails(props.data.id , values.caseName , values.numberOfBottles , props.currentPage , props.postsPerPage , props.setCases , props.setCount , setLoading);
            setOpen(false);
        }}
        >
        <Form>
        <DialogContent>
          <Grid container spacing={3} >
            <Grid item xs={12} sm={6} >
              <Textfield
                    type={"text"}
                    name="caseName"
                    label="Case Name"
                    placeholder="Case Name"
                />
            </Grid>
            <Grid item xs={12} sm={6} >
              <Textfield
                    type={"text"}
                    name="numberOfBottles"
                    label="Number of Bottles"
                    placeholder="Number Of Bottles"
                />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button  type='submit'>Save</Button>
        </DialogActions>
        </Form></Formik>
        {loading && <Loader />}
      </Dialog>
      </div>
    </div>
  );
}