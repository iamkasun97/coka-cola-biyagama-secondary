export const AUTH = "AUTH";
export const LOGOUT = "LOGOUT";
export const PUBLISH = "PUBLISH";
export const PREVIEW = "PREVIEW";

export const REMEMBER_USER = "REMEMBER USER";
export const FORGET_USER = "FORGET USER";
export const SECRET_KEY = "ASALYA#!00";

export const BOOTSTRAP_ICONS = [
  {
    value: "bi bi-airplane",
    label: "bi bi-airplane",
    name: "Airplane",
  },
  {
    value: "bi bi-bar-chart",
    label: "bi bi-bar-chart",
    name: "Bar Chart",
  },
  {
    value: "bi bi-bank",
    label: "bi bi-bank",
    name: "Bank",
  },
];

export const IMAGE_SIZE = 500000;
export const IMAGE_SUPPORTED_FORMATS = "image/jpg,image/jpeg,image/png,";
export const VIDEO_SIZE = 5000000;
export const VIDEO_SUPPORTED_FORMATS = "video/mp4,video/webm,video/ogg,";
